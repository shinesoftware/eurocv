<?php
namespace Cv\Form\View\Helper;

use Zend\Form\View\Helper\FormElementErrors as OriginalFormElementErrors;

class FormElementErrors extends OriginalFormElementErrors
{
    protected $messageCloseString     = '</li></ul>';
    protected $messageOpenFormat      = '<ul class="list-unstyled" %s><li class="text-danger"><span class="fa fa-info-circle"></span> ';
    protected $messageSeparatorString = '</li><li class="">';
}

