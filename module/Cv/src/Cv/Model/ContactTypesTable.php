<?php
namespace Cv\Model;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class ContactTypesTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveData (TypeTypes $type)
    {
        $data = $type->getArrayCopy();
        $id = (int) $type->id;
        
        if ($id == 0) {
            $this->tableGateway->insert($data); // add the record
            $type = $this->getLast();
        } else {
            if ($this->getType($id)) {
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
            } else {
                throw new \Exception('Type ID does not exist');
            }
        }
        
        return $type;
    }

    /**
     * Get all the records
     * 
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Get Type by TypeId
     * 
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getType ($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    /**
     * Get Last Type account
     * 
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getLast ()
    {
        $rowset = $this->tableGateway->select(function (Select $select) {
		     $select->sort('id DESC')->limit(1);
		});
        
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find the record");
        }
        return $row;
    }

    /**
     * Delete Type account by TypeId
     * 
     * @param string $id            
     */
    public function deleteType ($id)
    {
        $this->tableGateway->delete(array(
                'id' => $id
        ));
    }
}
