<?php

namespace Application\Service;

use Traversable;
use Application\Form\CvRequestForm;
use Application\Form\CvRequestFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;

class PrivateCvFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        $config  = $services->get('config');
        if ($config instanceof Traversable) {
            $config = ArrayUtils::iteratorToArray($config);
        }
        $name    = $config['application']['form']['name'];
        
        $captcha = $services->get('PrivateCvCaptcha');
        $filter  = new CvRequestFilter();
        
        $form    = new CvRequestForm($name, $captcha);
        $form->setInputFilter($filter);
        return $form;
    }
}
