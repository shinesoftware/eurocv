<?php
namespace Cv\Model;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use GoogleMaps;

class AddressesTable
{

    protected $tableGateway;
    protected $countryTable;

    public function __construct (TableGateway $tableGateway, CountryTable $countryTable)
    {
        $this->tableGateway = $tableGateway;
        $this->countryTable = $countryTable;
    }
    
    /**
     * @return the $tableGateway
     */
    public function getTableGateway ()
    {
        return $this->tableGateway;
    }
    

    public function saveData (Addresses $address)
    {
        $data = $address->getArrayCopy();
        $id = (int) $address->id;

        $country = $this->countryTable->getCountry($address->getCountryId());

        // create the address string for the Google Maps service
        $str_address = $address->getStreet();
        $str_address .= " " . $address->getCode();
        $str_address .= " " . $address->getCity();
        $str_address .= " " . $country->getCountry();
        $latlong = $this->getLatLong($str_address);
        
        $data['latitude'] = !empty($latlong['lat']) ? $latlong['lat'] : null;
        $data['longitude'] = !empty($latlong['lng']) ? $latlong['lng'] : null;
        
        if ($id == 0) {
        	unset($data['id']);
            $data['createdat'] = date('Y-m-d H:i:s');
            $data['updatedat'] = date('Y-m-d H:i:s');
            $this->tableGateway->insert($data); // add the record
            $id = $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->getAddress($id, $data['personaldata_id'])) {
                $data['updatedat'] = date('Y-m-d H:i:s');
                unset( $data['createdat']);
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
                
            } else {
                throw new \Exception('Address ID does not exist');
            }
        }
        
        $address = $this->getAddress($id);
        return $address;
    }

    /**
     * Get the latitude and the longitude of a specific address
     * 
     * @param string $address
     * @return \GoogleMaps\Response
     */
    public function getLatLong($address){
        
        if(empty($address)){
            return array();
        }
        
        $request = new \GoogleMaps\Request();
        $request->setAddress($address);
        $proxy = new \GoogleMaps\Geocoder();
        $response = $proxy->geocode($request);

        if($response->getResults()->count()){
            $latlong = $response->getResults();
            $latlong = $latlong->current()->getGeometry()->getArrayCopy();
        
            return !empty($latlong['location']) ? $latlong['location'] : array();
        }
        return array();
    }
    
    /**
     * Get all users
     * 
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Get Address account by AddressId
     * 
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getAddress ($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get Address by personaldata_id
     *
     * @param integer $personaldata_id
     * @throws \Exception
     * @return Row
     */
    public function getAddressByPdId ($personaldata_id)
    {
    
        $rowset = $this->tableGateway->select(array('personaldata_id' => $personaldata_id));

        if (! $rowset) {
            return false;
        }
        return $rowset;
    }
    
    /**
     * Get Last Address account
     * 
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getLast ()
    {
        $rowset = $this->tableGateway->select(function (Select $select) {
		     $select->sort('id DESC')->limit(1);
		});
        
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find the record");
        }
        return $row;
    }

    /**
     * Delete Address account by AddressId
     * 
     * @param integer $id            
     * @param integer $personaldata_id            
     */
    public function deleteAddress ($id, $personaldata_id)
    {
       return $this->tableGateway->delete(array(
                'id' => $id, 
                'personaldata_id' => $personaldata_id
        ));
    }
}
