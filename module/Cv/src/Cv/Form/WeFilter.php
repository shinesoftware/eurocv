<?php
namespace Cv\Form;
use Zend\InputFilter\InputFilter;

class WeFilter extends InputFilter
{

    public function __construct ()
    {
        $this->add(array ( 
                'name' => 'employer', 
                'required' => true, 
        ));
        $this->add(array ( 
                'name' => 'activities', 
                'required' => true, 
        ));
        $this->add(array ( 
                'name' => 'startdate', 
                'required' => true, 
        ));
        $this->add(array ( 
                'name' => 'jobtermination_id', 
                'required' => false, 
        ));
    }
}