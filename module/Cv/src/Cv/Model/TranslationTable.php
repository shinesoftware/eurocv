<?php
namespace Cv\Model;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class TranslationTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveTranslation (Translation $translation)
    {
        $data = array ( 
                'title' => $translation->title,
                'active' => $translation->active,
                'content' => $translation->content,
                'eu' => $translation->eu,
                'dst' => $translation->dst,
        );
        
        $id = (int) $translation->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getTranslation($id)) {
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
            } else {
                throw new \Exception('Translation ID does not exist');
            }
        }
        
        return $translation;
    }

    /**
     * Get all users
     *
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select(function (\Zend\Db\Sql\Select $select) {
            $select->order('title');
        });
        $resultSet->buffer();
        return $resultSet;
    }

    /**
     * Get Translation by TranslationId
     *
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getTranslation ($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array ( 
                'id' => $id
        ));
        $row = $rowset->current();
        return $row;
    }

    /**
     * Get Translation by the locale
     *
     * @param string $locale            
     * @throws \Exception
     * @return Row
     */
    public function getTranslationbyLocale ($locale)
    {
        if(!empty($locale)){
            $rowset = $this->tableGateway->select(array ( 
                    'locale' => $locale
            ));
            $row = $rowset->current();
            return $row;
        }
        return array();
    }

    /**
     * Get Translation by the language code
     *
     * @param string $code            
     * @throws \Exception
     * @return Row
     */
    public function getTranslationbyCode ($code)
    {
        if(!empty($code)){
            $rowset = $this->tableGateway->select(array ( 
                    'code' => $code
            ));
            $row = $rowset->current();
            return $row;
        }
        return array();
    }

    /**
     * Delete Translation by TranslationId
     *
     * @param string $id            
     */
    public function deleteTranslation ($id)
    {
        $this->tableGateway->delete(array ( 
                'id' => $id
        ));
    }
}
