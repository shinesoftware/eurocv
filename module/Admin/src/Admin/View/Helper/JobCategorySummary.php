<?php 
namespace Admin\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class JobCategorySummary extends AbstractHelper implements ServiceLocatorAwareInterface  
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function __invoke()
    {
        $nRecords = array();
        
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();
        $commondata = $serviceLocator->get('CommonData');
        $translator = $serviceLocator->get('translator');
        $records = $commondata->getJobCategorySummary();
        
        // Translate the job categories title and sort the result
        foreach ($records as $record){
            $titleTranslated = $translator->translate($record['category']);
            $nRecords[$record['users']] = $titleTranslated;
        }
        
        // Sorting of the result
        asort($nRecords);
        
        return $this->view->render('admin/partial/jobcategorysummary', array('records' => $nRecords));
    }
}