<?php
namespace Cv\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class IndexController extends AbstractActionController {
	
	public function indexAction(){
		
	}
	
	public function changeVersionAction(){
		$session = new Container('base');
		$versionId = $this->params()->fromRoute('id');
		$translation = $this->getServiceLocator()->get('TranslationTable');
		
		if(!empty($versionId) && is_numeric($versionId)){
			$session->offsetSet('versionid', $versionId);
		}
		
		$url = $this->getRequest()->getHeader('Referer')->getUri();
		return $this->redirect()->toUrl($url);
	}
}