<?php
namespace Cv\Model;

class Uploads
{

    public $id;

    public $personaldata_id;

    public $translation_id;

    public $workexperience_id;

    public $education_id;

    public $language_id;

    public $originalfilename;

    public $codedfilename;

    public $mimetype;

    public $size;

    public $title;
    
    public $createdat;

    /**
     * This method get the array posted and assign the values to the table
     * object
     * 
     * @param array $data            
     */
    public function exchangeArray ($data)
    {
        foreach ($data as $field => $value) {
            $this->$field = (isset($value)) ? $value : null;
        }
        
        return true;
    }
    
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $personaldata_id
     */
    public function getPersonaldataId ()
    {
        return $this->personaldata_id;
    }

	/**
     * @return the $translation_id
     */
    public function getTranslationId ()
    {
        return $this->translation_id;
    }

	/**
     * @return the $createdat
     */
    public function getCreatedat ()
    {
        return $this->createdat;
    }

	/**
     * @return the $workexperience_id
     */
    public function getWorkexperienceId ()
    {
        return $this->workexperience_id;
    }

	/**
     * @return the $education_id
     */
    public function getEducationId ()
    {
        return $this->education_id;
    }

	/**
     * @return the $language_id
     */
    public function getLanguageId ()
    {
        return $this->language_id;
    }

	/**
     * @return the $originalfilename
     */
    public function getOriginalfilename ()
    {
        return $this->originalfilename;
    }

	/**
     * @return the $codedfilename
     */
    public function getCodedfilename ()
    {
        return $this->codedfilename;
    }

	/**
     * @return the $mimetype
     */
    public function getMimetype ()
    {
        return $this->mimetype;
    }

	/**
     * @return the $size
     */
    public function getSize ()
    {
        return $this->size;
    }

	/**
     * @return the $title
     */
    public function getTitle ()
    {
        return $this->title;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $personaldata_id
     */
    public function setPersonaldataId ($personaldata_id)
    {
        $this->personaldata_id = $personaldata_id;
    }

	/**
     * @param field_type $translation_id
     */
    public function setTranslationId ($translation_id)
    {
        $this->translation_id = $translation_id;
    }

	/**
     * @param field_type $createdat
     */
    public function setCreatedat ($createdat)
    {
        $this->createdat = $createdat;
    }

	/**
     * @param field_type $workexperience_id
     */
    public function setWorkexperienceId ($workexperience_id)
    {
    	$workexperience_id = strlen($workexperience_id) == 0 ? null : $workexperience_id;
        $this->workexperience_id = $workexperience_id;
    }

	/**
     * @param field_type $education_id
     */
    public function setEducationId ($education_id)
    {
    	$education_id = strlen($education_id) == 0 ? null : $education_id;
        $this->education_id = $education_id;
    }

	/**
     * @param field_type $language_id
     */
    public function setLanguageId ($language_id)
    {
    	$language_id = strlen($language_id) == 0 ? null : $language_id;
    	$this->language_id = $language_id;
    }

	/**
     * @param field_type $originalfilename
     */
    public function setOriginalfilename ($originalfilename)
    {
        $this->originalfilename = $originalfilename;
    }

	/**
     * @param field_type $codedfilename
     */
    public function setCodedfilename ($codedfilename)
    {
        $this->codedfilename = $codedfilename;
    }

	/**
     * @param field_type $mimetype
     */
    public function setMimetype ($mimetype)
    {
        $this->mimetype = $mimetype;
    }

	/**
     * @param field_type $size
     */
    public function setSize ($size)
    {
        $this->size = $size;
    }

	/**
     * @param field_type $title
     */
    public function setTitle ($title)
    {
        $this->title = $title;
    }

}
