<?php
namespace Cv\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Cv\Model\We;
use Zend\Stdlib\Hydrator\ClassMethods;
use \Cv\Hydrator\Strategy\DateTimeStrategy;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Column\Style;
use ZfcDatagrid\Column\Formatter;
use ZfcDatagrid\Filter;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Session\Container;
/**
 * Work Experiences Controller
 * This controller gets the personal data of the user
 *
 * @author mturillo
 */
class WeController extends AbstractActionController
{
    protected $user;
    protected $personaldata;
    protected $translationId;
    protected $versionId;
	
    
	/**
	 * OnDispatch of the controller
	 * (non-PHPdoc)
	 * 
	 * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
	 */
	public function onDispatch(\Zend\Mvc\MvcEvent $e) {
		
		$session = new Container('base');
		$this->versionId = $session->offsetGet('versionid') ? $session->offsetGet('versionid') : 2;
		
		// check the credencials
		if (! $this->zfcUserAuthentication ()->hasIdentity ()) {
			return $this->redirect ()->toRoute ( 'www/user' );
		}
		
		// Get the identity information
		$this->user = $this->zfcUserAuthentication ()->getIdentity ();
		
		// Get the TableGateway object to retrieve the data
		$user = $this->getServiceLocator ()->get ( 'PdTable' );
		
		// Get the user
		$myUser = $user->getPdByUserId ( $this->user->getId() );
		
		if (! empty ( $myUser )) {
			$this->personaldata = $myUser;
		}else{
		    $this->flashMessenger ()->setNamespace ( 'danger' )->addMessage ( 'You have to complete the personal data section.' );
		    return $this->redirect()->toRoute('cv/pd');
		}
		
		return parent::onDispatch ( $e );
	}
    
    public function addAction ()
    {
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\WeForm');

        $viewModel = new ViewModel(array ( 
                'form' => $form
        ));
        $viewModel->setTemplate('cv/we/index');
        return $viewModel;
    }

    public function editAction ()
    {
        $id = $this->params()->fromRoute('id');
        
        if (empty($id)) {
            return $this->redirect()->toRoute(NULL, array ( 
                    'controller' => 'we', 
                    'action' => 'list'
            ));
        }
        
        $weTable = $this->getServiceLocator()->get('WeTable');
        $we = $weTable->getWe($id, $this->personaldata->getId());

        if (! empty($we)) {
            
            $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\WeForm');
            $form->bind($we);
            
            $viewModel = new ViewModel(array ( 
                    'form' => $form, 
                    'pac' => $this->personaldata->getPac(),
                    'id' => $id
            ));
            
            $viewModel->setTemplate('cv/we/index');
            
            return $viewModel;
        } else {
            return $this->redirect()->toRoute(NULL, array ( 
                    'controller' => 'we', 
                    'action' => 'edit',
                    'id' => $id
            ));
        }
    }

    public function listAction ()
    {
        $grid = $this->createGrid();
        $grid->render();
        
        $response = $grid->getResponse();
        
        if ($grid->isHtmlInitReponse()) {
            $view = new ViewModel();
            $view->addChild($response, 'grid');
            return $view;
        } else {
            return $response;
        }
        
    }


    // Create the list grid
    private function createGrid ()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $select = new Select();
        $select->from(array ('we' => 'work_experiences'))
               ->join(array('j' => 'job_category'), 'we.jobcategory_id = j.id', array('category'), 'left')
               ->where(array ('personaldata_id' => $this->personaldata->getId()))
               ->where(array ('translation_id' => $this->versionId))->order('enddate desc, sort asc');
    
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $grid->setDefaultItemsPerPage(100);
        $grid->setDataSource($select, $dbAdapter);
    
        $colId = new Column\Select('id', 'we');
        $colId->setLabel('Id');
        $colId->setIdentity();
        $grid->addColumn($colId);
    
        $colType = new Type\DateTime('Y-m-d', \IntlDateFormatter::SHORT);
        $colType->setSourceTimezone('Europe/Rome');
        $colType->setOutputTimezone('UTC');
        $colType->setLocale('it_IT');
        
        $col = new Column\Select('startdate', 'we');
        $col->setType($colType);
        $col->setUserFilterDisabled(true);
        $col->setLabel(_('Start'));
        $grid->addColumn($col);
    
        $col = new Column\Select('enddate', 'we');
        $col->setType($colType);
        $col->setUserFilterDisabled(true);
        $col->setLabel(_('Finish'));
        $grid->addColumn($col);
        
        $col = new Column\Select('employer', 'we');
        $col->setLabel(_('Name and address of employer'));
        $col->setUserFilterDisabled(true);
        $col->setWidth(15);
        $grid->addColumn($col);
    
        $col = new Column\Select('category', 'j');
        $col->setLabel(_('Job Categories'));
        $col->setUserFilterDisabled(true);
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
    
        $col = new Column\Select('visible', 'we');
        $col->setType(new \ZfcDatagrid\Column\Type\String());
        $col->setLabel(_('Visible'));
        $col->setUserFilterDisabled(true);
        $col->setTranslationEnabled(true);
        $col->setFilterSelectOptions(array (
                '' => '-',
                '0' => 'No',
                '1' => 'Yes'
        ));
        $col->setReplaceValues(array (
                '' => '-',
                '0' => 'No',
                '1' => 'Yes'
        ));
        $grid->addColumn($col);
        
        $col = new Column\Select('sort', 'we');
        $col->setLabel(_('Sort'));
        $col->setUserFilterDisabled(true);
        $grid->addColumn($col);
    
        // Add actions to the grid
        $showaction = new Column\Action\Button();
        $showaction->setAttribute('href', "/cv/we/edit/" . $showaction->getColumnValuePlaceholder(new Column\Select('id', 'we')));
        $showaction->setAttribute('class', 'btn btn-xs btn-success');
        $showaction->setLabel(_('edit'));
    
        $delaction = new Column\Action\Button();
        $delaction->setAttribute('href', '/cv/we/delete/' . $delaction->getRowIdPlaceholder());
        $delaction->setAttribute('onclick', "return confirm('Are you sure?')");
        $delaction->setAttribute('class', 'btn btn-xs btn-danger');
        $delaction->setLabel(_('delete'));
    
        $col = new Column\Action();
        $col->addAction($showaction);
        $col->addAction($delaction);
        $grid->addColumn($col);
    
        $grid->setToolbarTemplate('');
    
        return $grid;
    }
    
    public function processAction ()
    {
        
        if (! $this->request->isPost()) {
            
            return $this->redirect()->toRoute(NULL, array ( 
                    'controller' => 'we', 
                    'action' => 'index'
            ));
        }
        
        $post = $this->request->getPost();
        
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\WeForm');
        $inputFilter = $this->getServiceLocator()->get('WeFilter');
        $form->setInputFilter($inputFilter);
        
        $form->setData($post);
        
        if (! $form->isValid()) {
            $model = new ViewModel(array ( 
                    'error' => true, 
                    'form' => $form
            ));
            $model->setTemplate('cv/we/index');
            return $model;
        }

        // Get the posted vars
        $we = $form->getData();
        
        // Set the userId
        $we->setPersonaldataId($this->personaldata->getId());
        $we->setTranslationId($this->versionId);
        
        // Save the data in the database
        $WeTable = $this->getServiceLocator()->get('WeTable')->saveData($we);
        
        $this->flashMessenger ()->setNamespace ( 'success' )->addMessage ( 'The information have been saved correctly.' );
        
        return $this->redirect()->toRoute(NULL, array ( 
                'controller' => 'we', 
                'action' => 'edit', 
                'id' => $WeTable->getId()
        ));
    }

    /**
     * Delete the record
     */
    public function deleteAction ()
    {
        
        $id = $this->params()->fromRoute('id');
        
        if (empty($id)) {
            return $this->redirect()->toRoute(NULL, array (
                    'controller' => 'we',
                    'action' => 'list'
            ));
        }
        
        $this->getServiceLocator()->get('WeTable')->deleteWe($id, $this->personaldata->getId());
        $this->flashMessenger ()->setNamespace ( 'success' )->addMessage ( 'The information have been deleted correctly.' );
        return $this->redirect()->toRoute(NULL, array (
                    'controller' => 'we',
                    'action' => 'list'
            ));
    }
}