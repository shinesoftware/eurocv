<?php
return array ( 
        'module_config' => array(
                'document_location' => __DIR__ . '/../../../public/documents',  // Upload manager path for documents
                'photo_location' => __DIR__ . '/../../../public/photos',  // Upload manager path for photos
        ),
        'controllers' => array ( 
                'invokables' => array ( 
                        'Cv\Controller\Index' => 'Cv\Controller\IndexController', 
                        'Cv\Controller\Pd' => 'Cv\Controller\PdController', 
                        'Cv\Controller\We' => 'Cv\Controller\WeController',
                        'Cv\Controller\Et' => 'Cv\Controller\EtController',
                        'Cv\Controller\Ol' => 'Cv\Controller\OlController',
                        'Cv\Controller\Cs' => 'Cv\Controller\CsController',
                        'Cv\Controller\Uploads' => 'Cv\Controller\UploadsController',
                        'Cv\Controller\Cl' => 'Cv\Controller\ClController',
                )
        ), 
        
        'navigation' => array (
                'default' => array (
                        array (
                                'label' => 'Your Cv',
                                'uri' => 'http://www.eurocv.eu/cv/pd',
                                'resource' => 'cvmenu',
                                'privilege' => 'list',
                                'pages' => array (
                                        array (
                                                'label' => 'Personal data',
                                                'uri' => 'http://www.eurocv.eu/cv/pd',
                                                'icon' => 'fa fa-lock'
                                        ),
                                        array (
                                                'label' => 'Work Experience',
                                                'uri' => 'http://www.eurocv.eu/cv/we',
                                                'icon' => 'fa fa-tasks'
                                        ),
                                        array (
                                                'label' => 'Education & Training',
                                                'uri' => 'http://www.eurocv.eu/cv/et',
                                                'icon' => 'fa fa-book'
                                        ),
                                        array (
                                                'label' => 'Languages',
                                                'uri' => 'http://www.eurocv.eu/cv/ol',
                                                'icon' => 'fa fa-flag'
                                        ),
                                        array (
                                                'label' => 'Competences & Skills',
                                                'uri' => 'http://www.eurocv.eu/cv/cs',
                                                'icon' => 'fa fa-file'
                                        ),
                                        array (
                                                'label' => 'Cover letters',
                                                'uri' => 'http://www.eurocv.eu/cv/cl',
                                                'icon' => 'fa fa-file-o'
                                        ),
                                        array (
                                                'label' => 'Upload Manager',
                                                'uri' => 'http://www.eurocv.eu/cv/uploads',
                                                'icon' => 'fa fa-upload'
                                        )
                                )
                        ),
                )
        
        ),
        'view_helpers' => array(
                'invokables'=> array(
                        'createMap' => 'Cv\View\Helper\MapHelper',
                        'formelementerrors' => 'Cv\Form\View\Helper\FormElementErrors',
                		'photo' => 'Cv\View\Helper\Photo',
                		'versions' => 'Cv\View\Helper\Versions',
                        'toolbar' => 'Cv\View\Helper\Toolbar'
                )
        ),
        'router' => array ( 
                'routes' => array ( 
                        'cv' => array ( 
                                'type' => 'Literal', 
                                'options' => array ( 
                                        'route' => '/cv', 
                                        'defaults' => array ( 
                                                '__NAMESPACE__' => 'Cv\Controller', 
                                                'controller' => 'Index', 
                                                'action' => 'index'
                                        )
                                ), 
                                'may_terminate' => true, 
                                'child_routes' => array ( 
                                        'default' => array ( 
                                                'type' => 'Segment', 
                                                'options' => array ( 
                                                        'route' => '/[:controller[/:action[/:id]]]', 
                                                        'constraints' => array ( 
                                                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*', 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                        		'id' => '[a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ()
                                                )
                                        ), 
                                        'pd' => array ( 
                                                'type' => 'Segment', 
                                                'may_terminate' => true, 
                                                'options' => array ( 
                                                        'route' => '/pd[/:action[/:id]]', 
                                                        'constraints' => array ( 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'id' => '[a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ( 
                                                                'controller' => 'Cv\Controller\Pd', 
                                                                'action' => 'index'
                                                        )
                                                )
                                        ), 
                                        'we' => array ( 
                                                'type' => 'Segment', 
                                                'may_terminate' => true, 
                                                'options' => array ( 
                                                        'route' => '/we[/:action[/:id]]', 
                                                        'constraints' => array ( 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'id' => '[a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ( 
                                                                'controller' => 'Cv\Controller\We', 
                                                                'action' => 'list'
                                                        )
                                                ), 
                                        ), 
                                        'et' => array ( 
                                                'type' => 'Segment', 
                                                'may_terminate' => true, 
                                                'options' => array ( 
                                                        'route' => '/et[/:action[/:id]]', 
                                                        'constraints' => array ( 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'id' => '[a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ( 
                                                                'controller' => 'Cv\Controller\Et', 
                                                                'action' => 'list'
                                                        )
                                                ), 
                                        ), 
                                        'ol' => array ( 
                                                'type' => 'Segment', 
                                                'may_terminate' => true, 
                                                'options' => array ( 
                                                        'route' => '/ol[/:action[/:id]]', 
                                                        'constraints' => array ( 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'id' => '[a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ( 
                                                                'controller' => 'Cv\Controller\Ol', 
                                                                'action' => 'list'
                                                        )
                                                )
                                        ), 
                                        'cs' => array ( 
                                                'type' => 'Segment', 
                                                'may_terminate' => true, 
                                                'options' => array ( 
                                                        'route' => '/cs[/:action[/:id]]', 
                                                        'constraints' => array ( 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'id' => '[a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ( 
                                                                'controller' => 'Cv\Controller\Cs', 
                                                                'action' => 'list'
                                                        )
                                                )
                                        ),
                                        'uploads' => array(
                                                'type'    => 'Segment',
                                                'may_terminate' => true,
                                                'options' => array(
                                                        'route'    => '/uploads[/:action[/:id]]',
                                                        'constraints' => array(
                                                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'id'     => '[a-zA-Z0-9_-]*',
                                                        ),
                                                        'defaults' => array(
                                                                'controller' => 'Cv\Controller\Uploads',
                                                                'action'     => 'index',
                                                        ),
                                                ),
                                        ),
                                        'cl' => array(
                                                'type'    => 'Segment',
                                                'may_terminate' => true,
                                                'options' => array(
                                                        'route'    => '/cl[/:action[/:id]]',
                                                        'constraints' => array(
                                                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'id'     => '[a-zA-Z0-9_-]*',
                                                        ),
                                                        'defaults' => array(
                                                                'controller' => 'Cv\Controller\Cl',
                                                                'action'     => 'list',
                                                        ),
                                                ),
                                        ),
                                )
                        )
                )
                
        ), 
        'view_manager' => array ( 
                'template_path_stack' => array(__DIR__ . '/../view'),
        ),
);
