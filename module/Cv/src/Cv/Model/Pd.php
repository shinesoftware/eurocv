<?php
namespace Cv\Model;

class Pd{

    protected $id;
    protected $user_id;
    protected $firstname;
    protected $lastname;
    protected $birthdate;
    protected $gender;
    protected $birthplace;    
    protected $birthnation;    
    protected $nationality;    
    protected $country_id;    
    protected $mothertongue_id;    
    protected $email;    
    protected $pac;    
    protected $translation_id;
    protected $public;   
    protected $updatedat;   
    protected $createdat;   
    protected $keywords;   
    protected $ganalytics;   
    protected $jobcategory_id;   
    protected $download;   
    protected $visits;   
    
    protected $addresses;  // Fieldset
    protected $contacts;  // Fieldset

    /**
     * This method get the array posted and assign the values to the table object
     * @param array $data
     */
    public function exchangeArray ($data)
    {
    	foreach ($data as $field => $value){
    		$this->$field = (isset($value)) ? $value : null;
    	}
    
    	return true;
    }
    
    /**
	 * @return the $mothertongue_id
	 */
	public function getMothertongueId() {
		return $this->mothertongue_id;
	}

	/**
	 * @param field_type $mothertongue_id
	 */
	public function setMothertongueId($mothertongue_id) {
		$this->mothertongue_id = $mothertongue_id;
	}

    /**
     * Get the copy of the class as array
     * @return array
     */
//     public function getArrayCopy()
//     {
//         return get_object_vars($this);
//     }
    
	/**
     * @param \Cv\Model\Addresses $address
     */
    public function setAddresses ($address)
    {
        $this->addresses = $address;
    }

	/**
     * @param \Cv\Model\Contacts $contact
     */
    public function setContacts ($contact)
    {
        $this->contacts = $contact;
    }
    
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $user_id
     */
    public function getUserId ()
    {
        return $this->user_id;
    }

	/**
     * @return the $firstname
     */
    public function getFirstname ()
    {
        return $this->firstname;
    }

	/**
     * @return the $lastname
     */
    public function getLastname ()
    {
        return $this->lastname;
    }

	/**
     * @return the $birthdate
     */
    public function getBirthdate ()
    {
        return $this->birthdate;
    }

	/**
     * @return the $gender
     */
    public function getGender ()
    {
        return $this->gender;
    }

	/**
     * @return the $birthplace
     */
    public function getBirthplace ()
    {
        return $this->birthplace;
    }

	/**
     * @return the $birthnation
     */
    public function getBirthnation ()
    {
        return $this->birthnation;
    }

	/**
     * @return the $nationality
     */
    public function getNationality ()
    {
        return $this->nationality;
    }

	/**
     * @return the $country_id
     */
    public function getCountryId ()
    {
        return $this->country_id;
    }

	/**
     * @return the $email
     */
    public function getEmail ()
    {
        return $this->email;
    }

	/**
     * @return the $pac
     */
    public function getPac ()
    {
        return $this->pac;
    }

	/**
     * @return the $translation_id
     */
    public function getTranslationId ()
    {
        return $this->translation_id;
    }

	/**
     * @return the $public
     */
    public function getPublic ()
    {
        return $this->public;
    }

	/**
     * @return the $updatedat
     */
    public function getUpdatedat ()
    {
        return $this->updatedat;
    }

	/**
     * @return the $createdat
     */
    public function getCreatedat ()
    {
        return $this->createdat;
    }

	/**
     * @return the $keywords
     */
    public function getKeywords ()
    {
        return $this->keywords;
    }

	/**
     * @return the $jobcategory_id
     */
    public function getJobcategoryId ()
    {
        return $this->jobcategory_id;
    }

	/**
     * @return the $addresses
     */
    public function getAddresses ()
    {
        return $this->addresses;
    }

	/**
     * @return the $contacts
     */
    public function getContacts ()
    {
        return $this->contacts;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $user_id
     */
    public function setUserId ($user_id)
    {
        $this->user_id = $user_id;
    }

	/**
     * @param field_type $firstname
     */
    public function setFirstname ($firstname)
    {
        $this->firstname = $firstname;
    }

	/**
     * @param field_type $lastname
     */
    public function setLastname ($lastname)
    {
        $this->lastname = $lastname;
    }

	/**
     * @param field_type $birthdate
     */
    public function setBirthdate ($birthdate)
    {
        $this->birthdate = $birthdate;
    }

	/**
     * @param field_type $gender
     */
    public function setGender ($gender)
    {
        $this->gender = $gender;
    }

	/**
     * @param field_type $birthplace
     */
    public function setBirthplace ($birthplace)
    {
        $this->birthplace = $birthplace;
    }

	/**
     * @param field_type $birthnation
     */
    public function setBirthnation ($birthnation)
    {
        $this->birthnation = $birthnation;
    }

	/**
     * @param field_type $nationality
     */
    public function setNationality ($nationality)
    {
        $this->nationality = $nationality;
    }

	/**
     * @param field_type $country_id
     */
    public function setCountryId ($country_id)
    {
        $this->country_id = $country_id;
    }

	/**
     * @param field_type $email
     */
    public function setEmail ($email)
    {
        $this->email = $email;
    }

	/**
     * @param field_type $pac
     */
    public function setPac ($pac)
    {
        if(!is_null($pac)){
            $this->pac = $pac;
        }
    }

	/**
     * @param field_type $translation_id
     */
    public function setTranslationId ($translation_id)
    {
        $this->translation_id = $translation_id;
    }

	/**
     * @param field_type $public
     */
    public function setPublic ($public)
    {
        $this->public = $public;
    }

    /**
     * @param field_type $updatedat
     */
    public function setUpdatedat ($updatedat)
    {
        $this->updatedat = $updatedat;
    }

	/**
     * @param field_type $createdat
     */
    public function setCreatedat ($createdat)
    {
        $this->createdat = $createdat;
    }

	/**
     * @param field_type $keywords
     */
    public function setKeywords ($keywords)
    {
        $this->keywords = $keywords;
    }

	/**
     * @param field_type $jobcategory_id
     */
    public function setJobcategoryId ($jobcategory_id)
    {
        $this->jobcategory_id = $jobcategory_id;
    }
	/**
     * @return the $ganalytics
     */
    public function getGanalytics ()
    {
        return $this->ganalytics;
    }

	/**
     * @param field_type $ganalytics
     */
    public function setGanalytics ($ganalytics)
    {
        $this->ganalytics = $ganalytics;
    }
	/**
     * @return the $download
     */
    public function getDownload ()
    {
        return $this->download;
    }

	/**
     * @return the $visits
     */
    public function getVisits ()
    {
        return $this->visits;
    }

	/**
     * @param field_type $download
     */
    public function setDownload ($download)
    {
        $this->download = $download;
    }

	/**
     * @param field_type $visits
     */
    public function setVisits ($visits)
    {
        $this->visits = $visits;
    }




}