<?php 
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Attachments extends AbstractHelper implements ServiceLocatorAwareInterface  
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function __invoke($personadata_id, $translation_id=null, $workexperience_id=null, $education_id=null, $language_id=null)
    {
        if(!is_null($personadata_id)){
            $serviceLocator = $this->getServiceLocator()->getServiceLocator();
            $uploads = $serviceLocator->get('UploadsTable');
            $files = $uploads->getUploads($personadata_id, $translation_id, $workexperience_id, $education_id, $language_id);
            
            return $this->view->render('application/partial/attachments', array('files' => $files));
        }
    }
}