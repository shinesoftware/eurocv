<?php
namespace Cv\Model;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Stdlib\Hydrator\ClassMethods;

class CsTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return the $tableGateway
     */
    public function getTableGateway ()
    {
        return $this->tableGateway;
    }
    
    public function saveData (Cs $cs)
    {
         $hydrator = new ClassMethods(true);
        
        // extract the data from the object
        $data = $hydrator->extract($cs);
        
        $id = (int) $cs->getId();
        
        if ($id == 0) {
        	unset($data['id']);
            $data['createdat'] = date('Y-m-d H:i:s');
            $data['updatedat'] = date('Y-m-d H:i:s');
            $this->tableGateway->insert($data); // add the record
            $id = $this->tableGateway->getLastInsertValue();
        } else {
        	$rs = $this->getCs($id, $data['personaldata_id']);
            if (!empty($rs)) {
                $data['updatedat'] = date('Y-m-d H:i:s');
                $data['createdat'] = $rs->createdat;
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
            } else {
                throw new \Exception('Education ID does not exist');
            }
        }
        
        $cs = $this->getCs($id, $data['personaldata_id']);
        return $cs;
    }

    /**
     * Get all records
     *
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        
        return $resultSet;
    }

    /**
     * Get all records by Id
     *
     * @return ResultSet
     */
    public function fetchAllbyId ($personaldata_id, $translation_id=null)
    {
        $criteria = array();
        
        if(!is_null($translation_id)){
            $criteria['translation_id'] = $translation_id;
        }
        
        $criteria['personaldata_id'] = $personaldata_id;
        
        $resultSet = $this->tableGateway->select($criteria);
        $resultSet->buffer();
        
        return $resultSet;
    }
    
    /**
     * Get a summary of the records grouping them
     * by translation language and filter
     * them by personal data Id
     *
     * @param int $personaldata_id
     * @return ResultSet
     */
    public function getSummary ($personaldata_id, $versionId = null, $getTotal=false)
    {
    	$records = null;
    	
        $sql = new \Zend\Db\Sql\Sql($this->getTableGateway()->getAdapter());
        $select = $sql->select($this->getTableGateway()->getTable());
        $select->columns(array('items' => new \Zend\Db\Sql\Expression('COUNT(*)')));
    
        if(is_numeric($personaldata_id)){
            $select->where(array('personaldata_id' => $personaldata_id));
        }
        
        if(is_numeric($versionId)){
        	$select->where(array('translation_id' => $versionId));
        }
        
        if($getTotal === false){
            $select->group('translation_id');
            $select->join('translation', 'translation_id = translation.id', array ('title', 'code'), 'left');
        }
    
        $statement = $this->getTableGateway()->getAdapter()->createStatement();
        $select->prepareStatement($this->getTableGateway()->getAdapter(), $statement);
        $results = $statement->execute();
    
        $rows = array();
        if ($results->count()) {
            $rows = new \Zend\Db\ResultSet\ResultSet();
            $records = $rows->initialize($results)->toArray();
        }
        return $records;
    }

    /**
     * Get Personaldata account by PersonaldataId
     *
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getCs ($id, $personaldata_id)
    {
        if (is_numeric($id) && is_numeric($personaldata_id)) {
            $id = (int) $id;
            $personaldata_id = (int) $personaldata_id;
            $rowset = $this->tableGateway->select(array ( 
                    'id' => $id, 
                    'personaldata_id' => $personaldata_id
            ));
            
            $row = $rowset->current();
            if (! $row) {
                return false;
            }
            return $row;
        }
        
        return false;
    }

    /**
     * Get Last record
     *
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getLast ()
    {
        $rowset = $this->tableGateway->select(function  (Select $select)
        {
            $select->order('id DESC')->limit(1);
        });
        
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find the record");
        }
        return $row;
    }

    /**
     * Get Education by personaldata_id
     *
     * @param string $personaldata_id            
     * @throws \Exception
     * @return Row
     */
    public function getCsByPersonalDataId ($personaldata_id)
    {
        if(!is_numeric($personaldata_id)){
            return array();
        }
        
        $select = $this->tableGateway->getSql()
            ->select()
            ->join('translation', 'competences.translation_id = translation.id', array ( 
                'translation' => 'title', 
                'code'
        ), 'left')
            ->where(array ( 
                'personaldata_id' => $personaldata_id
        ));
        $rowset = $this->tableGateway->selectWith($select);
        
        if (! $rowset) {
            return false;
        }
        return $rowset;
    }

    /**
     * Delete the record
     *
     * @param integer $id            
     * @param integer $personaldata_id            
     */
    public function deleteCs ($id, $personaldata_id)
    {
        $this->tableGateway->delete(array ( 
                'id' => $id, 
                'personaldata_id' => $personaldata_id
        ));
    }
}
