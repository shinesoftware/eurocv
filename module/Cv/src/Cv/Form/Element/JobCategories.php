<?php
namespace Cv\Form\Element;

use Cv\Model\JobCategoryTable;
use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\I18n\Translator\Translator;

class JobCategories extends Select implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $translator;
    protected $jobcategoryTable;
    
    public function __construct(JobCategoryTable $jobcategoryTable, \Zend\Mvc\I18n\Translator $translator){
        parent::__construct();
        $this->jobcategoryTable = $jobcategoryTable;
        $this->translator = $translator;
    }
    
    public function init()
    {
        $data = array();
        
        $jobcategories = $this->jobcategoryTable->fetchAll();
        foreach ($jobcategories as $jobcategory){
            $data[$jobcategory->getId()] = $this->translator->translate($jobcategory->getCategory());
        }
        asort($data);
        $this->setValueOptions($data);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
