<?php
namespace Base\Model;

use ZfcDatagrid\Column\AbstractColumn as AbstractFormatter;
use ZfcDatagrid\Column\Formatter\Image;

class DatagridImage extends Image
{
	protected $path;
	
	public function setPath($path){
		$this->path = $path;
	}
	
	public function setFilename($name){
		$photos = glob($this->path . $name . "*");
		if(!empty($photos[0])){
			return basename($photos[0]);
		}
		return false;
	}
	
	
	public function getFormattedValue(AbstractFormatter $column)
	{
		$row = $this->getRowData();
		$value = $row[$column->getUniqueId()];
		$prefix = $this->getPrefix();
		
		if (is_array($value)) {
			$thumb = $value[0];
	
			if (isset($value[1])) {
				$original = $value[1];
			} else {
				$original = $thumb;
			}
		} else {
			$thumb = $value;
			$original = $value;
		}
	
		$linkAttributes = array();
		foreach ($this->getLinkAttributes() as $key => $value) {
			$linkAttributes[] = $key . '="' . $value . '"';
		}
	
		$attributes = array();
		foreach ($this->getAttributes() as $key => $value) {
			$attributes[] = $key . '="' . $value . '"';
		}
	
		if(!empty($thumb)){
			$filename = $this->setFilename($thumb);
			if($filename){
				return '<a href="' . $prefix . $original . '" ' . implode(' ', $linkAttributes) . '><img src="' . $prefix . $filename . '" ' . implode(' ', $attributes) . ' /></a>';
			}else{
                return '<img ' . implode(' ', $attributes) . ' src="https://placeholdit.imgix.net/~text?txtsize=33&txt=eurocv&w=350&h=200" />';
			}
		}else{
			return false;
		}
	
	}
}
