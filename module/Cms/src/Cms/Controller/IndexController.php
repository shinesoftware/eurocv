<?php
namespace Cms\Controller;

use Cms\Model\Page;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    public function indexAction ()
    {
        $pageTable = $this->getServiceLocator()->get('PageTable');
		$viewModel  = new ViewModel(array('pages' => $pageTable->fetchAll())); 
		return $viewModel; 
    }

    public function pageAction ()
    {
        $uri = $this->getEvent()->getRouteMatch()->getParam('page');
        $pageTable = $this->getServiceLocator()->get('PageTable');
		$viewModel  = new ViewModel(array('page' => $pageTable->getPageByUri($uri))); 
		return $viewModel; 
    }

    public function saveAction ()
    {
        $isError = true;
       
        if (! $this->request->isPost()) {
            return $this->redirect()->toRoute(NULL,
                    array(
                            'controller' => 'index',
                            'action' => 'index'
                    ));
        }
        
        $post = $this->request->getPost();
        $form = $this->getServiceLocator()->get('PageForm');
        $inputFilter = $this->getServiceLocator()->get('PageFilter');
        $form->setInputFilter($inputFilter);
        $form->setData($post);
        
    	if (! $form->isValid()) {
            $model = new ViewModel(
                    array(
                            'error' => true,
                            'form' => $form
                    ));
            $model->setTemplate('cms/index/index');
            return $model;
        }
        
        $contact = new Page();
        $contact->exchangeArray($post);
        $contactTable = $this->getServiceLocator()->get('PageTable');
        $contactTable->savePage($contact);

        // send the message 
        if($contactTable->sendMessage($contact)){
            $isError = false;
        }
        
        $view = new ViewModel(array('error' => $isError));
        $view->setTemplate('cms/index/confirm');
        return $view;
    }

}