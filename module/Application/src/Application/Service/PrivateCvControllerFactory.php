<?php

namespace Application\Service;

use Application\Controller\PrivateCvController;
use Zend\Mail\Message;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PrivateCvControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        $serviceLocator = $services->getServiceLocator();
        $form           = $serviceLocator->get('PrivateCvForm');
        $transport      = $serviceLocator->get('PrivateCvMailTransport');
        
        $controller = new PrivateCvController();
        $controller->setContactForm($form);
        $controller->setMailTransport($transport);
        
        return $controller;
    }
}
