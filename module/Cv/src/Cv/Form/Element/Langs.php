<?php
namespace Cv\Form\Element;

use Cv\Model\LangsTable;
use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\I18n\Translator\Translator;

class Langs extends Select implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $translator;
    protected $langsTable;
    
    public function __construct(LangsTable $langsTable, \Zend\Mvc\I18n\Translator $translator){
        parent::__construct();
        $this->langsTable = $langsTable;
        $this->translator = $translator;
    }
    
    public function init()
    {
        $data = array();
        $this->setEmptyOption($this->translator->translate('Please select ...'));
        
        $langs = $this->langsTable->fetchAll();
        foreach ($langs as $lang){
            $data[$lang->getId()] = $lang->getLanguage();
        }

        $this->setValueOptions($data);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
