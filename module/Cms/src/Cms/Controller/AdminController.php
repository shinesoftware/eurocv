<?php
namespace Cms\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Cms\Form\PageForm;
use Cms\Form\PageFilter;

use Cms\Model\Page;
use Cms\Model\PageTable;

class AdminController extends AbstractActionController
{
    public function indexAction()
    {
        // check if the user is logged
        $authService = $this->getServiceLocator()->get('AuthService');
        $userEmail = $authService->getStorage()->read();
        if(empty($userEmail)){
            return $this->redirect()->toRoute('users/login' , array(
                    'action' =>  'index'
            ));
        }
        
    	$this->layout('layout/admin');
    	    	 
		$pageTable = $this->getServiceLocator()->get('PageTable');
		$viewModel  = new ViewModel(array('cms' => $pageTable->fetchAll())); 
		return $viewModel; 
    }
    
    public function addAction()
    {
    	$this->layout('layout/admin');
    	    	 
		$form = $this->getServiceLocator()->get('PageForm');
		$viewModel  = new ViewModel(array('form' => $form));
		$viewModel->setTemplate('cms/admin/edit');
		return $viewModel; 
    }

    public function editAction()
    {
    	$this->layout('layout/admin');
    	
    	$pageTable = $this->getServiceLocator()->get('PageTable');

    	$page = $pageTable->getPage($this->params()->fromRoute('id')); 
		$form = $this->getServiceLocator()->get('PageForm');
		$form->bind($page);
    	$viewModel  = new ViewModel(array('form' => $form, 'id' => $this->params()->fromRoute('id')));
    	$viewModel->setTemplate('cms/admin/edit');
    	return $viewModel;    	 
    }
    
    public function processAction()
    {
    	$this->layout('layout/admin');
    	 
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute('cms/admin', array('action' => 'edit'));
        }

        $post = $this->request->getPost();
        $pageTable = $this->getServiceLocator()->get('PageTable');   

        if(!empty($post->id)){
        	$page = $pageTable->getPage($post->id);
        }else{
            $page = new Page();
	        $page->exchangeArray($post);
	        $pageTable = $this->getServiceLocator()->get('PageTable');
	        $pageTable->savePage($page);
	        return $this->redirect()->toRoute('cms/admin');
        }

        $form = $this->getServiceLocator()->get('PageForm');
		$form->bind($page);	
        $form->setData($post);
        
        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form'  => $form,
            ));
            $model->setTemplate('cms/admin/edit');
            return $model;
        }
		
        // Save contact
        $this->getServiceLocator()->get('PageTable')->savePage($page);
        
        return $this->redirect()->toRoute('cms/admin');
    }
    
    public function deleteAction()
    {
    	$this->layout('layout/admin');
    	$this->getServiceLocator()->get('PageTable')
				    				->deletePage($this->params()
				    				->fromRoute('id'));
    	return $this->redirect()
    						->toRoute('cms/admin');
    	 
    }

}
