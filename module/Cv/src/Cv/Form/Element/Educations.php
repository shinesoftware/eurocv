<?php
namespace Cv\Form\Element;

use Cv\Model\EtTable;
use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\I18n\Translator\Translator;

class Educations extends Select implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $translator;
    protected $etTable;
    protected $personaldata_id;
    
    public function __construct(EtTable $etTable, $idPersonaldata, \Zend\Mvc\I18n\Translator $translator){
        parent::__construct();
        $this->etTable = $etTable;
        $this->personaldata_id = $idPersonaldata;
        $this->translator = $translator;
    }
    
    public function init()
    {
        $data = array();
        $this->setEmptyOption($this->translator->translate('Please select ...'));
        
        $educations = $this->etTable->fetchAllbyId($this->personaldata_id);
        foreach ($educations as $et){
            $data[$et->getId()] = $et->getTitle();
        }

        $this->setValueOptions($data);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
