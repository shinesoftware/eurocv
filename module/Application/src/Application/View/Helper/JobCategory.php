<?php 
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class JobCategory extends AbstractHelper implements ServiceLocatorAwareInterface  
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function __invoke($value)
    {
        if(!is_null($value) && is_numeric($value))
            $serviceLocator = $this->getServiceLocator()->getServiceLocator();
            if(!empty($serviceLocator)){
                $jobCategory = $serviceLocator->get('JobCategoryTable');
               
                $translator = $serviceLocator->get('translator');
                
                $category = $jobCategory->getJobCategory($value);
                return $translator->translate($category->getCategory());
            }
        
        return null;   
    }
} 