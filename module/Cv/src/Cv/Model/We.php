<?php
namespace Cv\Model;

class We
{

    public $id;

    public $startdate;

    public $enddate;

    public $employer;

    public $sector;

    public $position;

    public $activities;

    public $personaldata_id;

    public $jobtermination_id;

    public $translation_id;

    public $visible;

    public $sort;

    public $terminationemployment;

    public $jobcategory_id;

    public $updatedat;

    public $createdat;

    /**
     *
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     *
     * @return the $startdate
     */
    public function getStartdate ()
    {
        return $this->startdate;
    }

    /**
     *
     * @return the $enddate
     */
    public function getEnddate ()
    {
        return $this->enddate;
    }

    /**
     *
     * @return the $employer
     */
    public function getEmployer ()
    {
        return $this->employer;
    }

    /**
     *
     * @return the $sector
     */
    public function getSector ()
    {
        return $this->sector;
    }

    /**
     *
     * @return the $position
     */
    public function getPosition ()
    {
        return $this->position;
    }

    /**
     *
     * @return the $activities
     */
    public function getActivities ()
    {
        return $this->activities;
    }

    /**
     *
     * @return the $personaldata_id
     */
    public function getPersonaldataId ()
    {
        return $this->personaldata_id;
    }

    /**
     *
     * @return the $translation_id
     */
    public function getTranslationId ()
    {
        return $this->translation_id;
    }

    /**
     *
     * @return the $visible
     */
    public function getVisible ()
    {
        return $this->visible;
    }

    /**
     *
     * @return the $sort
     */
    public function getSort ()
    {
        return $this->sort;
    }

    /**
     *
     * @return the $terminationemployment
     */
    public function getTerminationemployment ()
    {
        return $this->terminationemployment;
    }

    /**
     *
     * @return the $jobcategory_id
     */
    public function getjobcategoryId ()
    {
        return $this->jobcategory_id;
    }

    /**
     *
     * @return the $updatedat
     */
    public function getUpdatedat ()
    {
        return $this->updatedat;
    }

    /**
     *
     * @return the $createdat
     */
    public function getCreatedat ()
    {
        return $this->createdat;
    }

    /**
     *
     * @param field_type $id            
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @param field_type $startdate            
     */
    public function setStartdate ($startdate)
    {
        $this->startdate = $startdate;
    }

    /**
     *
     * @param field_type $enddate            
     */
    public function setEnddate ($enddate)
    {
        $this->enddate = $enddate;
    }

    /**
     *
     * @param field_type $employer            
     */
    public function setEmployer ($employer)
    {
        $this->employer = $employer;
    }

    /**
     *
     * @param field_type $sector            
     */
    public function setSector ($sector)
    {
        $this->sector = $sector;
    }

    /**
     *
     * @param field_type $position            
     */
    public function setPosition ($position)
    {
        $this->position = $position;
    }

    /**
     *
     * @param field_type $activities            
     */
    public function setActivities ($activities)
    {
        $this->activities = $activities;
    }

    /**
     *
     * @param field_type $personaldata_id            
     */
    public function setPersonaldataId ($personaldata_id)
    {
        $this->personaldata_id = $personaldata_id;
    }

    /**
     *
     * @param field_type $translation_id            
     */
    public function setTranslationId ($translation_id)
    {
        $this->translation_id = $translation_id;
    }

    /**
     *
     * @param field_type $visible            
     */
    public function setVisible ($visible)
    {
        $this->visible = $visible;
    }

    /**
     *
     * @param field_type $sort            
     */
    public function setSort ($sort)
    {
        $this->sort = $sort;
    }

    /**
     *
     * @param field_type $terminationemployment            
     */
    public function setTerminationemployment ($terminationemployment)
    {
        $this->terminationemployment = $terminationemployment;
    }

    /**
     *
     * @param field_type $jobcategory_id            
     */
    public function setjobcategoryId ($jobcategory_id)
    {
        $this->jobcategory_id = $jobcategory_id;
    }

    /**
     *
     * @param field_type $updatedat            
     */
    public function setUpdatedat ($updatedat)
    {
        $this->updatedat = $updatedat;
    }

    /**
     *
     * @param field_type $createdat            
     */
    public function setCreatedat ($createdat)
    {
        $this->createdat = $createdat;
    }

    /**
     *
     * @return the $jobtermination_id
     */
    public function getJobterminationId ()
    {
        return $this->jobtermination_id;
    }

    /**
     *
     * @param field_type $jobtermination_id            
     */
    public function setJobterminationId ($jobtermination_id)
    {
    	if(!empty($jobtermination_id)){
        	$this->jobtermination_id = $jobtermination_id;
    	}
    }

    /**
     * This method get the array posted and assign the values to the table
     * object
     *
     * @param array $data            
     */
    public function exchangeArray ($data)
    {
        foreach ($data as $field => $value) {
            $this->$field = (isset($value)) ? $value : null;
        }
        
        return true;
    }
}