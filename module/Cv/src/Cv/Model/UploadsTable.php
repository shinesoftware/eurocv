<?php
namespace Cv\Model;
use Zend\Db\Sql\Select;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator\ClassMethods;

class UploadsTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Save the uploaded data
     *
     * @param Uploads $upload            
     * @throws \Exception
     */
    public function saveUpload (Uploads $upload)
    {
        $hydrator = new ClassMethods(true);
        
        // extract the data from the object
        $data = $hydrator->extract($upload);
        
        $id = (int) $upload->getId();
        
        $personaldata_id = (int) $upload->personaldata_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getUpload($id, $personaldata_id)) {
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
            } else {
                throw new \Exception('Upload ID does not exist');
            }
        }
        
        return $this->getUpload($id, $personaldata_id);
    }

    /**
     * Get all the records
     *
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Get the uploaded documents of the user
     *
     * @param integer $uploadId            
     * @param integer $personaldata_id            
     * @throws \Exception
     * @return mixed
     */
    public function getUpload ($uploadId, $personaldata_id)
    {
        $uploadId = (int) $uploadId;
        
        $rowset = $this->tableGateway->select(array ( 
                'id' => $uploadId, 
                'personaldata_id' => $personaldata_id
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $uploadId");
        }
        return $row;
    }

    /**
     * Get the uploaded documents of the user
     *
     * @param integer $personaldata_id            
     * @throws \Exception
     * @return mixed
     */
    public function getUploadedFiles ($personaldata_id)
    {
        $rowset = $this->tableGateway->select(array ( 
                'personaldata_id' => $personaldata_id
        ));
        
        return $rowset;
    }

    /**
     * Get the uploaded documents of the user
     *
     * @param integer $uploadId            
     * @param integer $personaldata_id            
     * @throws \Exception
     * @return mixed
     */
    public function getUploadByName ($filename)
    {
        $rowset = $this->tableGateway->select(array ( 
                'codedfilename' => $filename
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $uploadId");
        }
        return $row;
    }

    /**
     * Delete the record
     *
     * @param integer $uploadId            
     * @param integer $personaldata_id            
     */
    public function deleteUpload ($uploadId, $personaldata_id)
    {
        $this->tableGateway->delete(array ( 
                'id' => $uploadId, 
                'personaldata_id' => $personaldata_id
        ));
    }

    /**
     * Delete all the documents of the user
     *
     * @param integer $personaldata_id            
     */
    public function deleteUploads ($personaldata_id)
    {
        $uploads = $this->getUploadedFiles($personaldata_id);
        foreach($uploads as $upload){
            @unlink(PUBLIC_PATH . "/" . $upload->getCodedFilename());
        }
        
        $this->tableGateway->delete(array ( 
                'personaldata_id' => $personaldata_id
        ));
    }

    /**
     * Uploads for the user
     */
    public function getUploads ($personaldata_id, $translation_id = null, $workexperience_id = null, $education_id = null, $language_id = null)
    {
        if (! empty($personaldata_id) && is_numeric($personaldata_id)) {
            $criteria['uploads.personaldata_id'] = $personaldata_id;
        } else {
            return false;
        }
        
        if (! empty($translation_id) && is_numeric($translation_id)) {
            $criteria['uploads.translation_id'] = $translation_id;
        }
        
        if (! empty($language_id) && is_numeric($language_id)) {
            $criteria['language_id'] = $language_id;
        }
        
        if (! empty($workexperience_id) && is_numeric($workexperience_id)) {
            $criteria['workexperience_id'] = $workexperience_id;
        }
        
        if (! empty($education_id) && is_numeric($education_id)) {
            $criteria['education_id'] = $education_id;
        }
        
        $select = $this->tableGateway->getSql()
            ->select()
            ->join('translation', 'uploads.translation_id = translation.id', array ( 
                'translation' => 'title', 
                'code'
        ), 'left')
            ->join('work_experiences', 'uploads.workexperience_id = work_experiences.id', array ( 
                'employer'
        ), 'left')
            ->join('education', 'uploads.education_id = education.id', array ( 
                'organisation'
        ), 'left')
            ->where($criteria);
        
        $rowset = $this->tableGateway->selectWith($select);
        $rowset->buffer();
        
        return $rowset;
    }
}
