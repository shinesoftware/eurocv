<?php
namespace Cv\Form;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;
use \Cv\Hydrator\Strategy\DateTimeStrategy;

class WeForm extends Form
{

    public function init ()
    {
        $hydrator = new ClassMethods(true);
        $hydrator->addStrategy('startdate', new DateTimeStrategy());
        $hydrator->addStrategy('enddate', new DateTimeStrategy());
        
        $this->setAttribute('method', 'post');
        $this->setHydrator($hydrator)->setObject(new \Cv\Model\We());

        $this->add(array ( 
//                 'type' => 'Zend\Form\Element\Date', 
                'name' => 'startdate', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Start date'), 
                        'format' => 'd/m/Y'
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
//                 'type' => 'Zend\Form\Element\Date', 
                'name' => 'enddate', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('End date'), 
                        'format' => 'd/m/Y'
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'type' => 'Zend\Form\Element\Select', 
                'name' => 'visible', 
                'attributes' => array ( 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Visible'), 
                        'value_options' => array ( 
                                '1' => _('Visible'),
                        		'0' => _('Not Visible'),
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'sort', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Sort order')
                )
        ));
        
        $this->add(array ( 
                'name' => 'employer', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Name and address of employer')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'sector', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Type of business or sector')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'position', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Occupation or sort held')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'activities', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control', 
                        'rows' => 10
                ), 
                'options' => array ( 
                        'label' => _('Main activities and responsibilities')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'employer', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Name and address of employer')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'terminationemployment', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Reason of the employement termination')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'type' => 'Cv\Form\Element\JobCategories', 
                'name' => 'jobcategory_id', 
                'attributes' => array ( 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Job Categories')
                )
        ));
        
        $this->add(array ( 
                'type' => 'Cv\Form\Element\JobTerminations', 
                'name' => 'jobtermination_id', 
                'attributes' => array ( 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Reason of employment end')
                )
        ));
        
        $this->add(array ( 
                'name' => 'submit', 
                'attributes' => array ( 
                        'type' => 'submit', 
                        'class' => 'btn btn-success', 
                        'value' => _('Save')
                )
        ));
        
        $this->add(array ( 
                'name' => 'id', 
                'attributes' => array ( 
                        'type' => 'hidden'
                )
        ));
    }
}