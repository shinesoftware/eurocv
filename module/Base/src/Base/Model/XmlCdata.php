<?php
/**
 * This Class extends SimpleXMLElement to write CDATA-Nodes and print a formated xml-string
 *
 * to add cdata do it like this:
 * ($node is an instance of Not_So_Simple_XML)
 * $node->title = NULL;
 * $node->title->addCData( );
 *
 * @author David Naber <kontakt@dnaber.de>
 * @version 2012.08.28
 * @url https://gist.github.com/3501167
 * @link http://coffeerings.posterous.com/php-simplexml-and-cdata
 */

namespace Base\Model;

class XmlCdata extends \SimpleXMLElement {

	/**
	 * adds a cdata node
	 *
	 * @param  string $cdata_text
	 * @return void
	 */
	public function addCdata( $cdata_text ) {
		$node = dom_import_simplexml( $this );
		$no = $node->ownerDocument;
		$node->appendChild( $no->createCDATASection( $cdata_text ) );
	}

	/**
	 * returns a formatet xml string
	 *
	 * @return string
	 */
	public function as_formated_xml() {

		$xml_string = $this->asXML();
		$dom = new \DOMDocument( '1.0', 'UTF-8' );
		$dom->loadXML( $xml_string );
		$dom->preserveWhiteSpace = FALSE;
		$dom->formatOutput = TRUE;

		return $dom->saveXML();
	}
}
