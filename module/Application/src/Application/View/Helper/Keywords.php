<?php 
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;

class Keywords extends AbstractHelper
{
    public function __invoke($value)
    {
        if(!is_null($value)){
            $keywords = explode(',', $value);
            return $this->view->render('application/partial/keywords', array('keywords' => $keywords));
        }
    }
}