<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Cms;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Cms\Model\Page;
use Cms\Model\PageTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module implements AutoloaderProviderInterface
{

    public function getAutoloaderConfig ()
    {
        return array(
                'Zend\Loader\ClassMapAutoloader' => array(
                        __DIR__ . '/autoload_classmap.php'
                ),
                'Zend\Loader\StandardAutoloader' => array(
                        'namespaces' => array(
                                __NAMESPACE__ => __DIR__ . '/src/' .
                                         str_replace('\\', '/', __NAMESPACE__)
                        )
                )
        );
    }

    /**
     * Set the Services Manager items
     */
    public function getServiceConfig ()
    {
        return array(
                'abstract_factories' => array(),
                'aliases' => array(),
                'factories' => array(
                        // DB
                        'PageTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('PageTableGateway');
                            $table = new PageTable($tableGateway);
                            return $table;
                        },
                        // DB
                        'PageTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Page());
                            return new TableGateway('page', $dbAdapter, null, $resultSetPrototype);
                        },
                        // FORMS
                        'PageEditForm' => function  ($sm)
                        {
                            $form = new \Cms\Form\PageEditForm();
                            $form->setInputFilter($sm->get('PageEditFilter'));
                            return $form;
                        },
                        'PageForm' => function ($sm) { 
                            $form = new \Cms\Form\PageForm();
                            return $form;
                        },
                        // FILTERS
                        'PageFilter' => function  ($sm)
                        {
                            return new \Cms\Form\PageFilter();
                        },
                        'PageEditFilter' => function  ($sm)
                        {
                            return new \Cms\Form\PageEditFilter();
                        },
                ),
                'invokables' => array(),
                'services' => array(),
                'shared' => array()
        );
    }

    public function getConfig ()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap (MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }
}
