<?php 

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class SearchController extends AbstractActionController
{

    protected $translator;
    
    /**
     * OnDispatch of the controller
     * (non-PHPdoc)
     * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
     */
    public function onDispatch(\Zend\Mvc\MvcEvent $e){
        $this->translator = $e->getApplication()->getServiceManager()->get('translator');
        return parent::onDispatch( $e );
    }
        
    public function indexAction()
    {
        $results = array();
        
        $search = $this->params()->fromRoute('id');
        
        if(empty($search)){
            return json_encode(array());
        }
        
        // Get the TableGateway object to retrieve the data
        $pd = $this->getServiceLocator()->get('PdTable');
        
        // Get the users by lastname
        $users = $pd->getPdByCustomFieldname('lastname', "$search%");
        
        foreach ($users as $user){
            if($user->getPublic() ){
                $results[] = array(
                        'icon' => 'fa-user',
                        'keywords' => $user->getKeywords() ,
                        'value' => $user->getLastname() . " " . $user->getFirstname(),
                        'url' => "http://" . $user->getPac() . ".eurocv.eu"
                );
            }
        }
        
        // Get the users by lastname
        $users = $pd->getPdByCustomFieldname('keywords', "%$search%");
        
        foreach ($users as $user){
            if($user->getPublic() ){
                $results[] = array(
                        'icon' => 'fa-key',
                        'keywords' => $user->getKeywords() ,
                        'value' => $user->getLastname() . " " . $user->getFirstname(),
                        'url' => "http://" . $user->getPac() . ".eurocv.eu"
                );
            }
        }
        
        $retval = new JsonModel($results);

        return $retval;
    }
}
