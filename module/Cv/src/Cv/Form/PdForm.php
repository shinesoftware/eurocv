<?php
namespace Cv\Form;
use Zend\Form\Form;

class PdForm extends Form
{

    public function init ()
    {
        // This is a fieldset
        $this->add(array(
                'name' => 'pd',
                'type' => 'Cv\Form\Fieldset\PdFieldset',
                'object' => '\Cv\Model\Pd',
                'options' => array(
                        'use_as_base_fieldset' => true,
                ),
        ));
        
        $this->add(array ( 
                'name' => 'submit', 
                'attributes' => array ( 
                        'type' => 'submit', 
                        'class' => 'btn btn-success', 
                        'value' => _('Save')
                )
        ));
        
    }
	

}