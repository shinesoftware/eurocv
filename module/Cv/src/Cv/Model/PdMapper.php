<?php
namespace Cv\Model;

use Cv\Model\Pd;
use Cv\Model\Addresses;
use Cv\Model\Contacts;

class PdMapper
{
    protected $pd;
    protected $address;
    protected $contact;

    public function __construct(PdTable $personaldata, AddressesTable $address, ContactsTable $contact)
    {
        $this->pd = $personaldata;
        $this->address = $address;
        $this->contact = $contact;
    }

    /**
     * Get all the users including all the foreign record information
     * 
     * @return \Cv\Model\Pd;
     */
    public function findAll()
    {
        $users = $this->pd->getTableGateway()->select();
        $users->buffer();

        foreach ($users as $user) {
            $addresses = $this->address->getTableGateway()->select(array('personaldata_id' => $user->id));
            
            $select = $this->contact->tableGateway->getSql()->select()
                                                            ->join('contact_types', 'contacts.type_id = contact_types.id', array (
                                                                    'type',
                                                            ), 'left')
                                                            ->where(array (
                                                                    'personaldata_id' => $user->id
                                                            ));
                                                            
            $contacts = $this->contact->tableGateway->selectWith($select);
            
            $user->setAddresses(iterator_to_array($addresses));
            $user->setContacts(iterator_to_array($contacts));
        }

        return $users;
    }

    /**
     * Get a user by the user_id
     * @param integer $userId
     * @return \Cv\Model\Pd
     */
    public function findUser($fieldname, $value)
    {
        $user = $this->pd->getTableGateway()->select(array ( 
                $fieldname => $value
        ));
        
        $row = $user->current();
        
        if(!empty ($row) && $row->getId()){
            $select = $this->address->getTableGateway()->getSql()->select()
                                                            ->join(array('c' => 'country'), 'addresses.country_id = c.id', array ('countrycode' => 'code', 'country'), 'left')
                                                            ->where(array (
                                                                    'personaldata_id' => $row->getId()
                                                            ));
                                                            
            $addresses = $this->address->getTableGateway()->selectWith($select);

            $select = $this->contact->getTableGateway()->getSql()->select()
                                                            ->join(array('ct' => 'contact_types'), 'contacts.type_id = ct.id', array ('type',), 'left')
                                                            ->where(array (
                                                                    'personaldata_id' => $row->getId()
                                                            ));
                                                            
            $contacts = $this->contact->getTableGateway()->selectWith($select);
                                                            
            $row->setAddresses(iterator_to_array($addresses));
            $row->setContacts(iterator_to_array($contacts));
        }
        
        if (! $row) {
            return false;
        }
        return $row;
    }
}