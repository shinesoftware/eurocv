<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonAdmin for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array ( 
        'router' => array ( 
                'routes' => array ( 
                        'zfcadmin' => array(
                                'child_routes' => array(
                                        'users' => array(
                                                'type' => 'literal',
                                                'options' => array(
                                                        'route' => '/users',
                                                        'defaults' => array(
                                                                'controller' => 'Admin\Controller\Users',
                                                                'action'     => 'index',
                                                        ),
                                                ),
                                                'may_terminate' => true,
                                                'child_routes' => array (
                                                        'default' => array (
                                                                'type' => 'Segment',
                                                                'options' => array (
                                                                        'route' => '/[:action[/:id]]',
                                                                        'constraints' => array (
                                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                                'id' => '[0-9]*'
                                                                        ),
                                                                        'defaults' => array ()
                                                                )
                                                        )
                                                ),
                                        ),
                                ),
                        ),
                )
        ), 
        
        'navigation' => array(
                'admin' => array(
                        'users' => array(
                                'label' => _('Users'),
                                'route' => 'zfcadmin/users',
                        ),
                ),
        ),
        
        'controllers' => array ( 
                'invokables' => array ( 
                        'Admin\Controller\Users' => 'Admin\Controller\UsersController', 
                )
        ), 
        'view_helpers' => array ( 
                'invokables' => array ( 
                    'jobcategorysummary' => 'Admin\View\Helper\JobCategorySummary',
                    'photo' => 'Admin\View\Helper\Photo',
                )
        ), 
        'view_manager' => array ( 
                'display_not_found_reason' => true, 
                'display_exceptions' => true, 
                'doctype' => 'HTML5', 
                'not_found_template' => 'error/404', 
                'exception_template' => 'error/index', 
                'template_map' => array ( 
                        'admin/index/index' => __DIR__ . '/../view/admin/index/index.phtml', 
                        'error/404' => __DIR__ . '/../view/error/404.phtml', 
                        'error/index' => __DIR__ . '/../view/error/index.phtml',
                        'zfc-datagrid/renderer/bootstrapTable/layout' => __DIR__ . '/../view/zfc-datagrid/renderer/bootstrapTable/layout.phtml',
                ), 
                'template_path_stack' => array ( 
                        __DIR__ . '/../view'
                )
        ), 
        // Placeholder for console routes
        'console' => array ( 
                'router' => array ( 
                        'routes' => array ()
                )
        )
);
