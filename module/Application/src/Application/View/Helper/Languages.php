<?php 
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Languages extends AbstractHelper implements ServiceLocatorAwareInterface  
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function __invoke($id)
    {
    	if(!is_numeric($id))
    		return null;
    	
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();
        $langsTable = $serviceLocator->get('LangsTable');
        $translator = $serviceLocator->get('translator');
        $language = $langsTable->getLanguage($id);
        
        return $translator->translate($language->getLanguage());
                
    }
}