<?php
namespace Cv\Model;

use Cv\Model\Pd;
use Cv\Model\Addresses;
use Cv\Model\Contacts;

class CommonData
{
    protected $pd;
    protected $address;
    protected $contact;
    protected $we;
    protected $et;
    protected $ol;
    protected $cs;

    public function __construct(PdTable $personaldata, AddressesTable $address, ContactsTable $contact, WeTable $we, EtTable $et, OlTable $ol, CsTable $cs)
    {
        $this->pd = $personaldata;
        $this->address = $address;
        $this->contact = $contact;
        $this->we = $we;
        $this->et = $et;
        $this->ol = $ol;
        $this->cs = $cs;
    }
    
    /**
     * Check if the curriculum has been written in more than one language
     */
    public function isMultilanguage($personaldataId){

    	$languageIds = array();
    	
        if(empty($personaldataId)){
            return false;
        }
        
        // Check the Work Experiences
        $records = $this->we->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($personaldataId){
            $select->columns(array('translation_id'));
            $select->where(array('personaldata_id' => $personaldataId));
            $select->group('translation_id');
            $select->join('translation', 'translation_id = translation.id', array ('title', 'code'), 'left');
        });

        if($records->count()){
            foreach ($records as $item){
                $languageIds[$item->getTranslationId()]['title'] = $item->title;
                $languageIds[$item->getTranslationId()]['code'] = $item->code;
            }
        }
        
        // Check the Education 
        $records = $this->et->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($personaldataId){
            $select->columns(array('translation_id'));
            $select->where(array('personaldata_id' => $personaldataId));
            $select->group('translation_id');
            $select->join('translation', 'translation_id = translation.id', array ('title', 'code'), 'left');
        });

        if($records->count()){
            foreach ($records as $item){
                $languageIds[$item->getTranslationId()]['title'] = $item->title;
                $languageIds[$item->getTranslationId()]['code'] = $item->code;
            }
        }
        
        // Check the Languages
        $records = $this->ol->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($personaldataId){
            $select->columns(array('translation_id'));
            $select->where(array('personaldata_id' => $personaldataId));
            $select->group('translation_id');
            $select->join('translation', 'translation_id = translation.id', array ('title', 'code'), 'left');
        });

        if($records->count()){
            foreach ($records as $item){
                $languageIds[$item->getTranslationId()]['title'] = $item->title;
                $languageIds[$item->getTranslationId()]['code'] = $item->code;
            }
        }
        
        // Check the Compentences and Skills
        $records = $this->cs->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($personaldataId){
            $select->columns(array('translation_id'));
            $select->where(array('personaldata_id' => $personaldataId));
            $select->group('translation_id');
            $select->join('translation', 'translation_id = translation.id', array ('title', 'code'), 'left');
        });

        if($records->count()){
            foreach ($records as $item){
                $languageIds[$item->getTranslationId()]['title'] = $item->title;
                $languageIds[$item->getTranslationId()]['code'] = $item->code;
            }
        }
        return $languageIds;
    }


    /**
     * Get summary of the users grouped by JobCategories
     *
     * @throws \Exception
     * @return Row
     */
    public function getJobCategorySummary ()
    {
        $records = array();
        
        $sql = new \Zend\Db\Sql\Sql($this->pd->getTableGateway()->getAdapter());
        $select = $sql->select('personal_data');
        $select->columns(array('users' => new \Zend\Db\Sql\Expression('COUNT(*)')));
        $select->group('jobcategory_id');
        $select->where('jobcategory_id IS NOT NULL');
        $select->join('job_category', 'jobcategory_id = job_category.id', array ('category'), 'left');
        
        $statement = $this->pd->getTableGateway()->getAdapter()->createStatement();
        $select->prepareStatement($this->pd->getTableGateway()->getAdapter(), $statement);
        $results = $statement->execute();
        
        $rows = array();
        if ($results->count()) {
            $rows = new \Zend\Db\ResultSet\ResultSet();
            $records = $rows->initialize($results)->toArray();
        }
        return $records;
    }
    
    /**
     * Get all the users including all the foreign record information
     * 
     * @return \Cv\Model\Pd;
     */
    public function findAll()
    {
        $users = $this->pd->getTableGateway()->select();
        $users->buffer();

        foreach ($users as $user) {
            $addresses = $this->address->getTableGateway()->select(array('personaldata_id' => $user->id));
            
            $select = $this->contact->tableGateway->getSql()->select()
                                                            ->join('contact_types', 'contacts.type_id = contact_types.id', array (
                                                                    'type',
                                                            ), 'left')
                                                            ->where(array (
                                                                    'personaldata_id' => $user->id
                                                            ));
                                                            
            $contacts = $this->contact->tableGateway->selectWith($select);
            
            $user->setAddresses(iterator_to_array($addresses));
            $user->setContacts(iterator_to_array($contacts));
        }

        return $users;
    }

    /**
     * Get a user by the user_id
     * @param integer $userId
     * @return \Cv\Model\Pd
     */
    public function findUser($fieldname, $value)
    {
        $user = $this->pd->getTableGateway()->select(array ( 
                $fieldname => $value
        ));
        
        $row = $user->current();
        
        if(!empty ($row) && $row->getId()){
            $select = $this->address->getTableGateway()->getSql()->select()
                                                            ->join(array('c' => 'country'), 'addresses.country_id = c.id', array ('countrycode' => 'code', 'country'), 'left')
                                                            ->where(array (
                                                                    'personaldata_id' => $row->getId()
                                                            ));
                                                            
            $addresses = $this->address->getTableGateway()->selectWith($select);

            $select = $this->contact->getTableGateway()->getSql()->select()
                                                            ->join(array('ct' => 'contact_types'), 'contacts.type_id = ct.id', array ('type',), 'left')
                                                            ->where(array (
                                                                    'personaldata_id' => $row->getId()
                                                            ));
                                                            
            $contacts = $this->contact->getTableGateway()->selectWith($select);
                                                            
            $row->setAddresses(iterator_to_array($addresses));
            $row->setContacts(iterator_to_array($contacts));
        }
        
        if (! $row) {
            return false;
        }
        return $row;
    }
}