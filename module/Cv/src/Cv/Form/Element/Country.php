<?php
namespace Cv\Form\Element;

use Cv\Model\CountryTable;

use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Country extends Select implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $countryTable;
    
    public function __construct(CountryTable $countryTable){
        parent::__construct();
        $this->countryTable = $countryTable;
    }
    
    public function init()
    {
        $data = array();
        
        $countries = $this->countryTable->fetchAll();
        foreach ($countries as $country){
            $data[$country->getId()] = $country->getCountry();
        }
        asort($data);
        $this->setValueOptions($data);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
