<?php
namespace Cv\Form\Element;

use Cv\Model\JobTerminationTable;
use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\I18n\Translator\Translator;

class JobTerminations extends Select implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $translator;
    protected $jobterminationTable;
    
    public function __construct(JobTerminationTable $jobterminationTable, \Zend\Mvc\I18n\Translator $translator){
        parent::__construct();
        $this->jobterminationTable = $jobterminationTable;
        $this->translator = $translator;
    }
    
    public function init()
    {
        $data = array();
        
        $jobterms = $this->jobterminationTable->fetchAll();

        $data[""] = ""; // adding an empty value
        foreach ($jobterms as $jobtermination){
            $data[$jobtermination->getId()] = $this->translator->translate($jobtermination->getName());
        }
        $this->setValueOptions($data);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
