<?php
namespace Cv\Model;

class Contacts
{
    protected $id;
    protected $personaldata_id;
    protected $type_id;
    protected $contact;
    protected $visible;

    /**
     * 
     * @param array $data
     */
    function exchangeArray ($data)
    {
        foreach ($data as $field => $value){
            $this->$field = (isset($value)) ? $value : null;
        }
    }
    
    /**
     * Get the keys of the array
     * @return multitype:
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $personaldata_id
     */
    public function getPersonaldataId ()
    {
        return $this->personaldata_id;
    }

	/**
     * @return the $type_id
     */
    public function getTypeId ()
    {
        return $this->type_id;
    }

	/**
     * @return the $visible
     */
    public function getVisible ()
    {
        return $this->visible;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $personaldata_id
     */
    public function setPersonaldataId ($personaldata_id)
    {
        $this->personaldata_id = $personaldata_id;
    }

	/**
     * @param field_type $type_id
     */
    public function setTypeId ($type_id)
    {
        $this->type_id = $type_id;
    }

	/**
     * @param field_type $visible
     */
    public function setVisible ($visible)
    {
        $this->visible = $visible;
    }
	/**
     * @return the $contact
     */
    public function getContact ()
    {
        return $this->contact;
    }

	/**
     * @param field_type $contact
     */
    public function setContact ($contact)
    {
        $this->contact = $contact;
    }


    
}