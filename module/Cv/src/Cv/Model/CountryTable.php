<?php
namespace Cv\Model;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class CountryTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCountry (Country $country)
    {
        $data = array(
                'code' => $country->code,
                'country' => $country->country,
                'password' => $country->password
        );
        
        $id = (int) $country->id;
        if ($id == 0) {
        	
            // we create a simple Uuid for the user
            $data['uuid'] = $this->Uuid();

            // add the record
            $this->tableGateway->insert($data);
        } else {
            if ($this->getCountry($id)) {
                if (empty($data['password'])) {
                    unset($data['password']);
                }
                $this->tableGateway->update($data, array(
                        'id' => $id
                ));
            } else {
                throw new \Exception('Country ID does not exist');
            }
        }
    }

    /**
     * Get all users
     * 
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Get Country by Id
     * 
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getCountry ($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get Last Country account
     * 
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getLastCountry ()
    {
        $rowset = $this->tableGateway->select(function (Select $select) {
		     $select->sort('id DESC')->limit(1);
		});
        
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find the record");
        }
        return $row;
    }

    /**
     * Get Country account by name
     * 
     * @param string $countryname            
     * @throws \Exception
     * @return Row
     */
    public function getCountryByName ($countryname)
    {
        $rowset = $this->tableGateway->select(array(
                'country' => $countryname
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $countryname");
        }
        return $row;
    }

    /**
     * Delete Country account by CountryId
     * 
     * @param string $id            
     */
    public function deleteCountry ($id)
    {
        $this->tableGateway->delete(array(
                'id' => $id
        ));
    }
}
