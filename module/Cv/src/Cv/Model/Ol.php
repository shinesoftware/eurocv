<?php
namespace Cv\Model;

class Ol
{

    public $id;

    public $lang_id;
    
    public $certificate;

    public $understanding_listening;

    public $understanding_reading;

    public $spoken_interaction;

    public $spoken_production;

    public $writing;

    public $personaldata_id;

    public $translation_id;

    public $visible;

    public $createdat;

    public $updatedat;

    /**
     * This method get the array posted and assign the values to the table
     * object
     *
     * @param array $data
     */
    public function exchangeArray ($data)
    {
    	foreach ($data as $field => $value) {
    		$this->$field = (isset($value)) ? $value : null;
    	}
    
    	return true;
    }
    
    
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $lang_id
     */
    public function getLangId ()
    {
        return $this->lang_id;
    }

	/**
     * @return the $understanding_listening
     */
    public function getUnderstandingListening ()
    {
        return $this->understanding_listening;
    }

	/**
     * @return the $understanding_reading
     */
    public function getUnderstandingReading ()
    {
        return $this->understanding_reading;
    }

	/**
     * @return the $spoken_interaction
     */
    public function getSpokenInteraction ()
    {
        return $this->spoken_interaction;
    }

	/**
     * @return the $spoken_production
     */
    public function getSpokenProduction ()
    {
        return $this->spoken_production;
    }

	/**
     * @return the $writing
     */
    public function getWriting ()
    {
        return $this->writing;
    }

	/**
     * @return the $personaldata_id
     */
    public function getPersonaldataId ()
    {
        return $this->personaldata_id;
    }

	/**
     * @return the $translation_id
     */
    public function getTranslationId ()
    {
        return $this->translation_id;
    }

	/**
     * @return the $visible
     */
    public function getVisible ()
    {
        return $this->visible;
    }

	/**
     * @return the $createdat
     */
    public function getCreatedat ()
    {
        return $this->createdat;
    }

	/**
     * @return the $updatedat
     */
    public function getUpdatedat ()
    {
        return $this->updatedat;
    }
    
    /**
     * @return the $certificate
     */
    public function getCertificate() {
    	return $this->certificate;
    }
    
    /**
     * @param field_type $certificate
     */
    public function setCertificate($certificate) {
    	$this->certificate = $certificate;
    }
    
	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $lang_id
     */
    public function setLangId ($lang_id)
    {
        $this->lang_id = $lang_id;
    }

	/**
     * @param field_type $understanding_listening
     */
    public function setUnderstandingListening ($understanding_listening)
    {
        $this->understanding_listening = $understanding_listening;
    }

	/**
     * @param field_type $understanding_reading
     */
    public function setUnderstandingReading ($understanding_reading)
    {
        $this->understanding_reading = $understanding_reading;
    }

	/**
     * @param field_type $spoken_interaction
     */
    public function setSpokenInteraction ($spoken_interaction)
    {
        $this->spoken_interaction = $spoken_interaction;
    }

	/**
     * @param field_type $spoken_production
     */
    public function setSpokenProduction ($spoken_production)
    {
        $this->spoken_production = $spoken_production;
    }

	/**
     * @param field_type $writing
     */
    public function setWriting ($writing)
    {
        $this->writing = $writing;
    }

	/**
     * @param field_type $personaldata_id
     */
    public function setPersonaldataId ($personaldata_id)
    {
        $this->personaldata_id = $personaldata_id;
    }

	/**
     * @param field_type $translation_id
     */
    public function setTranslationId ($translation_id)
    {
        $this->translation_id = $translation_id;
    }

	/**
     * @param field_type $visible
     */
    public function setVisible ($visible)
    {
        $this->visible = $visible;
    }

	/**
     * @param field_type $createdat
     */
    public function setCreatedat ($createdat)
    {
        $this->createdat = $createdat;
    }

	/**
     * @param field_type $updatedat
     */
    public function setUpdatedat ($updatedat)
    {
        $this->updatedat = $updatedat;
    }


}