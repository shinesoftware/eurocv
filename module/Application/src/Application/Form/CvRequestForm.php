<?php
namespace Application\Form;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;
use \Cv\Hydrator\Strategy\DateTimeStrategy;
use Zend\Form\Element;
use Zend\Captcha\AdapterInterface as CaptchaAdapter;

class CvRequestForm extends Form
{
    protected $captchaAdapter;
    protected $csrfToken;
    
    public function __construct($name = null, CaptchaAdapter $captchaAdapter = null)
    {
        parent::__construct($name);
    
        if (null !== $captchaAdapter) {
            $this->captchaAdapter = $captchaAdapter;
        }
    
        $this->init();
    }
    
    public function init ()
    {
        $this->setAttribute('method', 'post');
        
        $this->add(array ( 
                'name' => 'fullname', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Full Name')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'email', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('E-Mail')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'request', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control', 
                        'rows' => 10
                ), 
                'options' => array ( 
                        'label' => _('Write here the request to see the Curriculum Vitae')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $captcha = new Element\Captcha('captcha');
        $captcha->setCaptcha($this->captchaAdapter);
        $captcha->setOptions(array('label' => 'Please verify you are human.'));
        $this->add($captcha);
        
        $this->add(new Element\Csrf('csrf'));
        
        $this->add(array ( 
                'name' => 'submit', 
                'attributes' => array ( 
                        'type' => 'submit', 
                        'class' => 'btn btn-success', 
                        'value' => _('Send')
                )
        ));
    }
}