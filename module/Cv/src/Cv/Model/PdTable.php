<?php
namespace Cv\Model;

use Zend\Mail\Address;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Stdlib\Hydrator\ClassMethods;

class PdTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    /**
     * @return the $tableGateway
     */
    public function getTableGateway ()
    {
        return $this->tableGateway;
    }

    public function saveData (Pd $pd)
    {
        $hydrator = new ClassMethods(true);
        
        // extract the data from the object
        $data = $hydrator->extract($pd);
        
        unset($data['addresses']);
        unset($data['contacts']);
        
        $id = (int) $pd->getId();
        if ($id == 0) {
        	unset($data['id']);
        	$data['createdat'] = date('Y-m-d H:i:s');
            $data['updatedat'] = date('Y-m-d H:i:s');
            
            $this->tableGateway->insert($data); // add the record
            $id = $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->getPd($id)) {
                $data['updatedat'] = date('Y-m-d H:i:s');
                // exclude this field from the update task
                unset($data['download']); 
                unset($data['visits']); 
                unset($data['createdat']); 
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
            } else {
                throw new \Exception('Personal data ID does not exist');
            }
        }
        
        return $this->getPd($id);
    }

    /**
     * Set the user visits 
     * @param integer $personaldataId
     * @throws \Exception
     * @return Ambigous <\Zend\Text\Table\Row, mixed>
     */
    public function setVisits ($personaldataId)
    {
        $hydrator = new ClassMethods(true);
        
        if ($personaldataId ) {
            $pd = $this->getPd($personaldataId);
            
            // extract the data from the object
            $data = $hydrator->extract($pd);
            
            unset($data['addresses']);
            unset($data['contacts']);
            
            if ($data) {
                if(!is_numeric($data['visits'])){
                    $data['visits'] = 0;
                }
                
                $data['visits'] += 1;
                $this->tableGateway->update($data, array ( 
                        'id' => $personaldataId
                ));
            } else {
                throw new \Exception('Personal data ID does not exist');
            }
        }
        
        return $this->getPd($personaldataId);
    }

    /**
     * Set the download of the cv 
     * @param integer $personaldataId
     * @throws \Exception
     * @return Ambigous <\Zend\Text\Table\Row, mixed>
     */
    public function setDownload ($personaldataId)
    {
        $hydrator = new ClassMethods(true);
        
        if ($personaldataId ) {
            $pd = $this->getPd($personaldataId);
            
            // extract the data from the object
            $data = $hydrator->extract($pd);
            
            unset($data['addresses']);
            unset($data['contacts']);
            
            if ($data) {
                if(!is_numeric($data['download'])){
                    $data['download'] = 0;
                }
                
                $data['download'] += 1;
                $this->tableGateway->update($data, array ( 
                        'id' => $personaldataId
                ));
            } else {
                throw new \Exception('Personal data ID does not exist');
            }
        }
        
        return $this->getPd($personaldataId);
    }

    /**
     * Get all users
     *
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        $resultSet->buffer();
        return $resultSet;
    }

    /**
     * Get Personaldata account by id
     *
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getPd ($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array ( 
                'id' => $id
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get Personal data account by UserId
     *
     * @param integer $userId            
     * @throws \Exception
     * @return Row
     */
    public function getPdByUserId ($userId)
    {
        $rowset = $this->tableGateway->select(array ( 
                'user_id' => $userId
        ));
        $row = $rowset->current();
        
        if (! $row) {
            return false;
        }
        return $row;
    }

    /**
     * Get user data by a custom field
     *
     * @param string $fieldname            
     * @param string $value            
     * @throws \Exception
     * @return Row
     */
    public function getPdByCustomFieldname ($fieldname, $value)
    {
        if(empty($fieldname) || empty($value)){
            return false;
        }
        
        try{
            // Get the database adapter injected 
            $adapter = $this->tableGateway->getAdapter();

            // Create the "select" object
            $sql = new \Zend\Db\Sql\Sql($adapter);
            
            // Create the query
            $select = $sql->select();
            $select->from('personal_data');
            $select->where->like($fieldname, $value);
            $select->order(new \Zend\Db\Sql\Expression("RAND()"))->limit(10);
            
            // Prepare the statement for the SQL in order to avoid a potential SQL injection
            $statement = $sql->prepareStatementForSqlObject($select);
            
            // Execute the statement
            $result = $statement->execute();
            
            // Hydrate the result of the execution of the query in the "Pd" (Personal data) object
            if ($result instanceof \Zend\Db\Adapter\Driver\ResultInterface && $result->isQueryResult()) {
                $resultSet = new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\Reflection, new Pd());
                $resultSet->initialize($result);
            }
            
            return $resultSet;
            
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    /**
     * Delete Personaldata account by PersonaldataId
     *
     * @param string $id            
     */
    public function deletePd ($id)
    {
        $this->deletePhoto($id);
        $this->tableGateway->delete(array ( 
                'id' => $id
        ));
    }

    /**
     * Delete the photo of the user
     *
     * @param string $id            
     */
    public function deletePhoto ($id)
    {
        $user = $this->getPd($id);
        
        $matches = glob(PUBLIC_PATH . '/photos/' . $user->getPac() . "*");
        
        if(!empty($matches[0])){
        	$photo = $matches[0];
       		@unlink($photo);
        	return true;
        }
        
        return false;
    }

}
