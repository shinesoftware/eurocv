<?php 
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;
class Locale extends AbstractHelper implements ServiceLocatorAwareInterface  
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function __invoke()
    {
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();
        $session = new Container('base');
        $locale = "en_US";
        
        // Get the visitor language selection
        if(null != $session->offsetGet('locale')){
            $locale = $session->offsetGet('locale');  // Get the locale
        }
        
        $translation = $serviceLocator->get('TranslationTable');
        $locale = $translation->getTranslationbyLocale($locale)->getTitle();
        
        return $locale;
        
    }
}