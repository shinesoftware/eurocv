<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonBase for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array ( 
        'session' => array(
                'remember_me_seconds' => 2419200,
                'use_cookies' => true,
                'cookie_httponly' => true,
        ),
        'navigation' => array ( 
                'default' => array ( 
                        array ( 
                                'label' => 'Homepage', 
                                'route' => 'www',
                        ), 
                        array ( 
                                'label' => 'Admin', 
                                'route' => 'zfcadmin',
                                'resource' => 'adminmenu',
                                'privilege' => 'list',
                        ),
                        array (
                                'label' => 'Contact us',
                                'uri' => 'mailto:staff@eurocv.eu',
                        ),
                )
                
        ), 
        'service_manager' => array ( 
                'abstract_factories' => array ( 
                        'Zend\Cache\Service\StorageCacheAbstractServiceFactory', 
                        'Zend\Log\LoggerAbstractServiceFactory'
                ), 
                'aliases' => array ( 
                        'translator' => 'MvcTranslator'
                )
        ), 
        'translator' => array ( 
                'locale' => 'en_US', 
                'translation_file_patterns' => array ( 
                        array ( 
                                'type' => 'gettext', 
                                'base_dir' => __DIR__ . '/../language', 
                                'pattern' => '%s.mo',
                        )
                )
        ), 
        'controllers' => array ( 
                'invokables' => array ( 
                        'Base\Controller\Index' => 'Base\Controller\IndexController'
                )
        ), 
        'view_helpers' => array ( 
                'invokables' => array ( 
                        'socialSignInButton' => 'Base\View\Helper\SocialSignInButton'
                )
        ), 
        'view_manager' => array ( 
                'display_not_found_reason' => true, 
                'display_exceptions' => true, 
                'doctype' => 'HTML5', 
                'not_found_template' => 'error/404', 
                'exception_template' => 'error/index', 
                'template_map' => array ( 
                        'layout/layout' => __DIR__ . '/../view/layout/layout.phtml', 
                        'base/index/index' => __DIR__ . '/../view/base/index/index.phtml', 
                        'error/404' => __DIR__ . '/../view/error/404.phtml', 
                        'error/index' => __DIR__ . '/../view/error/index.phtml',
                        'phly-contact/contact/index'     => __DIR__ . '/../view/phly-contact/contact/index.phtml',
                        'phly-contact/contact/thank-you' => __DIR__ . '/../view/phly-contact/contact/thank-you.phtml',
                        'goalioforgotpassword' => __DIR__ . '/../view',
                        'error/403' => __DIR__ . '/../view/error/403.phtml',
                ), 
                'template_path_stack' => array ( 
                        __DIR__ . '/../view',
                )
        ), 
);
