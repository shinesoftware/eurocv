<?php
namespace Cv\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Cv\Model\Ol;
use Zend\Stdlib\Hydrator\ClassMethods;
use \Cv\Hydrator\Strategy\DateTimeStrategy;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Column\Style;
use ZfcDatagrid\Column\Formatter;
use ZfcDatagrid\Filter;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Session\Container;

/**
 * Work Experiences Controller
 * This controller gets the personal data of the user
 *
 * @author mturillo
 */
class OlController extends AbstractActionController
{

    protected $user;
    protected $personaldata;
    protected $versionId;
    
	/**
	 * OnDispatch of the controller
	 * (non-PHPdoc)
	 * 
	 * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
	 */
	public function onDispatch(\Zend\Mvc\MvcEvent $e) {
		
		$session = new Container('base');
		$this->versionId = $session->offsetGet('versionid') ? $session->offsetGet('versionid') : 2;
		
		// check the credencials
		if (! $this->zfcUserAuthentication ()->hasIdentity ()) {
			return $this->redirect ()->toRoute ( 'www/user' );
		}
		
		// Get the identity information
		$this->user = $this->zfcUserAuthentication ()->getIdentity ();
		
		// Get the TableGateway object to retrieve the data
		$user = $this->getServiceLocator ()->get ( 'PdTable' );
		
		// Get the user
		$myUser = $user->getPdByUserId ( $this->user->getId() );
		
		if (! empty ( $myUser )) {
			$this->personaldata = $myUser;
		}else{
		    $this->flashMessenger ()->setNamespace ( 'danger' )->addMessage ( 'You have to complete the personal data section.' );
		    return $this->redirect()->toRoute('cv/pd');
		}
		
		return parent::onDispatch ( $e );
	}
    
    public function addAction ()
    {

        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\OlForm');
        
        $viewModel = new ViewModel(array ( 
                'form' => $form
        ));
        
        $viewModel->setTemplate('cv/ol/index');
        return $viewModel;
    }

    public function editAction ()
    {
        $id = $this->params()->fromRoute('id');
        
        if (empty($id)) {
            return $this->redirect()->toRoute(NULL, array ( 
                    'controller' => 'ol', 
                    'action' => 'list'
            ));
        }
        
        $olTable = $this->getServiceLocator()->get('OlTable');
        
        $ol = $olTable->getOl($id, $this->personaldata->getId());
        if (! empty($ol)) {
            
            $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\OlForm');
            $form->bind($ol);
            
            $viewModel = new ViewModel(array ( 
                    'form' => $form, 
            		'pac' => $this->personaldata->getPac(),
                    'id' => $id
            ));
            $viewModel->setTemplate('cv/ol/index');
            
            return $viewModel;
        } else {
            return $this->redirect()->toRoute(NULL, array ( 
                    'controller' => 'ol', 
                    'action' => 'edit',
                    'id' => $id
            ));
        }
    }

    public function listAction ()
    {
    
        $grid = $this->createGrid();
        $grid->render();
    
        $response = $grid->getResponse();
    
        if ($grid->isHtmlInitReponse()) {
            $view = new ViewModel();
            $view->addChild($response, 'grid');
            return $view;
        } else {
            return $response;
        }
    }
    
    // Create the list grid
    private function createGrid ()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $select = new Select();
        $select->from(array ('ol' => 'languages'))
                        ->join(array('t' => 'translation'), 'ol.translation_id = t.id', array('name'), 'left')
                        ->join(array('l' => 'langs'), 'ol.lang_id = l.id', array('language'), 'left')
                        ->where(array ('personaldata_id' => $this->personaldata->getId()))
                        ->where(array ('translation_id' => $this->versionId));
    
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($select, $dbAdapter);
    
        $colId = new Column\Select('id', 'ol');
        $colId->setLabel('Id');
        $colId->setIdentity();
        $grid->addColumn($colId);
    
        $col = new Column\Select('language', 'l');
        $col->setLabel(_('Language'));
        $col->setWidth(15);
        $col->setUserFilterDisabled(true);
        $grid->addColumn($col);
    
        $col = new Column\Select('title', 't');
        $col->setLabel(_('Version'));
        $col->setUserFilterDisabled(true);
        $grid->addColumn($col);
    
        $col = new Column\Select('visible', 'ol');
        $col->setType(new \ZfcDatagrid\Column\Type\String());
        $col->setLabel(_('Visible'));
        $col->setUserFilterDisabled(true);
        $col->setTranslationEnabled(true);
        $col->setFilterSelectOptions(array (
                '' => '-',
                '0' => 'No',
                '1' => 'Yes'
        ));
        $col->setReplaceValues(array (
                '' => '-',
                '0' => 'No',
                '1' => 'Yes'
        ));
        $grid->addColumn($col);
    
        // Add actions to the grid
        $showaction = new Column\Action\Button();
        $showaction->setAttribute('href', "/cv/ol/edit/" . $showaction->getColumnValuePlaceholder(new Column\Select('id', 'ol')));
        $showaction->setAttribute('class', 'btn btn-xs btn-success');
        $showaction->setLabel(_('edit'));
    
        $delaction = new Column\Action\Button();
        $delaction->setAttribute('href', '/cv/ol/delete/' . $delaction->getRowIdPlaceholder());
        $delaction->setAttribute('onclick', "return confirm('Are you sure?')");
        $delaction->setAttribute('class', 'btn btn-xs btn-danger');
        $delaction->setLabel(_('delete'));
    
        $col = new Column\Action();
        $col->addAction($showaction);
        $col->addAction($delaction);
        $grid->addColumn($col);
    
        $grid->setToolbarTemplate('');
    
        return $grid;
    }

    public function processAction ()
    {
        if (! $this->request->isPost()) {
            
            return $this->redirect()->toRoute(NULL, array ( 
                    'controller' => 'ol', 
                    'action' => 'index'
            ));
        }
        
        $post = $this->request->getPost();
        
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\OlForm');
        $inputFilter = $this->getServiceLocator()->get('OlFilter');
        $form->setInputFilter($inputFilter);
        
        $form->setData($post);
        if (! $form->isValid()) {
            $model = new ViewModel(array ( 
                    'error' => true, 
            		'pac' => $this->personaldata->getPac(),
                    'form' => $form
            ));
            $model->setTemplate('cv/ol/index');
            return $model;
        }
        
        // Get the posted vars
        $ol = $form->getData();
        $ol->setPersonaldataId($this->personaldata->getId());
        $ol->setTranslationId($this->versionId);
        
        // Save the data in the database
        $OlTable = $this->getServiceLocator()->get('OlTable')->saveData($ol);
        
        $this->flashMessenger ()->setNamespace ( 'success' )->addMessage ( 'The information have been saved correctly.' );
        
        return $this->redirect()->toRoute(NULL, array ( 
                'controller' => 'ol', 
                'action' => 'edit', 
                'id' => $OlTable->getId()
        ));
    }

    /**
     * Delete the record
     */
    public function deleteAction ()
    {

        $id = $this->params()->fromRoute('id');
        
        if (empty($id)) {
            return $this->redirect()->toRoute(NULL, array (
                    'controller' => 'ol',
                    'action' => 'list'
            ));
        }
        
        $this->getServiceLocator()->get('OlTable')->deleteOl($id, $this->personaldata->getId());
        $this->flashMessenger ()->setNamespace ( 'success' )->addMessage ( 'The information have been deleted correctly.' );
        return $this->redirect()->toRoute(NULL, array (
                    'controller' => 'ol',
                    'action' => 'list'
            ));
    }
}