<?php
namespace Cv\Form;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;
use \Cv\Hydrator\Strategy\DateTimeStrategy;

class CsForm extends Form
{

    public function init ()
    {
        $hydrator = new ClassMethods;

        $this->setAttribute('method', 'post');
        $this->setHydrator($hydrator)->setObject(new \Cv\Model\Cs());
        
        $this->add(array ( 
                'name' => 'publications', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Publications')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'personal', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control',
                        'rows' => 10
                ), 
                'options' => array ( 
                        'label' => _('Personal skills and competences')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'social', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control',
                        'rows' => 10
                ), 
                'options' => array ( 
                        'label' => _('Social skills and competences')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'organisational', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control',
                        'rows' => 10
                ), 
                'options' => array ( 
                        'label' => _('Organisational skills and competences')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'technical', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control',
                        'rows' => 10
                ), 
                'options' => array ( 
                        'label' => _('Technical skills and competences')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'computer', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control',
                        'rows' => 10
                ), 
                'options' => array ( 
                        'label' => _('Computer skills and competences')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'artistic', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control',
                        'rows' => 10
                ), 
                'options' => array ( 
                        'label' => _('Artistic skills and competences')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'other', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Other skills and competences')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'driving', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Driving licence(s)')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'jobrelated', 
                'attributes' => array ( 
                        'type' => 'textarea', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Job-related skills')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array (
                'type' => 'Zend\Form\Element\Select',
                'name' => 'visible',
                'attributes' => array (
                        'class' => 'form-control'
                ),
                'options' => array (
                        'label' => _('Visible'),
                        'value_options' => array (
                                '0' => _('Not Visible'),
                                '1' => _('Visible')
                        )
                )
        ));

        $this->add(array ( 
                'name' => 'submit', 
                'attributes' => array ( 
                        'type' => 'submit', 
                        'class' => 'btn btn-success', 
                        'value' => _('Save')
                )
        ));
        $this->add(array (
                'name' => 'id',
                'attributes' => array (
                        'type' => 'hidden'
                )
        ));
    }
}