<?php 
namespace Cv\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;
class Versions extends AbstractHelper implements ServiceLocatorAwareInterface  
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function __invoke()
    {
    	$user = false;
    	$session = new Container('base');
    	$versionId = $session->offsetGet('versionid') ? $session->offsetGet('versionid') : 2;
    	$newVersion = array();
    	
    	$serviceLocator = $this->getServiceLocator()->getServiceLocator();
    	$auth = $serviceLocator->get('zfcuser_auth_service');
    	
        $translationversionsTable = $serviceLocator->get('TranslationTable');
        $translator = $serviceLocator->get('translator');
        $versions = $translationversionsTable->fetchAll();
        
        if ($auth->hasIdentity()) {
        	$userId = $auth->getIdentity()->getId();
        
        	$pdTable = $serviceLocator->get('PdTable');
        	$user = $pdTable->getPdByUserId($userId);
        }
        
        $getTotal = 0;
        $versionsWritten = array();
        
        // Translate the language title and sort the result
        foreach ($versions as $version){
            $titleTranslated = $translator->translate($version->getTitle());
            $newVersion[$version->getId()] = $titleTranslated;
            
            if($user){
            	// Get the Work Experience summary items
            	$weTable = $serviceLocator->get('WeTable');
            	$weSummary = $weTable->getSummary($user->getId(), $version->getId(), $getTotal);
            		 
            	// Get the Education & Training summary items
            	$etTable = $serviceLocator->get('EtTable');
            	$etSummary = $etTable->getSummary($user->getId(), $version->getId(), $getTotal);
            		 
            	// Get the Languages summary items
            	$olTable = $serviceLocator->get('OlTable');
            	$olSummary = $olTable->getSummary($user->getId(), $version->getId(), $getTotal);
            		 
            	// Get the Competences summary items
            	$csTable = $serviceLocator->get('CsTable');
            	$csSummary = $csTable->getSummary($user->getId(), $version->getId(), $getTotal);

            	if(!empty($weSummary[0]['items']) || $etSummary[0]['items'] || $olSummary[0]['items'] || $csSummary[0]['items'] ){
            		$versionsWritten[] = $version->getId();
            	}
            }
            
            if($versionId == $version->getId()){
            	$selected = $titleTranslated;
            }
        }

        // Sorting of the result
        asort($newVersion);
        
        return $this->view->render('cv/partial/versions', array('versions' => $newVersion, 'selected' => $selected, 'versionwritten' => $versionsWritten));
    }
}