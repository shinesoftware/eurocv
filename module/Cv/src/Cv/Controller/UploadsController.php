<?php
namespace Cv\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\Headers;
use Cv\Model\Uploads;
use Zend\Stdlib\Hydrator\ClassMethods;
use \Cv\Hydrator\Strategy\DateTimeStrategy;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Column\Style;
use ZfcDatagrid\Column\Formatter;
use ZfcDatagrid\Filter;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Session\Container;

class UploadsController extends AbstractActionController
{
    
    protected $user;
    protected $personaldata;
    protected $versionId;
    
    /**
     * OnDispatch of the controller
     * (non-PHPdoc)
     *
     * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
     */
    public function onDispatch(\Zend\Mvc\MvcEvent $e) {
    
    	$session = new Container('base');
    	$this->versionId = $session->offsetGet('versionid') ? $session->offsetGet('versionid') : 2;
    
    	// check the credencials
    	if (! $this->zfcUserAuthentication ()->hasIdentity ()) {
    		return $this->redirect ()->toRoute ( 'www/user' );
    	}
    
    	// Get the identity information
    	$this->user = $this->zfcUserAuthentication ()->getIdentity ();
    
    	// Get the TableGateway object to retrieve the data
    	$user = $this->getServiceLocator ()->get ( 'PdTable' );
    
    	// Get the user
    	$myUser = $user->getPdByUserId ( $this->user->getId() );
    
    	if (! empty ( $myUser )) {
    		$this->personaldata = $myUser;
    	}else{
    		$this->flashMessenger ()->setNamespace ( 'danger' )->addMessage ( 'You have to complete the personal data section.' );
    		return $this->redirect()->toRoute('cv/pd');
    	}
    
    	return parent::onDispatch ( $e );
    }
    
    public function getFileUploadLocation ()
    {
        // Fetch Configuration from Module Config
        $config = $this->getServiceLocator()->get('config');
        return $config['module_config']['document_location'];
    }

    public function indexAction ()
    {
    
        $grid = $this->createGrid();
        $grid->render();
    
        $response = $grid->getResponse();
    
        if ($grid->isHtmlInitReponse()) {
            $view = new ViewModel();
            $view->addChild($response, 'grid');
            return $view;
        } else {
            return $response;
        }
    }
    

    // Create the list grid
    private function createGrid ()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $select = new Select();
        $select->from(array ('up' => 'uploads'))
        ->join(array('we' => 'work_experiences'), 'up.workexperience_id = we.id', array('employer'), 'left')
        ->join(array('et' => 'education'), 'up.education_id = et.id', array('organisation'), 'left')
        ->join(array('ol' => 'languages'), 'up.language_id = ol.id', array('language'), 'left')
        ->where(array ('up.personaldata_id' => $this->personaldata->getId()))
        ->where(array ('up.translation_id' => $this->versionId));
    
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($select, $dbAdapter);
    
        $colId = new Column\Select('id', 'up');
        $colId->setLabel('Id');
        $colId->setIdentity();
        $grid->addColumn($colId);
    
        $col = new Column\Select('title', 'up');
        $col->setLabel(_('Title'));
        $col->setUserFilterDisabled(true);
        $grid->addColumn($col);
    
        $col = new Column\Select('originalfilename', 'up');
        $col->setLabel(_('Original Filename'));
        $col->setUserFilterDisabled(true);
        $col->setWidth(15);
        $grid->addColumn($col);
    
        $col = new Column\Select('employer', 'we');
        $col->setLabel(_('Work Experience'));
        $col->setUserFilterDisabled(true);
        $col->setWidth(15);
        $grid->addColumn($col);
    
        $col = new Column\Select('organisation', 'et');
        $col->setLabel(_('Education'));
        $col->setUserFilterDisabled(true);
        $col->setWidth(15);
        $grid->addColumn($col);
    
        $col = new Column\Select('language', 'ol');
        $col->setLabel(_('Language'));
        $col->setUserFilterDisabled(true);
        $col->setWidth(15);
        $grid->addColumn($col);
    
        // Add actions to the grid
        $downloadaction = new Column\Action\Button();
        $downloadaction->setAttribute('href', "/cv/uploads/filedownload/" . $downloadaction->getColumnValuePlaceholder(new Column\Select('id', 'up')));
        $downloadaction->setAttribute('class', 'btn btn-xs btn-success');
        $downloadaction->setLabel(_('download'));
    
        // Add actions to the grid
        $showaction = new Column\Action\Button();
        $showaction->setAttribute('href', "/cv/uploads/edit/" . $showaction->getColumnValuePlaceholder(new Column\Select('id', 'up')));
        $showaction->setAttribute('class', 'btn btn-xs btn-success');
        $showaction->setLabel(_('edit'));
    
        $delaction = new Column\Action\Button();
        $delaction->setAttribute('href', '/cv/uploads/delete/' . $delaction->getRowIdPlaceholder());
        $delaction->setAttribute('onclick', "return confirm('Are you sure?')");
        $delaction->setAttribute('class', 'btn btn-xs btn-danger');
        $delaction->setLabel(_('delete'));
    
        $col = new Column\Action();
        $col->addAction($downloadaction);
        $col->addAction($showaction);
        $col->addAction($delaction);
        $grid->addColumn($col);
    
        $grid->setToolbarTemplate('');
    
        return $grid;
    }

    public function processAction()
    {
        
        $uploadTable = $this->getServiceLocator()->get('UploadsTable');

        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\UploadForm');
        $inputFilter = $this->getServiceLocator()->get('UploadEditFilter');
        $form->setInputFilter($inputFilter);
        
        $request = $this->getRequest();
    
        if ($request->isPost()) {
            $upload = new Uploads();
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                // Fetch Configuration from Module Config
                $uploadPath    = $this->getFileUploadLocation();
    
                $id = $request->getPost()->get('id');
                $uploadItem = $uploadTable->getUpload($id, $this->personaldata->getId());
                
                $uploadItem->setTitle($request->getPost()->get('title'));
                $uploadItem->setWorkexperienceId($request->getPost()->get('workexperience_id'));
                $uploadItem->setEducationId($request->getPost()->get('education_id'));
                $uploadItem->setLanguageId($request->getPost()->get('language_id'));
                $uploadItem->setTranslationId($request->getPost()->get('translation_id'));
                
                $exchange_data['workexperience_id'] = $request->getPost()->get('workexperience_id') ? $request->getPost()->get('workexperience_id') : null;
                $exchange_data['education_id'] = $request->getPost()->get('education_id') ? $request->getPost()->get('education_id') : null;
                $exchange_data['translation_id'] = $request->getPost()->get('translation_id') ? $request->getPost()->get('translation_id') : null;
                $exchange_data['language_id'] = $request->getPost()->get('language_id') ? $request->getPost()->get('language_id') : null;
                
                
                $upload->exchangeArray($uploadItem);
                	
                
                $uploadObj = $uploadTable->saveUpload($upload);
    
                $this->flashMessenger ()->setNamespace ( 'success' )->addMessage ( 'The information have been saved correctly.' );
                
                return $this->redirect()->toRoute('cv/uploads' , array(
                        'action' =>  'edit',
                        'id' =>  $uploadObj->id
                ));
            }
        }
        	
        $viewModel = new ViewModel(array (
                'error' => true,
                'form' => $form
        ));
        $viewModel->setTemplate('cv/uploads/upload');
        return $viewModel;
        
    }

    public function UploadprocessAction()
    {
        
        // Get the TableGateway object to retrieve the data
        $user = $this->getServiceLocator()->get('PdTable');
        
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\UploadForm');
        $inputFilter = $this->getServiceLocator()->get('UploadFilter');
        $form->setInputFilter($inputFilter);
        
        $request = $this->getRequest();
    
        if ($request->isPost()) {
            $upload = new Uploads();
            $uploadFile    = $this->params()->fromFiles('fileupload');
            $uploadPath = $this->getFileUploadLocation();
            
            $form->setData($request->getPost());
    
            if (!$form->isValid()) {
                
                $viewModel = new ViewModel(array (
                        'error' => true,
                        'form' => $form
                ));
                $viewModel->setTemplate('cv/uploads/upload');
                
                return $viewModel;
            }else{
                
                $codedfile = strtolower($this->personaldata->getPac() . "_" . $this->CreateCode()) . ".zip";
                
                // Fetch Configuration from Module Config
                $uploadPath    = $this->getFileUploadLocation();
                @mkdir($uploadPath);
    
                // Save Uploaded file
                $adapter = new \Zend\File\Transfer\Adapter\Http();
                $adapter->setDestination($uploadPath);
                
                // Do not allow files with extension php or exe
                $adapter->addValidator('ExcludeExtension', false, 'php,exe');

                if ($adapter->isValid($uploadFile['name'])) {
	                if ($adapter->receive()) {
	                    	
	                    $exchange_data = array();
	                    $exchange_data['title'] = $request->getPost()->get('title');
	                    $exchange_data['originalfilename'] = $uploadFile['name'];
	                    $exchange_data['codedfilename'] = $codedfile;
	                    $exchange_data['mimetype'] = $uploadFile['type'];
	                    $exchange_data['size'] = $uploadFile['size'];
	                    $exchange_data['createdat'] = date('Y-m-d H:i:s');
	                    $exchange_data['personaldata_id'] = $this->personaldata->getId();
	                    $exchange_data['workexperience_id'] = $request->getPost()->get('workexperience_id') ? $request->getPost()->get('workexperience_id') : null;
	                    $exchange_data['education_id'] = $request->getPost()->get('education_id') ? $request->getPost()->get('education_id') : null;
	                    $exchange_data['translation_id'] = $request->getPost()->get('translation_id') ? $request->getPost()->get('translation_id') : null;
	                    $exchange_data['language_id'] = $request->getPost()->get('language_id') ? $request->getPost()->get('language_id') : null;
	
	                    // Create the zip file 
	                    $zip = new \ZipArchive();
	                    $filename = $uploadPath . "/" . $codedfile;
	                    
	                    if ($zip->open($filename, \ZipArchive::CREATE)!==TRUE) {
	                        exit("cannot open <$filename>\n");
	                    }
	                    
	                    $zip->addFile($uploadPath . "/" . $uploadFile['name'], $uploadFile['name']);
	                    $zip->close();
	                    
	                    // delete the old link
	                    @unlink($uploadPath . "/" . $uploadFile['name']);
	                    
	                    $upload->exchangeArray($exchange_data);
	                    $upload->setTranslationId($this->versionId);
	                    $uploadTable = $this->getServiceLocator()->get('UploadsTable');
	                    $uploadTable->saveUpload($upload);
	                }
	            }else{
	            	print_r($adapter->getMessages());
	            }
            }
        }
        

        $this->flashMessenger ()->setNamespace ( 'success' )->addMessage ( 'The information have been saved correctly.' );
        
        return $this->redirect()->toRoute('cv/uploads' , array(
                'action' =>  'index'
        ));
        
    }

    public function uploadAction ()
    {
        
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\UploadForm');
        $viewModel = new ViewModel(array ( 
                'form' => $form
        ));
        return $viewModel;
    }

    public function deleteAction ()
    {
        
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('UploadsTable');
        
        $upload = $uploadTable->getUpload($uploadId, $this->personaldata->getId());
        $uploadPath = $this->getFileUploadLocation();
       
        // Remove File
        @unlink($uploadPath . "/" . $upload->getCodedfilename());
       
        // Delete Records
        $uploadTable->deleteUpload($uploadId, $this->personaldata->getId());
        
        return $this->redirect()->toRoute('cv/uploads');
    }

    public function editAction ()
    {
        
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('UploadsTable');
        
        // Upload Edit Form
        $upload = $uploadTable->getUpload($uploadId, $this->personaldata->getId());
        
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\UploadEditForm');
        $form->bind($upload);
        
        $viewModel = new ViewModel(array ( 
                'form' => $form, 
                'file' => $upload, 
        ));
        return $viewModel;
    }
    
    public function fileDownloadAction ()
    {
        // check the credencials
        if (! $this->zfcUserAuthentication()->hasIdentity()) {
            return $this->redirect()->toRoute('zfcuser');
        }

        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('UploadsTable');
        $upload = $uploadTable->getUpload($uploadId, $this->personaldata->getId());
        
        // Fetch Configuration from Module Config
        $uploadPath = $this->getFileUploadLocation();
        
        $fileName = $uploadPath . "/" . $upload->getCodedfilename();
        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($fileName, 'r'));
        $response->setStatusCode(200);
        
        $headers = new \Zend\Http\Headers();
        $headers->addHeaderLine('Content-Type', 'application/octet-stream')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($fileName) . '"')
                ->addHeaderLine('Content-Length', filesize($fileName));
        
        $response->setHeaders($headers);
        
        return $response;
    }
    

    /**
     *
     * @param integer $length
     * @param string $prefix
     * @return string
     */
    private function CreateCode($length = 10, $prefix = '') {
        $len = $length;
        $lowercase = false;
        $uppercase = true;
        $number = true;
        $specchar = false;
    
        if ($uppercase) {
            $chars = array ("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" );
        }
        if ($lowercase) {
            $chars [] = "a";
            $chars [] = "b";
            $chars [] = "c";
            $chars [] = "d";
            $chars [] = "e";
            $chars [] = "f";
            $chars [] = "g";
            $chars [] = "h";
            $chars [] = "i";
            $chars [] = "j";
            $chars [] = "k";
            $chars [] = "l";
            $chars [] = "m";
            $chars [] = "n";
            $chars [] = "o";
            $chars [] = "p";
            $chars [] = "q";
            $chars [] = "r";
            $chars [] = "s";
            $chars [] = "t";
            $chars [] = "u";
            $chars [] = "v";
            $chars [] = "w";
            $chars [] = "x";
            $chars [] = "y";
            $chars [] = "z";
        }
        if ($number) {
            $chars [] = "1";
            $chars [] = "2";
            $chars [] = "3";
            $chars [] = "4";
            $chars [] = "5";
            $chars [] = "6";
            $chars [] = "7";
            $chars [] = "8";
            $chars [] = "9";
            $chars [] = "0";
        }
        if ($specchar) {
            $chars [] = '!';
            $chars [] = '@';
            $chars [] = '#';
            $chars [] = "$";
            $chars [] = '%';
            $chars [] = '^';
            $chars [] = '&';
            $chars [] = '*';
            $chars [] = '(';
            $chars [] = ')';
            $chars [] = '{';
            $chars [] = '}';
            $chars [] = '[';
            $chars [] = ']';
            $chars [] = '<';
            $chars [] = '>';
            $chars [] = '?';
            $chars [] = "=";
            $chars [] = '+';
            $chars [] = '-';
            $chars [] = "_";
            $chars [] = "/";
        }
    
        $max_elements = count ( $chars ) - 1;
        $newpw = $prefix;
        $l = $len - strlen ( $prefix );
        for($i = 0; $i < $l; $i ++) {
            srand ( ( double ) microtime () * 1000000 );
            $newpw .= $chars [rand ( 0, $max_elements )];
        }
    
        return $newpw;
    }
}

