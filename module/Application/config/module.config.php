<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array ( 
        'router' => array ( 
                'routes' => array ( 
                        'home' => array ( 
                                'type' => 'Zend\Mvc\Router\Http\Literal', 
                                'options' => array ( 
                                        'route' => '/', 
                                        'defaults' => array ( 
                                                'controller' => 'Application\Controller\Index', 
                                                'action' => 'index'
                                        )
                                ), 
                                'may_terminate' => true, 
                                'child_routes' => array ( 
                                        'default' => array ( 
                                                'type' => 'Segment', 
                                                'options' => array ( 
                                                        'route' => '/[:controller[/:action]]', 
                                                        'constraints' => array ( 
                                                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*', 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ()
                                                )
                                        )
                                ),
                        ), 
                        'application' => array ( 
                                'type' => 'Literal', 
                                'options' => array ( 
                                        'route' => '/application', 
                                        'defaults' => array ( 
                                                '__NAMESPACE__' => 'Application\Controller', 
                                                'controller' => 'Index', 
                                                'action' => 'index'
                                        )
                                ), 
                                'may_terminate' => true, 
                                'child_routes' => array ( 
                                        'default' => array ( 
                                                'type' => 'Segment', 
                                                'options' => array ( 
                                                        'route' => '/[:controller[/:action[/:id]]]', 
                                                        'constraints' => array ( 
                                                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*', 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*', 
                                                                'id' => '[a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ()
                                                )
                                        )
                                )
                        ), 
                        'download' => array ( 
                                'type' => 'Literal', 
                                'options' => array ( 
                                        'route' => '/download', 
                                        'defaults' => array ( 
                                                '__NAMESPACE__' => 'Application\Controller', 
                                                'controller' => 'Index', 
                                                'action' => 'download'
                                        )
                                ), 
                                'may_terminate' => true, 
                                'child_routes' => array ( 
                                        'default' => array ( 
                                                'type' => 'Segment', 
                                                'options' => array ( 
                                                        'route' => '/[:file]', 
                                                        'constraints' => array ( 
                                                                'file' => '[a-zA-Z0-9+-_\.]+'
                                                        ), 
                                                        'defaults' => array ()
                                                )
                                        )
                                )
                        ), 
                        'export' => array ( 
                                'type' => 'hostname', 
                                'options' => array ( 
                                        'route' => ':pac.:export.eurocv.:tld', 
                                        'constraints' => array ( 
                                                'pac' => '[a-z0-9-]{1,25}', 
                                                'export' => '[doc|pdf|odt|json|xml]*', 
                                                'tld' => '[it|eu]*'
                                        ), 
                                        'defaults' => array ( 
                                                'controller' => 'Application\Controller\Index', 
                                                'action' => 'export', 
                                                'tld' => 'eu'
                                        )
                                ), 
                                'may_terminate' => true, 
                                'child_routes' => array ( 
                                        'default' => array ( 
                                                'type' => 'Literal', 
                                                'options' => array ( 
                                                        'route' => '/'
                                                )
                                        )
                                )
                        ), 
                        'exportversion' => array ( 
                                'type' => 'hostname', 
                                'options' => array ( 
                                        'route' => ':pac.:language.:export.eurocv.:tld', 
                                        'constraints' => array ( 
                                                'pac' => '[a-z0-9-]{1,25}', 
                                                'export' => '[doc|pdf|odt|json|xml]*',
                                                'language' => '[it|en|es|da|de|el|fr|nl|pt|fi|sv|cs|et|lv|lt|hu|mt|pl|sk|si|ga|ru|al|ro|bg|no|hr]*',
                                                'tld' => '[it|eu]*'
                                        ), 
                                        'defaults' => array ( 
                                                'controller' => 'Application\Controller\Index', 
                                                'action' => 'export', 
                                                'tld' => 'eu'
                                        )
                                ), 
                                'may_terminate' => true, 
                                'child_routes' => array ( 
                                        'default' => array ( 
                                                'type' => 'Literal', 
                                                'options' => array ( 
                                                        'route' => '/'
                                                )
                                        )
                                )
                        ), 
                        'versions' => array ( 
                                'type' => 'hostname', 
                                'options' => array ( 
                                        'route' => ':pac.:language.eurocv.:tld', 
                                        'constraints' => array ( 
                                                'pac' => '[a-z0-9-]{1,25}', 
                                                'language' => '[it|en|es|da|de|el|fr|nl|pt|fi|sv|cs|et|lv|lt|hu|mt|pl|sk|si|ga|ru|al|ro|bg|no|hr]{2}', 
                                                'tld' => '[it|eu]*'
                                        ), 
                                        'defaults' => array ( 
                                                'controller' => 'Application\Controller\Index', 
                                                'action' => 'show', 
                                                'tld' => 'eu'
                                        )
                                ), 
                                'may_terminate' => true, 
                                'child_routes' => array ( 
                                        'default' => array ( 
                                                'type' => 'Literal', 
                                                'options' => array ( 
                                                        'route' => '/'
                                                )
                                        )
                                )
                        ), 
                        'pac' => array ( 
                                'type' => 'hostname', 
                                'options' => array ( 
                                        'route' => ':pac.eurocv.:tld', 
                                        'constraints' => array ( 
                                                'pac' => '[a-z0-9-]{1,25}', 
                                                'tld' => '[it|eu]*'
                                        ), 
                                        'defaults' => array ( 
                                                '__NAMESPACE__' => 'Application\Controller', 
                                                'controller' => 'Index', 
                                                'action' => 'show', 
                                                'tld' => 'eu'
                                        )
                                ), 
                                'may_terminate' => true, 
                                'child_routes' => array ( 
                                        'generic' => array ( 
                                                'type' => 'Literal', 
                                                'options' => array ( 
                                                        'route' => '/'
                                                )
                                        ), 
                                        'default' => array ( 
                                                'type' => 'Segment', 
                                                'options' => array ( 
                                                        'route' => '/index[/:action[/:id]]', 
                                                        'constraints' => array ( 
                                                                'controller' => 'Index', 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*', 
                                                                'id' => '[a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ()
                                                )
                                        ), 
                                        'private' => array ( 
                                                'type' => 'Segment', 
                                                'options' => array ( 
                                                        'route' => '/private[/:action[/:id]]', 
                                                        'constraints' => array ( 
                                                                'controller' => 'PrivateCv', 
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*', 
                                                                'id' => '[a-zA-Z0-9_-]*'
                                                        ), 
                                                        'defaults' => array ( 
                                                                '__NAMESPACE__' => 'Application\Controller', 
                                                                'controller' => 'PrivateCv', 
                                                                'action' => 'index'
                                                        )
                                                ), 
                                        )
                                )
                        ), 
                        'www' => array ( 
                                'type' => 'hostname', 
                                'options' => array ( 
                                        'route' => 'www.eurocv.:tld', 
                                        'constraints' => array ( 
                                                'tld' => '[it|eu]*'
                                        ), 
                                        'defaults' => array ( 
                                                '__NAMESPACE__' => 'Application\Controller', 
                                                'controller' => 'Index', 
                                                'action' => 'index', 
                                                'tld' => 'eu'
                                        )
                                ), 
                                'may_terminate' => true, 
                                'child_routes' => array ( 
                                        'default' => array ( 
                                                'type' => 'Literal', 
                                                'options' => array ( 
                                                        'route' => '/'
                                                )
                                        ),
                                        'showprivate' => array (
                                                'type' => 'Segment',
                                                'options' => array (
                                                        'route' => '/show[/:pac]',
                                                        'constraints' => array (
                                                                'action' => 'show',
                                                                'pac' => '[a-z0-9-]{1,25}'
                                                        ),
                                                        'defaults' => array(
                                                                'controller' => 'index',
                                                                'action'     => 'show',
                                                        ),
                                                ),
                                        ),
                                        'user' => array ( 
                                                'type' => 'Literal', 
                                                'options' => array (
                                                        'route' => '/user'
                                                ),
                                                'defaults' => array(
                                                    'controller' => 'zfcuser',
                                                    'action'     => 'index',
                                                ),
                                        ),
                                )
                        )
                )
                
        ), 
        
        'service_manager' => array ( 
                'abstract_factories' => array ( 
                        'Zend\Cache\Service\StorageCacheAbstractServiceFactory', 
                        'Zend\Log\LoggerAbstractServiceFactory'
                ), 
                'aliases' => array ( 
                        'translator' => 'MvcTranslator'
                ), 
                'factories' => array ( 
                        'PrivateCvCaptcha' => 'Application\Service\PrivateCvCaptchaFactory', 
                        'PrivateCvForm' => 'Application\Service\PrivateCvFormFactory', 
                        'PrivateCvMailTransport' => 'Application\Service\PrivateCvMailTransportFactory',
                ),
        ), 
        
        'controllers' => array ( 
                'invokables' => array ( 
                        'Application\Controller\Index' => 'Application\Controller\IndexController', 
                        'Application\Controller\Search' => 'Application\Controller\SearchController'
                ), 
                'factories' => array ( 
                        'Application\Controller\PrivateCv' => 'Application\Service\PrivateCvControllerFactory',
                        'Application\Controller\Europass' => 'Application\Service\EuropassControllerFactory',
                )
        ), 
        'view_helpers' => array ( 
                'invokables' => array ( 
                        'datetime' => 'Application\View\Helper\Datetime', 
                        'keywords' => 'Application\View\Helper\Keywords', 
                        'jobcategory' => 'Application\View\Helper\JobCategory', 
                        'attachments' => 'Application\View\Helper\Attachments', 
                        'translations' => 'Application\View\Helper\Translations', 
                        'cvversions' => 'Application\View\Helper\Versions', 
                        'locale' => 'Application\View\Helper\Locale',
                        'user' => 'Application\View\Helper\User',
                        'photo' => 'Application\View\Helper\Photo',
                        'wiki' => 'Application\View\Helper\Wiki',
                        'language' => 'Application\View\Helper\Languages',
                        'obfuscator' => 'Application\View\Helper\EmailObfuscator'
                )
        ), 
        'view_manager' => array ( 
                'display_not_found_reason' => true, 
                'display_exceptions' => true, 
                'doctype' => 'HTML5', 
                'not_found_template' => 'error/404', 
                'exception_template' => 'error/index', 
                'template_map' => array ( 
                        'application/index/index' => __DIR__ . '/../view/application/index/index.phtml', 
                        'error/404' => __DIR__ . '/../view/error/404.phtml', 
                        'error/index' => __DIR__ . '/../view/error/index.phtml'
                ), 
                'template_path_stack' => array ( 
                        __DIR__ . '/../view'
                ),
                'strategies' => array(
                        'ViewJsonStrategy',
                ),
        ), 
        
        // Placeholder for console routes
        'console' => array ( 
                'router' => array ( 
                        'routes' => array ()
                )
        )
);
