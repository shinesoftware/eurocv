EuroCV Project
=======================
EuroCV is a non-profit project aimed at supporting job searching. It allows users to create and keep online CVs up to date for free and with no advertising. One of the project's priorities is to improve the 'weakest link’ in the job search chain.
Many CV publishing agencies offering a similar service often require payment before all the details are displayed. CVs are frequently flooded by banners of the sponsors wishing to advertise their products. Furthermore, these agencies do not offer EU officially accepted CV formats.

Web Server Setup
----------------

### Apache Setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

    <VirtualHost *:80>
        ServerName www.eurocv.it
        DocumentRoot /Library/WebServer/Documents/eurocv/public
        SetEnv APPLICATION_ENV "development"
        <Directory /Library/WebServer/Documents/eurocv/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
