<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApi for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array ( 
        'router' => array ( 
                'routes' => array ( 
                      
                        'api' => array (
                                'type' => 'hostname',
                                'options' => array (
                                        'route' => 'api.eurocv.it',
                                        'defaults' => array (
                                                'controller' => 'Api\Controller\Index',
                                                'action' => 'index'
                                        )
                                )
                        ),
                )
        ), 
        'service_manager' => array ( 
                'abstract_factories' => array ( 
                        'Zend\Cache\Service\StorageCacheAbstractServiceFactory', 
                        'Zend\Log\LoggerAbstractServiceFactory'
                ), 
        ), 
        'controllers' => array ( 
                'invokables' => array ( 
                        'Api\Controller\Index' => 'Api\Controller\IndexController',
                )
        ), 
        'view_manager' => array ( 
                'display_not_found_reason' => true, 
                'display_exceptions' => true, 
                'doctype' => 'HTML5', 
                'not_found_template' => 'error/404', 
                'exception_template' => 'error/index', 
                'template_map' => array ( 
                        'api/index/index' => __DIR__ . '/../view/api/index/index.phtml', 
                ), 
                'template_path_stack' => array ( 
                        __DIR__ . '/../view'
                )
        ), 
        // Placeholder for console routes
        'console' => array ( 
                'router' => array ( 
                        'routes' => array ()
                )
        )
);
