<?php
namespace Cv\Form;
use Zend\InputFilter\InputFilter;

class UploadFilter extends InputFilter
{

    public function __construct ()
    {
        $this->add(array (
                'name' => 'title',
                'required' => true,
        ));
        
        $this->add(array (
                'name' => 'workexperience_id',
                'required' => false,
        ));
        
        $this->add(array (
                'name' => 'education_id',
                'required' => false,
        ));
        
        $this->add(array (
                'name' => 'language_id',
                'required' => false,
        ));
    }
}