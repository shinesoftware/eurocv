<?php
return array(
        'controllers' => array(
                'invokables' => array(
                        'Cms\Controller\Index' => 'Cms\Controller\IndexController',
                        'Cms\Controller\Admin' => 'Cms\Controller\AdminController',
                )
        ),
        'router' => array(
                'routes' => array(
                        'cms' => array(
                                'type' => 'Literal',
                                'options' => array(
                                        'route' => '/cms',
                                        'defaults' => array(
                                                '__NAMESPACE__' => 'Cms\Controller',
                                                'controller' => 'Index',
                                                'action' => 'index'
                                        )
                                ),
                                'may_terminate' => true,
                                'child_routes' => array(
                                        'default' => array(
                                                'type' => 'Segment',
                                                'options' => array(
                                                        'route' => '/[:controller[/:action]]',
                                                        'constraints' => array(
                                                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                                                        ),
                                                        'defaults' => array()
                                                )
                                        ),
                                       
                                        'seo' => array(
                                                'type'    => 'Segment',
                                                'may_terminate' => true,
                                                'options' => array(
                                                        'route'    => '[/:page].html',
                                                        'constraints' => array(
                                                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'id'     => '[a-zA-Z0-9_-]*',
                                                        ),
                                                        'defaults' => array(
                                                                'controller' => 'Cms\Controller\Index',
                                                                'action'     => 'page',
                                                        ),
                                                ),
                                        ),
                                       
                                        'admin' => array(
                                                'type'    => 'Segment',
                                                'may_terminate' => true,
                                                'options' => array(
                                                        'route'    => '/admin[/:action[/:id]]',
                                                        'constraints' => array(
                                                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                                'id'     => '[a-zA-Z0-9_-]*',
                                                        ),
                                                        'defaults' => array(
                                                                'controller' => 'Cms\Controller\Admin',
                                                                'action'     => 'index',
                                                        ),
                                                ),
                                        ),
                                )
                        ),
                        
                )
        ),
        'view_manager' => array(
                'template_path_stack' => array(
                        'cms' => __DIR__ . '/../view'
                )
        )
);
