<?php 
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Translations extends AbstractHelper implements ServiceLocatorAwareInterface  
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function __invoke()
    {
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();
        $translationTable = $serviceLocator->get('TranslationTable');
        $translator = $serviceLocator->get('translator');
        $translations = $translationTable->fetchAll();
        $newTranslations = array();
        
        // Translate the language title and sort the result
        foreach ($translations as $translation){
            if($translation->getActive()){
                $titleTranslated = $translator->translate($translation->getTitle());
                $newTranslations[$translation->getId()] = $titleTranslated;
            }
        }

        // Sorting of the result
        asort($newTranslations);
        
        return $this->view->render('application/partial/translations', array('translations' => $newTranslations));
    }
}