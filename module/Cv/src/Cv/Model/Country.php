<?php
namespace Cv\Model;

class Country
{
    protected $id;
    protected $code;
    protected $country;
    
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $code
     */
    public function getCode ()
    {
        return $this->code;
    }

	/**
     * @return the $country
     */
    public function getCountry ()
    {
        return $this->country;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $code
     */
    public function setCode ($code)
    {
        $this->code = $code;
    }

	/**
     * @param field_type $country
     */
    public function setCountry ($country)
    {
        $this->country = $country;
    }

    /**
     * 
     * @param array $data
     */
    function exchangeArray ($data)
    {
        foreach ($data as $field => $value){
            $this->$field = (isset($value)) ? $value : null;
        }
    }
    
    /**
     * Get the keys of the array
     * @return multitype:
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
}