<?php

/**
 *  Europass promotes interoperability by defining a specific data model and vocabulary to express the information contained in Europass documents. 
 *  This data model is realised in terms of an XML Schema as well as a JSON Schema which: 
 */

namespace Application\Service;
use \Base\Model\XmlCdata;
use \Application\View\Helper\Wiki as Wiki;

class Europass
{
    protected $xml;
    
    protected $schema;

    protected $format;

    protected $command;

    protected $langs;
    
    protected $user;

    protected $workexperience;

    protected $education;

    protected $languages;

    protected $competences;

    protected $jobcategory;
    
    protected $coverletter;

    protected $locale;
    
    protected $debug;
    protected $wiki;

    public function __construct ($locale="en", \Cv\Model\LangsTable $langsTable)
    {
    	$this->wiki = new Wiki();
        $this->setFormat('pdf');
        $this->setCommand('document/to/pdf-cv');
        $this->schema = "http://europass.cedefop.europa.eu/xml/v3.2.0/EuropassSchema.xsd";
        $this->langs = $langsTable;
        
        $lang = explode("_", $locale);
        // http://interop.europass.cedefop.europa.eu/web-services/rest-api-summary/
        $acceptedLocale = array('bg', 'es', 'cs', 'da', 'de', 'et'. 'el', 'en', 'fr', 'hr', 'is', 'it', 'lv', 'lt', 'hu', 'mt', 'nl', 'no', 'pl', 'pt', 'ro', 'sk', 'sl', 'fi', 'sv', 'tr');
        if(!empty($lang[0]) && in_array($lang[0], $acceptedLocale)){
            $this->locale = $lang[0];
        }
        
        // Creation of the header of the file
        $xml = new XmlCdata('<?xml version=\'1.0\' encoding=\'utf-8\'?><SkillsPassport xmlns="http://europass.cedefop.europa.eu/Europass" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="'.$this->schema.'" />');
//         $xml->addAttribute("xmlns", "http://europass.cedefop.europa.eu/Europass");
//         $xml->addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance", "xmlns");
//         $xml->addAttribute("xmlns:schemaLocation", "http://europass.cedefop.europa.eu/Europass " . $this->schema, "xmlns");
        $xml->addAttribute("locale", $this->locale);
        
        $this->xml = $xml;
    }
    
    /**
	 * @return the $coverletter
	 */
	public function getCoverletter() {
		return $this->coverletter;
	}

	/**
	 * @param field_type $coverletter
	 */
	public function setCoverletter($coverletter) {
		$this->coverletter = $coverletter;
	}

	/**
     *
     * @return the $jobcategory
     */
    public function getJobcategory ()
    {
        return $this->jobcategory;
    }
    
    /**
     *
     * @param field_type $jobcategory
     */
    public function setJobcategory ($jobcategory)
    {
        $this->jobcategory = $jobcategory;
    }

    /**
     *
     * @return the $debug
     */
    public function getDebug ()
    {
        return $this->debug;
    }

    /**
     *
     * @param field_type $debug            
     */
    public function setDebug ($debug)
    {
        $this->debug = $debug;
    }

    /**
     *
     * @return the $user
     */
    public function getUser ()
    {
        return $this->user;
    }

    /**
     *
     * @return the $workexperience
     */
    public function getWorkexperience ()
    {
        return $this->workexperience;
    }

    /**
     *
     * @return the $education
     */
    public function getEducation ()
    {
        return $this->education;
    }

    /**
     *
     * @return the $languages
     */
    public function getLanguages ()
    {
        return $this->languages;
    }

    /**
     *
     * @return the $competences
     */
    public function getCompetences ()
    {
        return $this->competences;
    }

    /**
     *
     * @param field_type $user            
     */
    public function setUser (\Cv\Model\Pd $user)
    {
        $this->user = $user;
    }

    /**
     *
     * @param field_type $workexperience            
     */
    public function setWorkexperience ($workexperience)
    {
        $this->workexperience = $workexperience;
    }

    /**
     *
     * @param field_type $education            
     */
    public function setEducation ($education)
    {
        $this->education = $education;
    }

    /**
     *
     * @param field_type $languages            
     */
    public function setLanguages ($languages)
    {
        $this->languages = $languages;
    }

    /**
     *
     * @param field_type $competences            
     */
    public function setCompetences ($competences)
    {
        $this->competences = $competences;
    }

    /**
     *
     * @return the $format
     */
    public function getFormat ()
    {
        return $this->format;
    }
    
    /**
     *
     * @return the $command
     */
    public function getCommand ()
    {
        return $this->command;
    }
    
    /**
     *
     * @param field_type $command
     */
    public function setCommand ($command)
    {
        $this->command = $command;
    }

    /**
     *
     * @param field_type $format            
     */
    public function setFormat ($format)
    {
        if (! empty($format)) {
            $this->format = $format;
            if ($format == "pdf") {
                $this->setCommand('document/to/pdf');
            } elseif ($format == "doc") {
                $this->setCommand('document/to/word');
            } elseif ($format == "odt") {
                $this->setCommand('document/to/opendoc');
            } elseif ($format == "json") {
                $this->setCommand('document/to/json');
            } elseif ($format == "xml") {
                $this->setDebug(true);
                $format = "xml";
            }
            
            // Set the header of the content
            $this->setHeader($format);
        }
    }

    private function setHeader ($format)
    {
        $user = $this->user;
        
        if ($format == "pdf") {
            header('Content-Type: application/pdf;');
        } elseif ($format == "doc") {
            header('Content-Type: application/vnd.ms-word;');
            header('Content-Disposition: attachment; filename="' . $user->getPac() . '.doc"');
        } elseif ($format == "odt") {
            header('Content-Type: application/vnd.oasis.opendocument.text;');
            header('Content-Disposition: attachment; filename="'.$user->getPac().'.odt"');
        } elseif ($format == "json") {
            header('Content-Type: text/plain;');
        } elseif ($format == "xml") {
            header('Content-Type: text/xml;');
        }
    }

    /**
     * Set the Document information for the XML
     *
     * @return \SimpleXMLElement
     */
    private function setDocumentInfo ()
    {
        $user = $this->getUser();
        $xml = $this->xml;
        
        $DocumentInfo = $xml->addChild('DocumentInfo');

        
        $createdat = $user->getCreatedat();
        $updatedat = $user->getUpdatedat();
        
        $createdat = !empty($createdat) ? str_replace(" ", "T", $createdat) . "Z" : date('Y-m-d') . "T" . date('H:i:s') . "Z";
        $updatedat = !empty($updatedat) ? str_replace(" ", "T", $updatedat) . "Z" : date('Y-m-d') . "T" . date('H:i:s') . "Z";
        
        $DocumentInfo->addChild('DocumentType', 'ECV_ESP');
        $DocumentInfo->addChild('CreationDate', $createdat);

        $DocumentInfo->addChild('LastUpdateDate', $updatedat);
        

        $DocumentInfo->addChild('XSDVersion', 'V3.2');

        $DocumentInfo->addChild('Generator', 'EuroCv.eu');

        $DocumentInfo->addChild('Comment', 'EuroCv.eu');
        
        return $xml;
    }
    
    /**
     * Create Identification section of the XML
     *
     * @return \SimpleXMLElement
     */
    private function createPersonaldata ()
    {
        $user = $this->getUser();
        $xml = $this->xml;
        
        $identification = $xml->addChild('LearnerInfo')->addChild('Identification');
        $person = $identification->addChild('PersonName');
        $person->addChild('FirstName', $user->getFirstname());
        $person->addChild('Surname', $user->getLastname());
        
        $contactInfo = $identification->addChild('ContactInfo');
        $Demographics = $identification->addChild('Demographics');
        
        $birthdate = $user->getBirthdate();
        if (! empty($birthdate)) {
            list ($year, $month, $day) = explode('-', date('Y-m-d', strtotime($birthdate)));
            $birthdateItem = $Demographics->addChild('Birthdate');
            $birthdateItem->addAttribute('year', $year);
            $birthdateItem->addAttribute('month', "--$month");
            $birthdateItem->addAttribute('day', "---$day");
        }
        
        $gender = $Demographics->addChild('Gender');

        $gender->addChild('Code', $user->getGender());

        $gender->addChild('Label', $user->getGender());

        
        $addresses = $user->getAddresses();
        if (! empty($addresses)) {
            foreach ($addresses as $address) {
            	if($address->getVisible()){
	                $addressItem = $contactInfo->addChild('Address')->addChild('Contact');
	                $addressItem->addChild('AddressLine', $address->getStreet());
	                $addressItem->addChild('PostalCode', $address->getCode());
	                $addressItem->addChild('Municipality', $address->getCity());
	                
	                $countryItem = $addressItem->addChild('Country');
	                $countryItem->addChild('Code', $address->countrycode);
	                $countryItem->addChild('Label', $address->country);
	                break; //   Only one <Address /> element is allowed in <ContactInfo />, remove two as suggested by Dimitris Zavaliadis (Cedefop.europa.eu)
            	}
            }
        }
        
        $contactInfo->addChild('Email')->addChild('Contact', $user->getEmail());
        
        $contacts = $user->getContacts();
        if (! empty($contacts)) {
        	$TelephoneList = $contactInfo->addChild('TelephoneList');
            foreach ($contacts as $contact) {
            	if($contact->getVisible()){            		
	                if ($contact->type == "Telephone") {
	                    $contactItem = $TelephoneList->addChild('Telephone');
	                    $contactItem->addChild('Contact', $contact->getContact());
	                    $contactUse = empty($contactItem->Use) ? $contactItem->addChild('Use') : $contactItem->Use;
	                    $contactUse->addChild('Code', 'home');
	                    $contactUse->addChild('Label', 'Home');
	                } elseif ($contact->type == "Mobile") {
	                    $contactItem = $TelephoneList->addChild('Telephone');
	                    $contactItem->addChild('Contact', $contact->getContact());
	                    $contactUse = empty($contactItem->Use) ? $contactItem->addChild('Use') : $contactItem->Use;
	                    $contactUse->addChild('Code', 'mobile');
	                    $contactUse->addChild('Label', 'Mobile');
	                } elseif ($contact->type == "Fax") {
	                    $contactItem = $TelephoneList->addChild('Telephone');
	                    $contactItem->addChild('Contact', $contact->getContact());
	                    $contactUse = empty($contactItem->Use) ? $contactItem->addChild('Use') : $contactItem->Use;
	                    $contactUse->addChild('Code', 'home');
	                    $contactUse->addChild('Label', 'Home');
	                } elseif ($contact->type == "Skype") {
	                    $contactItem = $contactInfo->addChild('InstantMessagingList')->addChild('InstantMessaging');
	                    $contactItem->addChild('Contact', $contact->getContact());
	                    $contactUse = empty($contactItem->Use) ? $contactItem->addChild('Use') : $contactItem->Use;
	                    $contactUse->addChild('Code', 'home');
	                    $contactUse->addChild('Label', 'Home');
	                } elseif ($contact->type == "Yahoo") {
	                    $contactItem = $contactInfo->addChild('InstantMessagingList')->addChild('InstantMessaging');
	                    $contactItem->addChild('Contact', $contact->getContact());
	                    $contactUse = empty($contactItem->Use) ? $contactItem->addChild('Use') : $contactItem->Use;
	                    $contactUse->addChild('Code', 'home');
	                    $contactUse->addChild('Label', 'Home');
	                }
	            }
            }
        }
        return $xml;
    }

    /**
     * Create the Headline section of the XML
     *
     * @return \SimpleXMLElement
     */
    private function createHeadline ()
    {
        $user = $this->getUser();
        $xml = $this->xml;
        $jobCategory = $this->getJobcategory();

        if($jobCategory){
        	$HeadlineType = $xml->LearnerInfo->addChild('Headline')->addChild('Type');

        	$HeadlineType->addChild('Code', 'position');

        	$HeadlineType->addChild('Label', 'Position');
            $xml->LearnerInfo->Headline->addChild('Description')->addChild('Label', $jobCategory->getCategory());
        }
        return $xml;
    }

    /**
     * Create the Work Experience section of the XML
     *
     * @return \SimpleXMLElement
     */
    private function createWorkexperience ()
    {
        $we = $this->getWorkexperience();
        $xml = $this->xml;
        $xml->LearnerInfo->addChild('WorkExperienceList');
        
        foreach ($we as $item) {
            $WorkExperience = $xml->LearnerInfo->WorkExperienceList->addChild('WorkExperience');
            $WorkExperience->addChild('Period');
            $WorkExperience->Period->addChild('From');
            $WorkExperience->Period->addChild('To');
            
            $startdate = $item->getStartdate();
            if (! empty($startdate)) {
                if(strpos($startdate, "-00")){ // dd/YYYY
                    $startdate = str_replace("-00", "", $startdate);
                    $start = new \Datetime(date('Y-m', strtotime($startdate)));
                }else{
                    $start = new \Datetime(date('Y-m-d', strtotime($startdate)));
                }
                
                $month = $start->format('m');

                $year = $start->format('Y');
                
                $WorkExperience->Period->From->addAttribute('year', $year);
                $WorkExperience->Period->From->addAttribute('month', "--$month");
            }
            
            $enddate = $item->getEnddate();
            if (! empty($enddate)) {
                if(strpos($enddate, "-00")){ // dd/YYYY

                    $enddate = str_replace("-00", "", $enddate);

                    $end = new \Datetime(date('Y-m', strtotime($enddate)));

                }else{

                    $end = new \Datetime(date('Y-m-d', strtotime($enddate)));

                }

                $month = $end->format('m');

                $year = $end->format('Y');

                
                $WorkExperience->Period->To->addAttribute('year', $year);
                $WorkExperience->Period->To->addAttribute('month', "--$month");
                $WorkExperience->Period->addChild('Current', "false");
            } else {
                $WorkExperience->Period->addChild('Current', "true");
            }
            
            $WorkExperience->addChild('Position')->addChild('Label')->addCdata($item->getPosition());
            $WorkExperience->addChild('Activities')->addCdata($this->wiki->__invoke($item->getActivities()));
            $WorkExperience->addChild('Employer');
            $WorkExperience->Employer->addChild('Name')->addCdata($item->getEmployer());
        }
        
        return $xml;
    }

    /**
     * Create the Education section of the XML
     *
     * @return \SimpleXMLElement
     */
    private function createEducation ()
    {
        $et = $this->getEducation();
        $xml = $this->xml;
        
        $xml->LearnerInfo->addChild('EducationList');
        
        if (0 == $et->count()) {
            return $xml;
        }
        
        foreach ($et as $item) {
            $Education = $xml->LearnerInfo->EducationList->addChild('Education');
            $Education->addChild('Period');
            $Education->Period->addChild('From');
            $Education->Period->addChild('To');
            
            $startdate = $item->getStartdate();
            if (! empty($startdate)) {
                if(strpos($startdate, "-00")){ // dd/YYYY
                    $startdate = str_replace("-00", "", $startdate);
                    $start = new \Datetime(date('Y-m', strtotime($startdate)));
                }else{
                    $start = new \Datetime(date('Y-m-d', strtotime($startdate)));
                }
                
                $month = $start->format('m');
                $year = $start->format('Y');
                
                $Education->Period->From->addAttribute('year', $year);
                $Education->Period->From->addAttribute('month', "--$month");
            }
            
            $enddate = $item->getEnddate();
            if (! empty($enddate)) {
                if(strpos($enddate, "-00")){ // dd/YYYY
                    $enddate = str_replace("-00", "", $enddate);
                    $end = new \Datetime(date('Y-m', strtotime($enddate)));
                }else{
                    $end = new \Datetime(date('Y-m-d', strtotime($enddate)));
                }

                $month = $end->format('m');
                $year = $end->format('Y');
                
                $Education->Period->To->addAttribute('year', $year);
                $Education->Period->To->addAttribute('month', "--$month");
                $Education->Period->addChild('Current', "false");
            } else {
                $Education->Period->addChild('Current', "true");
            }
            
            $Education->addChild('Title')->addCdata($item->getTitle());
            $Education->addChild('Activities')->addCdata($this->wiki->__invoke($item->getSkills()));
            $Education->addChild('Organisation');
            $Education->Organisation->addChild('Name')->addCdata($item->getOrganisation());
        }
        
        return $xml;
    }

    /**
     * Create the Languages section of the XML
     *
     * @return \SimpleXMLElement
     */
    private function createLanguage ()
    {
        $lang = $this->getLanguages();
        $xml = $this->xml;
        $user = $this->getUser();
        
        $xml->LearnerInfo->addChild('Skills');
        $xml->LearnerInfo->Skills->addChild('Linguistic');
        
        if($user->getMothertongueId()){

        	$mothertongue = $this->langs->getLanguage($user->getMothertongueId())->getLanguage();
        	$xml->LearnerInfo->Skills->Linguistic->addChild('MotherTongueList')->addChild('MotherTongue')->addChild('Description');

        	$xml->LearnerInfo->Skills->Linguistic->MotherTongueList->MotherTongue->Description->addChild('Label', $mothertongue);

        }
        
        // there are no other languages
        if (! $lang->count()) {
            return $xml;
        }
        
        $foreignLanguageList = $xml->LearnerInfo->Skills->Linguistic->addChild('ForeignLanguageList');
        
        foreach ($lang as $item) {
            
            $foreignLanguage = $foreignLanguageList->addChild('ForeignLanguage');
            
            $foreignLanguage->addChild('Description')->addChild('Label', $item->language);
            
            if($item->getCertificate()){
            	$foreignLanguage->addChild('VerifiedBy')->addChild('Certificate')->addChild('Title', $item->getCertificate());
            }
            
            $foreignLanguage->addChild('ProficiencyLevel');
            $foreignLanguage->ProficiencyLevel->addChild('Listening', $item->getUnderstandingListening());
            $foreignLanguage->ProficiencyLevel->addChild('Reading', $item->getUnderstandingReading());
            $foreignLanguage->ProficiencyLevel->addChild('SpokenInteraction', $item->getSpokenInteraction());
            $foreignLanguage->ProficiencyLevel->addChild('SpokenProduction', $item->getSpokenProduction());
            $foreignLanguage->ProficiencyLevel->addChild('Writing', $item->getWriting());
        }
        
        return $xml;
    }

    /**
     * Create the Compentences and Skills section of the XML
     *
     * @return \SimpleXMLElement
     */
    private function createCompetences ()
    {
        $skills = $this->getCompetences();
        $xml = $this->xml;

        if($skills){
            
            if($skills->getSocial()){
            	$xml->LearnerInfo->Skills->addChild('Communication')->addChild('Description')->addCdata($this->wiki->__invoke($skills->getSocial()));

            }
            
            if($skills->getOrganisational()){
            	$xml->LearnerInfo->Skills->addChild('Organisational')->addChild('Description')->addCdata($this->wiki->__invoke($skills->getOrganisational()));

            }

            $computer = $skills->getComputer() . " " . $skills->getTechnical();

            if (!empty($computer)) {
                $xml->LearnerInfo->Skills->addChild('Computer')->addChild('Description')->addCdata($this->wiki->__invoke($computer));
            }

            if($skills->getDriving()){
            	$xml->LearnerInfo->Skills->addChild('Driving')->addChild('Description')->addChild('Licence')->addCdata($this->wiki->__invoke($skills->getDriving()));
            }

            if($skills->getPersonal()){
            	$xml->LearnerInfo->Skills->addChild('Other')->addChild('Description')->addCdata($this->wiki->__invoke($skills->getPersonal()));
            }

            if ($skills->getOther()) {
                $xml->LearnerInfo->Skills->addChild('Other')->addChild('Description')->addCdata($this->wiki->__invoke($skills->getOther()));
            }

            $achivement = $xml->LearnerInfo->addChild('AchievementList')->addChild('Achievement');
            $achivement->addChild('Title')->addChild('Code', 'publications');
            $achivement->Title->addChild('Label', 'Publications');
            $achivement->addChild('Description')->addCdata($skills->getPublications());
        }
        return $xml;
    }

    private function addCoverLetter(){
    	$user = $this->getUser();

    	$xml = $this->xml;
    	$coverletter = $this->getCoverletter();
    	
    	if (empty($coverletter)){
    		return $xml;
    	}
    	

    	// create new PDF document

    	$pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    	

    	// set document information

    	$pdf->SetCreator(PDF_CREATOR);

    	$pdf->SetAuthor('EuroCv.eu');

    	$pdf->SetTitle('Cover Letter');

    	$pdf->SetSubject('Cover letter');

    	$pdf->SetKeywords('cover letter, eurocv, curriculum vitae');

    	

    	// set default header data

//     	$pdf->SetHeaderData("/img/logo_eurocv.png", PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));

    	$pdf->setFooterData(array(0,64,0), array(0,64,128));

    	

    	// set header and footer fonts

    	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

    	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    	

    	// set default monospaced font

    	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    	

    	// set margins

    	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

    	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

    	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    	

    	// set auto page breaks

    	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    	

    	// set image scale factor

    	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    	 
    	

    	// set default font subsetting mode

    	$pdf->setFontSubsetting(true);

    	

    	// Set font

    	// dejavusans is a UTF-8 Unicode font, if you only need to

    	// print standard ASCII chars, you can use core fonts like

    	// helvetica or times to reduce file size.

    	$pdf->SetFont('dejavusans', '', 12, '', true);

    	

    	// Add a page

    	// This method has several options, check the source code documentation for more information.

    	$pdf->AddPage();

    	

    	// set text shadow effect

//     	$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

    	

    	// get the coverletter
    	$strCoverletter = null;
    	if($coverletter){
    		$strCoverletter = $this->wiki->__invoke($coverletter->getCoverletter());
    	}

    	

    	// Print text using writeHTMLCell()

    	$pdf->writeHTMLCell(0, 0, '', '', $strCoverletter, 0, 1, 0, true, '', true);

    	

    	// ---------------------------------------------------------

    	

     	$theCoverLetter = $pdf->Output('example_001.pdf', 'S'); // as string
//    	$theCoverLetter = $pdf->Output('example_001.pdf', 'I'); // print in the screen
    	
     	// Create a unique code
     	$code = 'ATT_'. date('Ymdhis');
    	
    	$attachment = $xml->addChild('AttachmentList')->addChild('Attachment');

    	$attachment->addAttribute('id', $code);

    	$attachment->addChild('Name', 'coverletter.pdf');
    	$attachment->addChild('MimeType', 'application/pdf');
    	$attachment->addChild('Data', base64_encode($theCoverLetter));

    	$attachment->addChild('MetadataList')->addChild('Metadata');
    	$attachment->addChild('Description', 'coverletter.pdf');

    	$attachment->MetadataList->Metadata->addAttribute('key', 'number-of-pages');
    	$attachment->MetadataList->Metadata->addAttribute('value', '2');
    	
    	$xml->LearnerInfo->addChild('Documentation')->addChild('ReferenceTo')->addAttribute('idref', $code);
    	return $xml;
    }
    
    /**
     * Adding the photo to the XML
     *
     * @return \SimpleXMLElement
     */
    private function addPhoto ()
    {
        $user = $this->getUser();
        $xml = $this->xml;
        $matches = glob(PUBLIC_PATH . '/photos/' . $user->getPac() . ".*");
        
        if(!empty($matches[0]) && file_exists($matches[0])){
            
            // Get the mimetype of the photo
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimetype = finfo_file($finfo, $matches[0]);
            finfo_close($finfo);
            
            // Get the image size
            list($width, $height, $type, $attr) = getimagesize($matches[0]);
            
            // Convert the photo into a string
            $strphoto = file_get_contents($matches[0]);
            $strphoto = base64_encode($strphoto);
            
            $xml->LearnerInfo->Identification->addChild('Photo');
            $xml->LearnerInfo->Identification->Photo->addChild('MimeType', $mimetype);

            $xml->LearnerInfo->Identification->Photo->addChild('Data', $strphoto);
            
            $xml->LearnerInfo->Identification->Photo->addChild('MetadataList')->addChild('Metadata');
            $xml->LearnerInfo->Identification->Photo->MetadataList->Metadata->addAttribute('key', 'dimension');
            $xml->LearnerInfo->Identification->Photo->MetadataList->Metadata->addAttribute('value', $width . "x" . $height);
        }
        return $xml;
    }

    /**
     * Create the Europass file
     */
    public function build ()
    {
		$xml = $this->setDocumentInfo();
    	$xml = $this->createPersonaldata();
        $xml = $this->addPhoto();
        $xml = $this->createHeadline();
        $xml = $this->createWorkexperience();
        $xml = $this->createEducation();
        $xml = $this->createLanguage();
        $xml = $this->createCompetences();
        $xml = $this->addCoverLetter();
        $data = $xml->asXML();

        if ($this->getDebug()) {
            die($data);
        }
        
        $apiUrl = "https://europass.cedefop.europa.eu/rest/v1/";
        $apiUrl .= $this->getCommand();
        
        $resume = $this->call('POST', $apiUrl, $data, $this->locale); // call API call method
        
        if(strpos($resume, "exception")){
	        header('Content-Type: text/xml;');
	        print_r($resume);
	        die;
        }
        
        return $resume;
    }

    /**
     *
     * @param s: $method
     *            = POST, PUT, GET etc...
     *            : $data = array('param' => 'value') == api.php?param=value
     */
    protected function call ($method, $url, $data = false, $language = "en")
    {
        $curl = curl_init();
        
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }
        
        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPHEADER, array ( 
                'Content-Type: application/xml',
                "Accept-Language: $language"
        ));
//         curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//         curl_setopt($curl, CURLOPT_USERPWD, "username:password");
        
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        
        return curl_exec($curl);
    }

    
}
