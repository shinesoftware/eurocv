<?php
namespace Cv\Model;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Stdlib\Hydrator\ClassMethods;
class LangsTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return the $tableGateway
     */
    public function getTableGateway ()
    {
        return $this->tableGateway;
    }
    
    public function saveData (Langs $langs)
    {
       $hydrator = new ClassMethods(true);
        
        // extract the data from the object
        $data = $hydrator->extract($langs);
        
        $id = (int) $langs->getId();
        
        if ($id == 0) {
        	unset($data['id']);
            $this->tableGateway->insert($data); // add the record
            $id = $this->tableGateway->getLastInsertValue();
        } else {
        	$rs = $this->getLangs($id);
            if (!empty($rs)) {
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
            } else {
                throw new \Exception('Education ID does not exist');
            }
        }
        
        $langs = $this->getLangs($id, $data['personaldata_id']);
        return $langs;
    }

    /**
     * Get all records
     *
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        
        return $resultSet;
    }

    /**
     * Get Language by id
     *
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getLanguage ($id)
    {
        if (is_numeric($id)) {
            $id = (int) $id;
            $rowset = $this->tableGateway->select(array ( 
                    'id' => $id, 
            ));
            
            $row = $rowset->current();
            if (! $row) {
                return false;
            }
            return $row;
        }
        
        return false;
    }

    /**
     * Get Last record
     *
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getLast ()
    {
        $rowset = $this->tableGateway->select(function  (Select $select)
        {
            $select->order('id DESC')->limit(1);
        });
        
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find the record");
        }
        return $row;
    }


    /**
     * Delete the record
     *
     * @param integer $id            
     */
    public function deleteLangs ($id)
    {
        $this->tableGateway->delete(array ( 
                'id' => $id, 
        ));
    }
}
