<?php
namespace Application\Controller;
use Application\Form\CvRequestForm;
use Zend\Mail\Transport;
use Zend\Mail\Message as Message;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PrivateCvController extends AbstractActionController
{

    protected $form;
    protected $transport;
    protected $pac;
    protected $user;
    protected $translation_id;
    protected $translator;
    
    /**
     * OnDispatch of the controller
     * (non-PHPdoc)
     * 
     * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
     */
    public function onDispatch (\Zend\Mvc\MvcEvent $e)
    {
        $locale = "en_US";
        
        $this->pac = $this->params()->fromRoute('pac');
        $translation = $this->getServiceLocator()->get('TranslationTable');
        
        // Get the TableGateway object to retrieve the data
        $user = $this->getServiceLocator()->get('PdMapper');
        
        $this->translator = $e->getApplication()->getServiceManager()->get('translator');
        
        // Get the user
        $this->user = $user->findUser('pac', $this->pac);
        
        // get the default language set by the user
        $translation_id = $this->user->getTranslationId();
        $locale = $translation->getTranslation($translation_id)->getLocale();
        
        // Set the base locale for the session
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $translator->setLocale($locale)->setFallbackLocale('en_US');
        
        return parent::onDispatch($e);
    }
    
    public function setContactForm (CvRequestForm $form)
    {
        $this->form = $form;
    }

    public function setMailTransport (Transport\TransportInterface $transport)
    {
        $this->transport = $transport;
    }

    public function indexAction ()
    {
        return array ( 
                'form' => $this->form, 
                'user' => $this->user, 
                'pac' => $this->pac
        );
    }

    public function processAction ()
    {
        if (! $this->request->isPost()) {
            return $this->redirect()->toRoute('pac/private', array ( 
                    'pac' => $this->pac
            ));
        }
        
        $post = $this->request->getPost();
        $form = $this->form;
        
        $form->setData($post);
        
        if (! $form->isValid()) {
            $model = new ViewModel(array ( 
                    'error' => true, 
                    'form' => $form, 
                    'user' => $this->user, 
                    'pac' => $this->pac
            ));
            $model->setTemplate('application/private-cv/index');
            return $model;
        }
        
        // send email...
        $this->sendEmail($form->getData());
        
        return $this->redirect()->toRoute('pac/private', array ( 
                    'pac' => $this->pac,
                    'action' => 'confirm'
            ));
    }

    public function confirmAction ()
    {
        $model = new ViewModel();
        $model->setTemplate('application/private-cv/confirm');
        return $model;
        
    }

    protected function sendEmail (array $data)
    {
        $translator = $this->translator;
        
        $subject = sprintf($translator->translate('[EuroCv Request] - %s would like to see your private curriculum vitae'), $data['fullname']);
        $body = $data['request'];
        
        $message = new Message();
        $message->addFrom("staff@eurocv.eu")
            ->addReplyTo("staff@eurocv.eu")
            ->setSubject($subject)
            ->setBody($body);
        
        $user = $this->user;
        $message->addTo($user->getEmail(), $user->getLastname());
        $this->transport->send($message);
    }
}
