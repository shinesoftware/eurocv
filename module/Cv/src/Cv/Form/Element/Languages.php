<?php
namespace Cv\Form\Element;

use Cv\Model\olTable;
use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\I18n\Translator\Translator;

class Languages extends Select implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $translator;
    protected $olTable;
    protected $personaldata_id;
    
    public function __construct(olTable $olTable, $idPersonaldata, \Zend\Mvc\I18n\Translator $translator){
        parent::__construct();
        $this->olTable = $olTable;
        $this->personaldata_id = $idPersonaldata;
        $this->translator = $translator;
    }
    
    public function init()
    {
        $data = array();
        $this->setEmptyOption($this->translator->translate('Please select ...'));
        
        $language = $this->olTable->fetchAllbyId($this->personaldata_id);
        foreach ($language as $lg){
            $data[$lg->getId()] = $lg->language;
        }

        $this->setValueOptions($data);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
