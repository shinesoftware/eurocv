<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Cv;
use Zend\Mail\Address;
use Cv\Form\Element\JobCategories;
use Cv\Form\PacFilter;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Mvc\MvcEvent;
use Cv\Model\Pd;
use Cv\Model\PdTable;
use Cv\Model\CommonData;
use Cv\Model\PdMapper;
use Cv\Model\We;
use Cv\Model\WeTable;
use Cv\Model\Et;
use Cv\Model\EtTable;
use Cv\Model\Ol;
use Cv\Model\OlTable;
use Cv\Model\Cs;
use Cv\Model\CsTable;
use Cv\Model\Cl;
use Cv\Model\ClTable;
use Cv\Model\Uploads;
use Cv\Model\UploadsTable;
use Cv\Model\Langs;
use Cv\Model\LangsTable;
use Cv\Model\Country;
use Cv\Model\CountryTable;
use Cv\Model\Addresses;
use Cv\Model\AddressesTable;
use Cv\Model\Contacts;
use Cv\Model\ContactsTable;
use Cv\Model\ContactTypes;
use Cv\Model\ContactTypesTable;
use Cv\Model\JobCategory;
use Cv\Model\JobCategoryTable;
use Cv\Model\JobTermination;
use Cv\Model\JobTerminationTable;
use Cv\Model\Translation;
use Cv\Model\TranslationTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module implements AutoloaderProviderInterface
{

    public function getAutoloaderConfig ()
    {
        return array ( 
                'Zend\Loader\ClassMapAutoloader' => array ( 
                        __DIR__ . '/autoload_classmap.php'
                ), 
                'Zend\Loader\StandardAutoloader' => array ( 
                        'namespaces' => array ( 
                                // if we're in a namespace deeper than one level
                                // we need to fix the \ in the path
                                __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__)
                        )
                )
        );
    }

    /**
     * Set the Services Manager items
     */
    public function getServiceConfig ()
    {
        return array ( 
                'abstract_factories' => array (), 
                'aliases' => array (), 
                'factories' => array ( 
                        
                        'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory', 
                        'CommonData' => function  ($sm)
                        {
                            $pdtable = $sm->get('PdTable');
                            $addresstable = $sm->get('AddressesTable');
                            $contacttable = $sm->get('ContactsTable');
                            $wetable = $sm->get('WeTable');
                            $ettable = $sm->get('EtTable');
                            $oltable = $sm->get('OlTable');
                            $cstable = $sm->get('CsTable');
                        
                            $mapper = new CommonData($pdtable, $addresstable, $contacttable, $wetable, $ettable, $oltable, $cstable);
                        
                            return $mapper;
                        },
                        // DB
                        'PdTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('PdTableGateway');
                            $table = new PdTable($tableGateway);
                            return $table;
                        }, 
                        'PdTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            
                            $resultSet = new HydratingResultSet();
                            $resultSet->setHydrator(new ClassMethods(true));
                            $resultSet->setObjectPrototype(new Pd());
                            return new TableGateway('personal_data', $dbAdapter, null, $resultSet);
                        }, 
                        'PdMapper' => function  ($sm)
                        {
                            $pdtable = $sm->get('PdTable');
                            $addresstable = $sm->get('AddressesTable');
                            $contacttable = $sm->get('ContactsTable');
                            
                            $mapper = new PdMapper($pdtable, $addresstable, $contacttable);
                            
                            return $mapper;
                        }, 
                        'WeTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('WeTableGateway');
                            $table = new WeTable($tableGateway);
                            return $table;
                        }, 
                        'WeTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new We());
                            return new TableGateway('work_experiences', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'EtTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('EtTableGateway');
                            $table = new EtTable($tableGateway);
                            return $table;
                        }, 
                        'EtTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Et());
                            return new TableGateway('education', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'OlTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('OlTableGateway');
                            $table = new OlTable($tableGateway);
                            return $table;
                        }, 
                        'OlTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Ol());
                            return new TableGateway('languages', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'CsTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('CsTableGateway');
                            $table = new CsTable($tableGateway);
                            return $table;
                        }, 
                        'CsTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Cs());
                            return new TableGateway('competences', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'ClTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('ClTableGateway');
                            $table = new ClTable($tableGateway);
                            return $table;
                        }, 
                        'ClTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Cl());
                            return new TableGateway('coverletters', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'UploadsTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('UploadsTableGateway');
                            $table = new UploadsTable($tableGateway);
                            return $table;
                        }, 
                        'UploadsTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Uploads());
                            return new TableGateway('uploads', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'CountryTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('CountryTableGateway');
                            $table = new CountryTable($tableGateway);
                            return $table;
                        }, 
                        'CountryTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Country());
                            return new TableGateway('country', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'JobCategoryTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('JobCategoryTableGateway');
                            $table = new JobCategoryTable($tableGateway);
                            return $table;
                        }, 
                        'JobCategoryTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new JobCategory());
                            return new TableGateway('job_category', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'AddressesTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('AddressesTableGateway');
                            $countryGateway = $sm->get('CountryTable');
                            $table = new AddressesTable($tableGateway, $countryGateway);
                            return $table;
                        }, 
                        'AddressesTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Addresses());
                            return new TableGateway('addresses', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'ContactsTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('ContactsTableGateway');
                            $table = new ContactsTable($tableGateway);
                            return $table;
                        }, 
                        'ContactsTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Contacts());
                            return new TableGateway('contacts', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'ContactTypesTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('ContactTypesTableGateway');
                            $table = new ContactTypesTable($tableGateway);
                            return $table;
                        }, 
                        'ContactTypesTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new ContactTypes());
                            return new TableGateway('contact_types', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'JobTerminationTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('JobTerminationTableGateway');
                            $table = new JobTerminationTable($tableGateway);
                            return $table;
                        }, 
                        'JobTerminationTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new JobTermination());
                            return new TableGateway('jobtermination', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'TranslationTable' => function  ($sm)
                        {
                            $tableGateway = $sm->get('TranslationTableGateway');
                            $table = new TranslationTable($tableGateway);
                            return $table;
                        }, 
                        'TranslationTableGateway' => function  ($sm)
                        {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Translation());
                            return new TableGateway('translation', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        
                        'LangsTable' => function  ($sm)
                        {
                        	$tableGateway = $sm->get('LangsTableGateway');
                        	$table = new LangsTable($tableGateway);
                        	return $table;
                        },
                        'LangsTableGateway' => function  ($sm)
                        {
                        	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        	$resultSetPrototype = new ResultSet();
                        	$resultSetPrototype->setArrayObjectPrototype(new Langs());
                        	return new TableGateway('langs', $dbAdapter, null, $resultSetPrototype);
                        },
                        
                        'UploadForm' => function  ($sm)
                        {
                            $form = new \Cv\Form\UploadForm();
                            return $form;
                        }, 
                        
                        'UploadEditForm' => function  ($sm)
                        {
                            $form = new \Cv\Form\UploadEditForm();
                            return $form;
                        }, 
                        'UploadEditFilter' => function  ($sm)
                        {
                            return new \Cv\Form\UploadEditFilter();
                        }, 
                        'UploadFilter' => function  ($sm)
                        {
                            return new \Cv\Form\UploadFilter();
                        }, 
                        'PdForm' => function  ($sm)
                        {
                            $form = new \Cv\Form\PdForm();
                            return $form;
                        }, 
                        
                        'WeForm' => function  ($sm)
                        {
                            $form = new \Cv\Form\WeForm();
                            $form->setInputFilter($sm->get('WeFilter'));
                            return $form;
                        }, 
                        'WeFilter' => function  ($sm)
                        {
                            return new \Cv\Form\WeFilter();
                        }, 
                        
                        'EtForm' => function  ($sm)
                        {
                            $form = new \Cv\Form\EtForm();
                            $form->setInputFilter($sm->get('EtFilter'));
                            return $form;
                        }, 
                        'EtFilter' => function  ($sm)
                        {
                            return new \Cv\Form\EtFilter();
                        }, 
                        
                        'OlForm' => function  ($sm)
                        {
                            $form = new \Cv\Form\OlForm();
                            $form->setInputFilter($sm->get('OlFilter'));
                            return $form;
                        }, 
                        'OlFilter' => function  ($sm)
                        {
                            return new \Cv\Form\OlFilter();
                        }, 
                        
                        'CsForm' => function  ($sm)
                        {
                            $form = new \Cv\Form\CsForm();
                            $form->setInputFilter($sm->get('CsFilter'));
                            return $form;
                        }, 
                        'CsFilter' => function  ($sm)
                        {
                            return new \Cv\Form\CsFilter();
                        },
                        
                        'ClForm' => function  ($sm)
                        {
                            $form = new \Cv\Form\ClForm();
                            $form->setInputFilter($sm->get('ClFilter'));
                            return $form;
                        }, 
                        'ClFilter' => function  ($sm)
                        {
                            return new \Cv\Form\ClFilter();
                        }
                ), 
                'invokables' => array (), 
                'services' => array (), 
                'shared' => array ()
        );
    }

    public function getFormElementConfig ()
    {
        return array ( 
                'invokables' => array (), 
                'factories' => array ( 
                        'Cv\Form\Fieldset\PdFieldset' => function  ($fem)
                        {
                            $sm = $fem->getServiceLocator();
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $fieldset = new \Cv\Form\Fieldset\PdFieldset($dbAdapter);
                            return $fieldset;
                        },
                        
                        'Cv\Form\Element\Country' => function  ($sm)
                        {
                            $serviceLocator = $sm->getServiceLocator();
                            $CountryTable = $serviceLocator->get('CountryTable');
                            $element = new \Cv\Form\Element\Country($CountryTable);
                            return $element;
                        }, 
                        'Cv\Form\Element\JobCategories' => function  ($sm)
                        {
                            $serviceLocator = $sm->getServiceLocator();
                            $translator = $sm->getServiceLocator()->get('translator');
                            $JobCategoryTable = $serviceLocator->get('JobCategoryTable');
                            $element = new \Cv\Form\Element\JobCategories($JobCategoryTable, $translator);
                            return $element;
                        }, 
                        'Cv\Form\Element\Translations' => function  ($sm)
                        {
                            $serviceLocator = $sm->getServiceLocator();
                            $translator = $sm->getServiceLocator()->get('translator');
                            $TranslationTable = $serviceLocator->get('TranslationTable');
                            $element = new \Cv\Form\Element\Translations($TranslationTable, $translator);
                            return $element;
                        }, 
                        'Cv\Form\Element\JobTerminations' => function  ($sm)
                        {
                            $serviceLocator = $sm->getServiceLocator();
                            $translator = $sm->getServiceLocator()->get('translator');
                            $JobTerminationTable = $serviceLocator->get('JobTerminationTable');
                            $element = new \Cv\Form\Element\JobTerminations($JobTerminationTable, $translator);
                            return $element;
                        }, 
                        'Cv\Form\Element\ContactTypes' => function  ($sm)
                        {
                            $serviceLocator = $sm->getServiceLocator();
                            $translator = $sm->getServiceLocator()->get('translator');
                            
                            $contactTypesTable = $serviceLocator->get('ContactTypesTable');
                            $element = new \Cv\Form\Element\ContactTypes($contactTypesTable, $translator);
                            return $element;
                        }, 
                        'Cv\Form\Element\WorkExperiences' => function  ($sm)
                        {
                            $serviceLocator = $sm->getServiceLocator();
                            $translator = $sm->getServiceLocator()->get('translator');
                            
                            // Get the identity information
                            $auth = $serviceLocator->get('zfcuser_auth_service');
                            $Identity = $auth->getIdentity();
                            $userId = $Identity->getId();
                            
                            // Get the TableGateway object to retrieve the data
                            $user = $serviceLocator->get('PdTable');
                            
                            // Get the user
                            $myUser = $user->getPdByUserId($userId);
                            
                            $personaldataId = null;
                            $WeTable = $serviceLocator->get('WeTable');
                            if(!empty($myUser)){
                                $personaldataId = $myUser->getId();
                            }
                            $element = new \Cv\Form\Element\WorkExperiences($WeTable, $personaldataId, $translator);
                            return $element;
                        }, 
                        'Cv\Form\Element\Educations' => function  ($sm)
                        {
                            $serviceLocator = $sm->getServiceLocator();
                            $translator = $sm->getServiceLocator()->get('translator');
                            
                            // Get the identity information
                            $auth = $serviceLocator->get('zfcuser_auth_service');
                            $Identity = $auth->getIdentity();
                            $userId = $Identity->getId();
                            
                            // Get the TableGateway object to retrieve the data
                            $user = $serviceLocator->get('PdTable');
                            
                            // Get the user
                            $myUser = $user->getPdByUserId($userId);
                            $personaldataId = null;
                            if(!empty($myUser)){
                                $personaldataId = $myUser->getId();
                            }
                            $EtTable = $serviceLocator->get('EtTable');
                            $element = new \Cv\Form\Element\Educations($EtTable, $personaldataId, $translator);
                            return $element;
                        },
                        'Cv\Form\Element\Languages' => function  ($sm)
                        {
                            $serviceLocator = $sm->getServiceLocator();
                            $translator = $sm->getServiceLocator()->get('translator');
                        
                            // Get the identity information
                            $auth = $serviceLocator->get('zfcuser_auth_service');
                            $Identity = $auth->getIdentity();
                            $userId = $Identity->getId();
                        
                            // Get the TableGateway object to retrieve the data
                            $user = $serviceLocator->get('PdTable');
                        
                            // Get the user
                            $myUser = $user->getPdByUserId($userId);
                            $personaldataId = null;
                            if(!empty($myUser)){
                                $personaldataId = $myUser->getId();
                            }
                            $OlTable = $serviceLocator->get('OlTable');
                            $element = new \Cv\Form\Element\Languages($OlTable, $personaldataId, $translator);
                            return $element;
                        },
                        'Cv\Form\Element\Langs' => function  ($sm)
                        {
                        	$serviceLocator = $sm->getServiceLocator();
                        	$translator = $sm->getServiceLocator()->get('translator');
                        	$LangsTable = $serviceLocator->get('LangsTable');
                        	$element = new \Cv\Form\Element\Langs($LangsTable, $translator);
                        	return $element;
                        },
                                              
                )
        );
    }

    public function getConfig ()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap (MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }
}
