<?php
namespace Cv\Model;

class Translation
{
    protected $id;
    protected $title;
    protected $active;
    protected $code;
    protected $locale;
    protected $eu;
    protected $dst;
    
	/**
     * 
     * @param array $data
     */
    function exchangeArray ($data)
    {
        foreach ($data as $field => $value){
            $this->$field = (isset($value)) ? $value : null;
        }
    }
    
    /**
     * Get the keys of the array
     * @return multitype:
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $title
     */
    public function getTitle ()
    {
        return $this->title;
    }

	/**
     * @return the $active
     */
    public function getActive ()
    {
        return $this->active;
    }

	/**
     * @return the $code
     */
    public function getCode ()
    {
        return $this->code;
    }

	/**
     * @return the $eu
     */
    public function getEu ()
    {
        return $this->eu;
    }

	/**
     * @return the $dst
     */
    public function getDst ()
    {
        return $this->dst;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $title
     */
    public function setTitle ($title)
    {
        $this->title = $title;
    }

	/**
     * @param field_type $active
     */
    public function setActive ($active)
    {
        $this->active = $active;
    }

	/**
     * @param field_type $code
     */
    public function setCode ($code)
    {
        $this->code = $code;
    }

	/**
     * @param field_type $eu
     */
    public function setEu ($eu)
    {
        $this->eu = $eu;
    }

	/**
     * @param field_type $dst
     */
    public function setDst ($dst)
    {
        $this->dst = $dst;
    }
	/**
     * @return the $locale
     */
    public function getLocale ()
    {
        return $this->locale;
    }

	/**
     * @param field_type $locale
     */
    public function setLocale ($locale)
    {
        $this->locale = $locale;
    }


	
    
    
}