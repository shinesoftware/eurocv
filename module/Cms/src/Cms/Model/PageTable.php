<?php
namespace Cms\Model;
use Zend\Db\Sql\Ddl\Column\Boolean;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mail;

class PageTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePage (Page $page)
    {
        $data = array(
                'title' => $page->title,
                'uri' => $page->uri,
                'content' => $page->content,
                'updatedat' => date('Y-m-d H:i:s')
        );
        
        $id = (int) $page->id;
        if ($id == 0) {
            $data['createdat'] = date('Y-m-d H:i:s');
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPage($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Page ID does not exist');
            }
        }
    }

    /**
     * Get all pages
     * 
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Get Page account by PageId
     * 
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getPage ($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get Page account by Uri
     * 
     * @param string $pageUri            
     * @throws \Exception
     * @return Row
     */
    public function getPageByUri ($pageUri)
    {
        $rowset = $this->tableGateway->select(array(
                'uri' => $pageUri
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $pageUri");
        }
        return $row;
    }

    /**
     * Delete Page account by PageId
     * 
     * @param string $id            
     */
    public function deletePage ($id)
    {
        $this->tableGateway->delete(array(
                'id' => $id
        ));
    }
}
