<?php
namespace Cv\Form\Element;

use Cv\Model\ContactTypesTable;
use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\I18n\Translator\Translator;

class ContactTypes extends Select implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $translator;
    protected $contactTypeTable;
    
    public function __construct(ContactTypesTable $contactTypeTable, \Zend\Mvc\I18n\Translator $translator){
        parent::__construct();
        $this->contactTypeTable = $contactTypeTable;
        $this->translator = $translator;
    }
    
    public function init()
    {
        $data = array();
        
        $records = $this->contactTypeTable->fetchAll();
        foreach ($records as $type){
            $data[$type->getId()] = $this->translator->translate($type->getType());
        }
        $this->setValueOptions($data);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
