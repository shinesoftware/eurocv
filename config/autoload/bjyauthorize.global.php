<?php

return array(
        
    'bjyauthorize' => array(

        // set the 'guest' role as default (must be defined in a role provider)
        'default_role'       => 'guest',         // not authenticated
        'authenticated_role' => 'user',          // authenticated
        'identity_provider'  => 'BjyAuthorize\Provider\Identity\ZfcUserZendDb',
        'role_providers' => array(
                
            'BjyAuthorize\Provider\Role\ZendDb' => array(
                'table'                 => 'user_role',
                'identifier_field_name' => 'id',
                'role_id_field'         => 'role_id',
                'parent_role_field'     => 'parent_id',
            ),

        ),
            
        'resource_providers' => array(
                'BjyAuthorize\Provider\Resource\Config' => array(
                        'cvmenu' => array(),
                        'adminmenu' => array(),
                ),
        ),
    
        'rule_providers' => array(
                'BjyAuthorize\Provider\Rule\Config' => array(
                        'allow' => array(
                                array(array('user'), 'cvmenu', array('list')),  // UserType, Resource, Privileges
                                array(array('admin'), 'adminmenu', array('list')),  // UserType, Resource, Privileges
                        ),
                ),
        ),

        /* Currently, only controller and route guards exist
         *
         * Consider enabling either the controller or the route guard depending on your needs.
         */
        'guards' => array(
            
            # IMPORTANT: There is a bug in this module! Fix the /vendor/bjyoungblood/bjy-authorize/src/BjyAuthorize/Provider/Role/ZendDb.php with https://gist.github.com/shinesoftware/9736775
            'BjyAuthorize\Guard\Route' => array(

                // Generic route guards
                array('route' => 'home', 'roles' => array('guest')),
                array('route' => 'application', 'roles' => array('guest')),
                array('route' => 'application/default', 'roles' => array('guest')),
                array('route' => 'europass', 'roles' => array('guest')),
                array('route' => 'gallery', 'roles' => array('guest')),

                array('route' => 'export', 'roles' => array('guest')),
                array('route' => 'download', 'roles' => array('guest')),
                array('route' => 'download/default', 'roles' => array('guest')),
                array('route' => 'pac', 'roles' => array('guest')),
                array('route' => 'pac/generic', 'roles' => array('guest')),
                array('route' => 'pac/private', 'roles' => array('guest')),
                array('route' => 'pac/default', 'roles' => array('guest')),
                array('route' => 'versions/default', 'roles' => array('guest')),
                array('route' => 'www/default', 'roles' => array('guest')),
                array('route' => 'www/showprivate', 'roles' => array('guest')),
                array('route' => 'export/default', 'roles' => array('guest')),
                array('route' => 'exportversion/default', 'roles' => array('guest')),

                // Contact module    
//                array('route' => 'contact', 'roles' => array('guest')),
//                array('route' => 'contact/process', 'roles' => array('guest')),
//                array('route' => 'contact/thank-you', 'roles' => array('guest')),
                   
                // ZfcUser module
                array('route' => 'zfcuser', 'roles' => array('guest')),
                array('route' => 'zfcuser/index', 'roles' => array('user')),
                array('route' => 'zfcuser/logout', 'roles' => array('user')),
                array('route' => 'zfcuser/login', 'roles' => array('guest')),
                array('route' => 'zfcuser/forgotpassword', 'roles' => array('guest')),
                array('route' => 'zfcuser/resetpassword', 'roles' => array('guest')),
                array('route' => 'zfcuser/changepassword', 'roles' => array('user')),
                array('route' => 'zfcuser/changeemail', 'roles' => array('user')),
                array('route' => 'zfcuser/register', 'roles' => array('guest')),

                // Social Auth Module
                array('route' => 'scn-social-auth-hauth', 'roles' => array('guest')),
                array('route' => 'scn-social-auth-user', 'roles' => array('guest')),
                array('route' => 'scn-social-auth-user/authenticate/provider', 'roles' => array('guest')),
                array('route' => 'scn-social-auth-user/authenticate', 'roles' => array('guest')),
                array('route' => 'scn-social-auth-user/login', 'roles' => array('guest')),
                array('route' => 'scn-social-auth-user/logout', 'roles' => array('guest')),
                array('route' => 'scn-social-auth-user/register', 'roles' => array('guest')),
                array('route' => 'scn-social-auth-user/login/provider', 'roles' => array('guest')),
                    
                // Curriculum Vitae user administration
                array('route' => 'cv', 'roles' => array('user')),
                array('route' => 'cv/default', 'roles' => array('user')),
                array('route' => 'cv/pd', 'roles' => array('user')),
                array('route' => 'cv/we', 'roles' => array('user')),
                array('route' => 'cv/et', 'roles' => array('user')),
                array('route' => 'cv/ol', 'roles' => array('user')),
                array('route' => 'cv/cs', 'roles' => array('user')),
                array('route' => 'cv/cl', 'roles' => array('user')),
                array('route' => 'cv/uploads', 'roles' => array('user')),
                    
                //Admin module
                array('route' => 'zfcadmin', 'roles' => array('admin')),
                array('route' => 'zfcadmin/users', 'roles' => array('admin')),
                array('route' => 'zfcadmin/users/default', 'roles' => array('admin')),

            ),
        ),
    ),
);