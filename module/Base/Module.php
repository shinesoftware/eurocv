<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Base;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Cv\Model\Pd;
use Cv\Model\PdTable;
use Cv\Model\We;
use Cv\Model\WeTable;
use Cv\Model\Et;
use Cv\Model\EtTable;
use Cv\Model\Ol;
use Cv\Model\OlTable;
use Cv\Model\Cs;
use Cv\Model\CsTable;
use Cv\Model\Uploads;
use Cv\Model\UploadsTable;
use Cv\Model\Country;
use Cv\Model\CountryTable;
use Cv\Model\JobCategory;
use Cv\Model\JobCategoryTable;
use Cv\Model\JobTermination;
use Cv\Model\JobTerminationTable;
use Cv\Model\Translation;
use Cv\Model\TranslationTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;
use Zend\Session\Container;

class Module implements AutoloaderProviderInterface
{

    public function getAutoloaderConfig ()
    {
        return array ( 
                'Zend\Loader\ClassMapAutoloader' => array ( 
                        __DIR__ . '/autoload_classmap.php'
                ), 
                'Zend\Loader\StandardAutoloader' => array ( 
                        'namespaces' => array ( 
                                // if we're in a namespace deeper than one level
                                // we need to fix the \ in the path
                                __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__)
                        )
                )
        );
    }

    /**
     * Set the Services Manager items
     */
    public function getServiceConfig ()
    {
        return array ( 
                'abstract_factories' => array (), 
                'aliases' => array (), 
                'factories' => array (), 
                'invokables' => array ( 
                        'goalioforgotpassword_password_service' => 'Base\Service\Password', 
                        'ZfcUser\Authentication\Adapter\Db' => 'Base\Authentication\Adapter\Db', 
                        'zfcuser_user_service' => 'Base\Service\Eurocvuser'
                ), 
                'services' => array (), 
                'shared' => array ()
        );
    }

    public function getViewHelperConfig ()
    {
        return array ( 
                'factories' => array ( 
                        'flashMessage' => function  ($sm)
                        {
                            $flashmessenger = $sm->getServiceLocator()->get('ControllerPluginManager')->get('flashmessenger');
                            $message = new \Base\View\Helper\FlashMessages();
                            $message->setFlashMessenger($flashmessenger);
                            return $message;
                        }
                )
        );
    }

    public function getConfig ()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap (MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        $config = $e->getApplication()->getServiceManager()->get('Configuration');
			
        // get the host name like eurocv.it or eurocv.eu for the session cookie
        $host_names = explode(".", $_SERVER['SERVER_NAME']);
		$host = $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1];
        
        $config['session']['cookie_domain'] = "." . $host;
        
        // Start the session
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config['session']);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();
        
        $session = new Container('base');
        
        // Get the visitor language selection
        $locale = $session->offsetGet('locale'); // Get the locale
        if (! empty($locale)) {
            $translator = $e->getApplication()->getServiceManager()->get('translator');
            $translator->setLocale($locale)->setFallbackLocale('en_US');
        }
        
        // Add ACL information to the Navigation view helper
        $authorize = $e->getApplication()->getServiceManager()->get('BjyAuthorize\Service\Authorize');
        $acl = $authorize->getAcl();
        $role = $authorize->getIdentity();
        \Zend\View\Helper\Navigation::setDefaultAcl($acl);
        \Zend\View\Helper\Navigation::setDefaultRole($role);
        
        // BjyAuthorize user role configuration
        $adapter = $e->getApplication()->getServiceManager()->get('Zend\Db\Adapter\Adapter');
        
        // adding action for user registration
        $zfcServiceEvents = $e->getApplication()->getServiceManager()->get('zfcuser_user_service')->getEventManager();
        $zfcServiceEvents->attach('register.post', function  ($e) use( $adapter)
        {
            $user = $e->getParam('user'); // User account object
            $id = $user->getId(); // get user id

            $adapter->query('INSERT INTO
                                user_role_linker (user_id, role_id)
                                VALUES
                                (' . $id . ', "user")', \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);

        });
    }
}