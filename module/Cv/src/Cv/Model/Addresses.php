<?php
namespace Cv\Model;

class Addresses
{
    public $id;
    public $street;
    public $code;
    public $city;
    public $country_id;
    public $latitude;
    public $longitude;
    public $visible;
    public $showmap;
    public $personaldata_id;
    public $createdat;
    public $updatedat;

    /**
     * 
     * @param array $data
     */
    function exchangeArray ($data)
    {
        foreach ($data as $field => $value){
            $this->$field = (isset($value)) ? $value : null;
        }
    }
    
    /**
     * Get the keys of the array
     * @return multitype:
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $street
     */
    public function getStreet ()
    {
        return $this->street;
    }

	/**
     * @return the $code
     */
    public function getCode ()
    {
        return $this->code;
    }

	/**
     * @return the $city
     */
    public function getCity ()
    {
        return $this->city;
    }

	/**
     * @return the $country_id
     */
    public function getCountryId ()
    {
        return $this->country_id;
    }

	/**
     * @return the $latitude
     */
    public function getLatitude ()
    {
        return $this->latitude;
    }

	/**
     * @return the $longitude
     */
    public function getLongitude ()
    {
        return $this->longitude;
    }

	/**
     * @return the $visible
     */
    public function getVisible ()
    {
        return $this->visible;
    }

	/**
     * @return the $showmap
     */
    public function getShowmap ()
    {
        return $this->showmap;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $street
     */
    public function setStreet ($street)
    {
        $this->street = $street;
    }

	/**
     * @param field_type $code
     */
    public function setCode ($code)
    {
        $this->code = $code;
    }

	/**
     * @param field_type $city
     */
    public function setCity ($city)
    {
        $this->city = $city;
    }

	/**
     * @param field_type $country_id
     */
    public function setCountryId ($country_id)
    {
        $this->country_id = $country_id;
    }

	/**
     * @param field_type $latitude
     */
    public function setLatitude ($latitude)
    {
        $this->latitude = $latitude;
    }

	/**
     * @param field_type $longitude
     */
    public function setLongitude ($longitude)
    {
        $this->longitude = $longitude;
    }

	/**
     * @param field_type $visible
     */
    public function setVisible ($visible)
    {
        $this->visible = $visible;
    }

	/**
     * @param field_type $showmap
     */
    public function setShowmap ($showmap)
    {
        $this->showmap = $showmap;
    }
	
	/**
     * @return the $personaldata_id
     */
    public function getPersonaldataId ()
    {
        return $this->personaldata_id;
    }

	/**
     * @return the $createdat
     */
    public function getCreatedat ()
    {
        return $this->createdat;
    }

	/**
     * @return the $updatedat
     */
    public function getUpdatedat ()
    {
        return $this->updatedat;
    }

	/**
     * @param field_type $personaldata_id
     */
    public function setPersonaldataId ($personaldata_id)
    {
        $this->personaldata_id = $personaldata_id;
    }

	/**
     * @param field_type $createdat
     */
    public function setCreatedat ($createdat)
    {
        $this->createdat = $createdat;
    }

	/**
     * @param field_type $updatedat
     */
    public function setUpdatedat ($updatedat)
    {
        $this->updatedat = $updatedat;
    }


    
}