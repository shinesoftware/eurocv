<?php
namespace Cv\Form;
use Zend\InputFilter\InputFilter;

class OlFilter extends InputFilter
{

    public function __construct ()
    {
        $this->add(array (
                'name' => 'lang_id',
                'required' => true
        ));
    }
}