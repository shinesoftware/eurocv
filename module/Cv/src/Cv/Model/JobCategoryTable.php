<?php
namespace Cv\Model;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class JobCategoryTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveJobCategory (JobCategory $jobcategory)
    {
        $data = array ( 
                'category' => $jobcategory->category
        );
        
        $id = (int) $jobcategory->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getJobCategory($id)) {
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
            } else {
                throw new \Exception('JobCategory ID does not exist');
            }
        }
    }

    /**
     * Get all users
     *
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Get JobCategory account by JobCategoryId
     *
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getJobCategory ($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array ( 
                'id' => $id
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get JobCategory account by parent
     *
     * @param int $jobcategoryid            
     * @throws \Exception
     * @return Row
     */
    public function getJobCategoryByParent ($jobcategoryid)
    {
        $rowset = $this->tableGateway->select(array ( 
                'idfather' => $jobcategoryid
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $jobcategoryid");
        }
        return $row;
    }

    /**
     * Delete JobCategory by JobCategoryId
     *
     * @param string $id            
     */
    public function deleteJobCategory ($id)
    {
        $this->tableGateway->delete(array ( 
                'id' => $id
        ));
    }
}
