<?php
namespace Cms\Form;

use Zend\Form\Form;

class PageForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Pages');
        $this->setAttribute('method', 'post');

        $this->add(array(
                'name' => 'id',
                'attributes' => array(
                        'type'  => 'hidden',
                ),
        ));
        
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type'  => 'text',
				'required' => 'required' 
            ),
            'options' => array(
                'label' => 'Title',
            ),
        )); 
        
        $this->add(array(
            'name' => 'uri',
            'attributes' => array(
                'type'  => 'text',
				'required' => 'required' 
            ),
            'options' => array(
                'label' => 'Uri',
            ),
        )); 
        
        $this->add(array(
            'name' => 'content',
            'attributes' => array(
                'type'  => 'textarea',
				'required' => 'required' 
            ),
            'options' => array(
                'label' => 'Content',
            ),
        )); 
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Send Request'
            ),
        )); 
    }
}
