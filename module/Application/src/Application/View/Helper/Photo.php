<?php 
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Photo extends AbstractHelper implements ServiceLocatorAwareInterface  
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function __invoke($userId)
    {
    	$filename = null;
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();
        $user = $serviceLocator->get('PdTable');
        $theuser = $user->getPdByUserId($userId);
        
        if($theuser){
        	$matches = glob(PUBLIC_PATH . '/photos/' . $theuser->getPac() . "*");
        	if(!empty($matches[0])){
        		$filepath = $matches[0];
        		$filename = basename($filepath);
        	}
        }
        
        return $this->view->render('application/partial/photo', array('user' => $theuser, 'filename' => $filename));
    }
}