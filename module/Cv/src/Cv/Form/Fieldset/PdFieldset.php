<?php
namespace Cv\Form\Fieldset;
use Cv\Model\Pd;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Cv\Hydrator\Strategy\DateTimeStrategy;
use Zend\Validator\Hostname as HostnameValidator;

class PdFieldset extends Fieldset implements InputFilterProviderInterface {
	
	protected $dbAdapter;
	
	public function __construct($dbAdapter) {
		parent::__construct ( 'pdFieldset' );
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 *
	 * @return the $dbAdapter
	 */
	public function getDbAdapter() {
		return $this->dbAdapter;
	}
	
	public function getInputFilterSpecification() {
        return array ( 
                'firstname' => array ( 
                        'required' => true, 
                        'filters' => array ( 
                                array ( 
                                        'name' => 'StripTags'
                                )
                        ), 
                        'validators' => array ( 
                                array ( 
                                        'name' => 'StringLength', 
                                        'options' => array ( 
                                                'encoding' => 'UTF-8', 
                                                'min' => 2, 
                                                'max' => 140
                                        )
                                )
                        )
                ), 
                'email' => array ( 
                        'required'   => true,
                        'filters' => array (
                                array (
                                        'name' => 'StripTags'
                                )
                        ),
                        'validators' => array(
                                array(
                                        'name'    => 'EmailAddress',
                                        'options' => array(
                                                'allow'  => HostnameValidator::ALLOW_DNS,
                                                'domain' => true,
                                        ),
                                ),
                        ),
                ),
                'lastname' => array ( 
                        'required' => true, 
                        'filters' => array ( 
                                array ( 
                                        'name' => 'StripTags'
                                )
                        ), 
                        'validators' => array ( 
                                array ( 
                                        'name' => 'StringLength', 
                                        'options' => array ( 
                                                'encoding' => 'UTF-8', 
                                                'min' => 2, 
                                                'max' => 140
                                        )
                                )
                        )
                ), 
                'birthplace' => array ( 
                        'required' => true, 
                        'filters' => array ( 
                                array ( 
                                        'name' => 'StripTags'
                                )
                        ), 
                        'validators' => array ( 
                                array ( 
                                        'name' => 'StringLength', 
                                        'options' => array ( 
                                                'encoding' => 'UTF-8', 
                                                'min' => 2, 
                                                'max' => 140
                                        )
                                )
                        )
                ), 
                'birthnation' => array ( 
                        'required' => true, 
                        'filters' => array ( 
                                array ( 
                                        'name' => 'StripTags'
                                )
                        ), 
                        'validators' => array ( 
                                array ( 
                                        'name' => 'StringLength', 
                                        'options' => array ( 
                                                'encoding' => 'UTF-8', 
                                                'min' => 2, 
                                                'max' => 140
                                        )
                                )
                        )
                ), 
                'nationality' => array ( 
                        'required' => true, 
                        'filters' => array ( 
                                array ( 
                                        'name' => 'StripTags'
                                )
                        ), 
                        'validators' => array ( 
                                array ( 
                                        'name' => 'StringLength', 
                                        'options' => array ( 
                                                'encoding' => 'UTF-8', 
                                                'min' => 2, 
                                                'max' => 140
                                        )
                                )
                        )
                ), 
        		
                'pac' => array ( 
                        'required' => true, 
                        'filters' => array ( 
                                array ( 
                                        'name' => 'StripTags', 
                                        'name' => 'StringToLower'
                                )
                        ), 
                        'validators' => array ( 
                                array ( 
                                        'name' => 'StringLength', 
                                        'options' => array ( 
                                                'min' => '3', 
                                                'max' => '25'
                                        )
                                ), 
                                array ( 
                                        'name' => 'Db\NoRecordExists', 
                                        'options' => array ( 
                                                'table' => 'personal_data', 
                                                'field' => 'pac', 
                                                'message' => _('This P.A.C has been already taken.'), 
                                                'exclude' => array ( 
                                                        'field' => 'id', 'value' => $this->getValue('id')
                                                ), 
                                                'adapter' => $this->getDbAdapter()
                                        )
                                ),
                                array (
                                        'name' => 'Regex',
                                        'options' => array (
                                                'pattern' => '/^[a-z0-9-]{3,25}$/',
                                                "messages" => array (
                                                        "regexNotMatch" => _("P.A.C code is not correct! Please create a valid nickname: no spaces, lowercase, no symbols, but numbers and/or max 25 characters."),
                                                        "regexErrorous" => _("Internal error")
                                                )
                                        )
                                ),
                        )
                )
        )
		
		
		;
	}
	
	public function init() {
		$hydrator = new ClassMethods ( true );
		$hydrator->addStrategy ( 'birthdate', new DateTimeStrategy () );
		
		$this->setAttribute ( 'method', 'post' );
		#$this->setPreferFormInputFilter(true); // This will tell your program to prefer your inputfilter to the default one.
		$this->setHydrator ( $hydrator )->setObject ( new Pd () );
		
		$this->add ( array ('name' => 'firstname', 'attributes' => array ('type' => 'text', 'class' => 'form-control' ), 'options' => array ('label' => _ ( 'First Name' ) ), 'filters' => array (array ('name' => 'StringTrim' ) ) ) );
		
		$this->add ( array ('name' => 'lastname', 'attributes' => array ('type' => 'text', 'class' => 'form-control' ), 'options' => array ('label' => _ ( 'Last Name' ) ), 'filters' => array (array ('name' => 'StringTrim' ) ) ) );
		
		$this->add ( array ('type' => 'Zend\Form\Element\Date', 'name' => 'birthdate', 'attributes' => array ('type' => 'text', 'class' => 'form-control' ), 'options' => array ('label' => _ ( 'Birth date' ), 'format' => 'd/m/Y' ) ) );
		
		$this->add ( array ('name' => 'email', 'attributes' => array ('type' => 'text', 'class' => 'form-control' ), 'options' => array ('label' => 'E-Mail' ), 'filters' => array (array ('name' => 'StringTrim' ) ) ) );
		
		$this->add ( array ('type' => 'Zend\Form\Element\Select', 'name' => 'gender', 'attributes' => array ('class' => 'form-control' ), 'options' => array ('label' => _ ( 'Gender' ), 'value_options' => array ('M' => _ ( 'Male' ), 'F' => _ ( 'Female' ) ) ) ) );
		
		$this->add ( array ('type' => 'Cv\Form\Element\Translations', 'name' => 'translation_id', 'attributes' => array ('class' => 'form-control' ), 'options' => array ('label' => _ ( 'Base version' ) ) ) );
		
		$this->add ( array ('name' => 'birthplace', 'attributes' => array ('type' => 'text', 'class' => 'form-control' ), 'options' => array ('label' => _ ( 'Birth place' ) ), 'filters' => array (array ('name' => 'StringTrim' ) ) ) );
		
		$this->add ( array ('name' => 'birthnation', 'attributes' => array ('type' => 'text', 'class' => 'form-control' ), 'options' => array ('label' => _ ( 'Country of Origin' ) ), 'filters' => array (array ('name' => 'StringTrim' ) ) ) );
		
		$this->add ( array ('name' => 'nationality', 'attributes' => array ('class' => 'form-control' ), 'options' => array ('label' => _ ( 'Nationality' ) ) ) );
		
		$this->add ( array ('type' => 'Cv\Form\Element\Country', 'name' => 'country_id', 'attributes' => array ('class' => 'form-control' ), 'options' => array ('label' => _ ( 'Country' ) ) ) );
		
		$this->add(array (
				'type' => 'Cv\Form\Element\Langs',
				'name' => 'mothertongue_id',
				'attributes' => array (
						'class' => 'form-control'
				),
				'options' => array (
						'label' => _('Mothertongue')
				)
		));
		
		$this->add ( array ('name' => 'photo', 'attributes' => array ('type' => 'file' ), 'options' => array ('label' => _ ( 'Photo Upload' ) ) ) );
		
		$this->add ( array ('name' => 'pac', 'attributes' => array ('type' => 'text', 'class' => 'form-control', 'placeholder' => 'P.A.C (Public Access Code)', 'title' => 'Create your access code to use and promote your curriculum' ), 'options' => array ('label' => _ ( 'P.A.C (Public Access Code)' ) ), 'filters' => array (array ('name' => 'StringTrim' ) ) ) );
		
		$this->add ( array ('type' => 'Zend\Form\Element\Select', 'name' => 'public', 'attributes' => array ('class' => 'form-control' ), 'options' => array ('label' => _ ( 'Public' ), 'value_options' => array ('1' => _ ( 'Yes, I want share my curriculum vitae' ), '0' => _ ( 'No, I prefer to hide my curriculum vitae' ) ) ) ) );
		
		$this->add ( array ('type' => 'textarea', 'name' => 'keywords', 'attributes' => array ('class' => 'form-control' ), 'options' => array ('label' => _ ( 'Keywords' ) ) ) );
		
		$this->add ( array ('type' => 'Cv\Form\Element\JobCategories', 'name' => 'jobcategory_id', 'attributes' => array ('class' => 'form-control' ), 'options' => array ('label' => _ ( 'Job Categories' ) ) ) );
		
		// This is a fieldset
		$this->add ( array ('name' => 'addresses', 'type' => '\Cv\Form\Fieldset\AddressFieldset', 'object' => '\Cv\Model\Addresses', 'options' => array ('use_as_base_fieldset' => false ) ) );
		
		// This is a fieldset
		$this->add ( array ('name' => 'contacts', 'type' => '\Cv\Form\Fieldset\ContactFieldset', 'object' => '\Cv\Model\Contacts', 'options' => array ('use_as_base_fieldset' => false ) ) );
		
		$this->add ( array ('name' => 'ganalytics', 'attributes' => array ('type' => 'text', 'class' => 'form-control' ), 'options' => array ('label' => _ ( 'Google Analytics Code' ) ), 'filters' => array (array ('name' => 'StringTrim' ) ) ) );
		$this->add ( array ('name' => 'id', 'attributes' => array ('type' => 'hidden' ) ) );
	}
}