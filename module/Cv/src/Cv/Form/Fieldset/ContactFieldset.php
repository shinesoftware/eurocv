<?php
namespace Cv\Form\Fieldset;
use Cv\Model\Contacts;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class ContactFieldset extends Fieldset implements InputFilterProviderInterface
{

    public function getInputFilterSpecification ()
    {
        return array ( 
        );
    }

    public function init ()
    {
       $hydrator = new ClassMethods(true);
        
        $this->setHydrator($hydrator)->setObject(new Contacts());
        
        
        $this->add(array ( 
                'name' => 'contact', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Contact')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'type' => 'Cv\Form\Element\ContactTypes', 
                'name' => 'type_id', 
                'attributes' => array ( 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Contact Type')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array ( 
                'type' => 'checkbox',
                'name' => 'visible', 
                'attributes' => array ( 
                        'class' => 'form-control',
                ), 
                'options' => array ( 
                        'label' => _('Visible')
                ), 
                'filters' => array ( 
                        array ( 
                                'name' => 'StringTrim'
                        )
                )
        ));

        $this->add(array ( 
                'name' => 'id', 
                'attributes' => array ( 
                        'type' => 'hidden'
                )
        ));
    }
}