<?php
namespace Cv\Model;

class Cs
{

    public $id;
    public $publications;
    public $personal;
    public $social;
    public $organisational;
    public $technical;    
    public $computer;    
    public $artistic;    
    public $other;    
    public $driving;    
    public $jobrelated;    
    public $personaldata_id;  
    public $translation_id;
    public $visible;   
    public $updatedat;   
    public $createdat;   

	/**
     * This method get the array posted and assign the values to the table object
     * @param array $data
     */
    public function exchangeArray ($data)
    {
        foreach ($data as $field => $value){
            $this->$field = (isset($value)) ? $value : null;
        }
        
        return true;
    }
    
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $publications
     */
    public function getPublications ()
    {
        return $this->publications;
    }

	/**
     * @return the $personal
     */
    public function getPersonal ()
    {
        return $this->personal;
    }

	/**
     * @return the $social
     */
    public function getSocial ()
    {
        return $this->social;
    }

	/**
     * @return the $organisational
     */
    public function getOrganisational ()
    {
        return $this->organisational;
    }

	/**
     * @return the $technical
     */
    public function getTechnical ()
    {
        return $this->technical;
    }

	/**
     * @return the $computer
     */
    public function getComputer ()
    {
        return $this->computer;
    }

	/**
     * @return the $artistic
     */
    public function getArtistic ()
    {
        return $this->artistic;
    }

	/**
     * @return the $other
     */
    public function getOther ()
    {
        return $this->other;
    }

	/**
     * @return the $driving
     */
    public function getDriving ()
    {
        return $this->driving;
    }

	/**
     * @return the $jobrelated
     */
    public function getJobrelated ()
    {
        return $this->jobrelated;
    }

	/**
     * @return the $personaldata_id
     */
    public function getPersonaldataId ()
    {
        return $this->personaldata_id;
    }

	/**
     * @return the $translation_id
     */
    public function getTranslationId ()
    {
        return $this->translation_id;
    }

	/**
     * @return the $visible
     */
    public function getVisible ()
    {
        return $this->visible;
    }

	/**
     * @return the $updatedat
     */
    public function getUpdatedat ()
    {
        return $this->updatedat;
    }

	/**
     * @return the $createdat
     */
    public function getCreatedat ()
    {
        return $this->createdat;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $publications
     */
    public function setPublications ($publications)
    {
        $this->publications = $publications;
    }

	/**
     * @param field_type $personal
     */
    public function setPersonal ($personal)
    {
        $this->personal = $personal;
    }

	/**
     * @param field_type $social
     */
    public function setSocial ($social)
    {
        $this->social = $social;
    }

	/**
     * @param field_type $organisational
     */
    public function setOrganisational ($organisational)
    {
        $this->organisational = $organisational;
    }

	/**
     * @param field_type $technical
     */
    public function setTechnical ($technical)
    {
        $this->technical = $technical;
    }

	/**
     * @param field_type $computer
     */
    public function setComputer ($computer)
    {
        $this->computer = $computer;
    }

	/**
     * @param field_type $artistic
     */
    public function setArtistic ($artistic)
    {
        $this->artistic = $artistic;
    }

	/**
     * @param field_type $other
     */
    public function setOther ($other)
    {
        $this->other = $other;
    }

	/**
     * @param field_type $driving
     */
    public function setDriving ($driving)
    {
        $this->driving = $driving;
    }

	/**
     * @param field_type $jobrelated
     */
    public function setJobrelated ($jobrelated)
    {
        $this->jobrelated = $jobrelated;
    }

	/**
     * @param field_type $personaldata_id
     */
    public function setPersonaldataId ($personaldata_id)
    {
        $this->personaldata_id = $personaldata_id;
    }

	/**
     * @param field_type $translation_id
     */
    public function setTranslationId ($translation_id)
    {
        $this->translation_id = $translation_id;
    }

	/**
     * @param field_type $visible
     */
    public function setVisible ($visible)
    {
        $this->visible = $visible;
    }

	/**
     * @param field_type $updatedat
     */
    public function setUpdatedat ($updatedat)
    {
        $this->updatedat = $updatedat;
    }

	/**
     * @param field_type $createdat
     */
    public function setCreatedat ($createdat)
    {
        $this->createdat = $createdat;
    }

}