<?php
namespace Cv\Model;

class Cl
{

    public $id;
    public $coverletter;
    public $personaldata_id;  
    public $translation_id;
    public $visible;   
    public $updatedat;   
    public $createdat;   

	/**
     * This method get the array posted and assign the values to the table object
     * @param array $data
     */
    public function exchangeArray ($data)
    {
        foreach ($data as $field => $value){
            $this->$field = (isset($value)) ? $value : null;
        }
        
        return true;
    }
    
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $coverletter
     */
    public function getCoverletter ()
    {
        return $this->coverletter;
    }

	/**
     * @return the $personaldata_id
     */
    public function getPersonaldataId ()
    {
        return $this->personaldata_id;
    }

	/**
     * @return the $translation_id
     */
    public function getTranslationId ()
    {
        return $this->translation_id;
    }

	/**
     * @return the $visible
     */
    public function getVisible ()
    {
        return $this->visible;
    }

	/**
     * @return the $updatedat
     */
    public function getUpdatedat ()
    {
        return $this->updatedat;
    }

	/**
     * @return the $createdat
     */
    public function getCreatedat ()
    {
        return $this->createdat;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $coverletter
     */
    public function setCoverletter ($coverletter)
    {
        $this->coverletter = $coverletter;
    }

	/**
     * @param field_type $personaldata_id
     */
    public function setPersonaldataId ($personaldata_id)
    {
        $this->personaldata_id = $personaldata_id;
    }

	/**
     * @param field_type $translationId
     */
    public function setTranslationId ($translation_id)
    {
        $this->translation_id = $translation_id;
    }

	/**
     * @param field_type $visible
     */
    public function setVisible ($visible)
    {
        $this->visible = $visible;
    }

	/**
     * @param field_type $updatedat
     */
    public function setUpdatedat ($updatedat)
    {
        $this->updatedat = $updatedat;
    }

	/**
     * @param field_type $createdat
     */
    public function setCreatedat ($createdat)
    {
        $this->createdat = $createdat;
    }

}