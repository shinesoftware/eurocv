<?php
namespace Cv\Form;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;
use \Cv\Hydrator\Strategy\DateTimeStrategy;

class OlForm extends Form
{

    public function init ()
    {
        $hydrator = new ClassMethods(true);
        
        $this->setAttribute('method', 'post');
        $this->setHydrator($hydrator)->setObject(new \Cv\Model\Ol());

        $this->add(array (
        		'type' => 'Cv\Form\Element\Langs',
        		'name' => 'lang_id',
        		'attributes' => array (
        				'class' => 'form-control'
        		),
        		'options' => array (
        				'label' => _('Language')
        		)
        ));

        $this->add(array (
                'name' => 'certificate',
                'attributes' => array (
                        'type' => 'text',
                        'class' => 'form-control'
                ),
                'options' => array (
                        'label' => _('Diploma(s) or certificate(s)')
                ),
                'filters' => array (
                        array (
                                'name' => 'StringTrim'
                        )
                )
        ));
        
        $this->add(array (
                'type' => 'Zend\Form\Element\Select',
                'name' => 'understanding_listening',
                'attributes' => array (
                        'class' => 'form-control'
                ),
                'options' => array (
                        'label' => _('Understanding Listening'),
                        'value_options' => array (
                                'A1' => _('Basic user (A1)'),
                                'A2' => _('Basic user (A2)'),
                                'B1' => _('Independent user (B1)'),
                                'B2' => _('Independent user (B2)'),
                                'C1' => _('Proficient user (C1)'),
                                'C2' => _('Proficient user (C2)'),
                        )
                )
        ));
        
        $this->add(array (
                'type' => 'Zend\Form\Element\Select',
                'name' => 'understanding_reading',
                'attributes' => array (
                        'class' => 'form-control'
                ),
                'options' => array (
                        'label' => _('Understanding Reading'),
                        'value_options' => array (
                                'A1' => _('Basic user (A1)'),
                                'A2' => _('Basic user (A2)'),
                                'B1' => _('Independent user (B1)'),
                                'B2' => _('Independent user (B2)'),
                                'C1' => _('Proficient user (C1)'),
                                'C2' => _('Proficient user (C2)'),
                        )
                )
        ));
        
        $this->add(array (
                'type' => 'Zend\Form\Element\Select',
                'name' => 'spoken_interaction',
                'attributes' => array (
                        'class' => 'form-control'
                ),
                'options' => array (
                        'label' => _('Spoken Interaction'),
                        'value_options' => array (
                                'A1' => _('Basic user (A1)'),
                                'A2' => _('Basic user (A2)'),
                                'B1' => _('Independent user (B1)'),
                                'B2' => _('Independent user (B2)'),
                                'C1' => _('Proficient user (C1)'),
                                'C2' => _('Proficient user (C2)'),
                        )
                )
        ));
        
        $this->add(array (
                'type' => 'Zend\Form\Element\Select',
                'name' => 'spoken_production',
                'attributes' => array (
                        'class' => 'form-control'
                ),
                'options' => array (
                        'label' => _('Spoken Production'),
                        'value_options' => array (
                                'A1' => _('Basic user (A1)'),
                                'A2' => _('Basic user (A2)'),
                                'B1' => _('Independent user (B1)'),
                                'B2' => _('Independent user (B2)'),
                                'C1' => _('Proficient user (C1)'),
                                'C2' => _('Proficient user (C2)'),
                        )
                )
        ));
        
        $this->add(array (
                'type' => 'Zend\Form\Element\Select',
                'name' => 'writing',
                'attributes' => array (
                        'class' => 'form-control'
                ),
                'options' => array (
                        'label' => _('Writing'),
                        'value_options' => array (
                                'A1' => _('Basic user (A1)'),
                                'A2' => _('Basic user (A2)'),
                                'B1' => _('Independent user (B1)'),
                                'B2' => _('Independent user (B2)'),
                                'C1' => _('Proficient user (C1)'),
                                'C2' => _('Proficient user (C2)'),
                        )
                )
        ));


        
        $this->add(array ( 
                'type' => 'Zend\Form\Element\Select', 
                'name' => 'visible', 
                'attributes' => array ( 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Visible'), 
                        'value_options' => array ( 
                        		'1' => _('Visible'),
                        		'0' => _('Not Visible'), 
                                
                        )
                )
        ));
        
        $this->add(array ( 
                'name' => 'sort', 
                'attributes' => array ( 
                        'type' => 'text', 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Sort')
                )
        ));
        
        $this->add(array ( 
                'name' => 'submit', 
                'attributes' => array ( 
                        'type' => 'submit', 
                        'class' => 'btn btn-success', 
                        'value' => _('Save')
                )
        ));
        
        $this->add(array ( 
                'name' => 'id', 
                'attributes' => array ( 
                        'type' => 'hidden'
                )
        ));
    }
}