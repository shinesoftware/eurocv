<?php 
namespace Cv\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

class Toolbar extends AbstractHelper implements ServiceLocatorAwareInterface  
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    
    public function __invoke($getTotal=false)
    {
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();
        $auth = $serviceLocator->get('zfcuser_auth_service');
        
        $session = new Container('base');
        $versionId = $session->offsetGet('versionid') ? $session->offsetGet('versionid') : 2;
        
        if ($auth->hasIdentity()) {
            $userId = $auth->getIdentity()->getId();
            
            $pdTable = $serviceLocator->get('PdTable');
            $user = $pdTable->getPdByUserId($userId);
            if($user){
	            // Get the Work Experience summary items
	            $weTable = $serviceLocator->get('WeTable');
	            $weSummary = $weTable->getSummary($user->getId(), $versionId, $getTotal);
	            
	            // Get the Education & Training summary items
	            $etTable = $serviceLocator->get('EtTable');
	            $etSummary = $etTable->getSummary($user->getId(), $versionId, $getTotal);
	            
	            // Get the Languages summary items
	            $olTable = $serviceLocator->get('OlTable');
	            $olSummary = $olTable->getSummary($user->getId(), $versionId, $getTotal);
	            
	            // Get the Competences summary items
	            $csTable = $serviceLocator->get('CsTable');
	            $csSummary = $csTable->getSummary($user->getId(), $versionId, $getTotal);
	            
	            // Get the Cover letter summary items
	            $clTable = $serviceLocator->get('ClTable');
	            $clSummary = $csTable->getSummary($user->getId(), $versionId, $getTotal);
	            
	            return $this->view->render('cv/partial/toolbar', array('we' => $weSummary, 'et' => $etSummary, 'ol' => $olSummary, 'cs' => $csSummary, 'cl' => $clSummary));
	        }
	    }
        return null;
    }
}