<?php

    // This file is not executed from the Apache 
    // It is a dummy file to handle the translations to do 
    
    $terms[] = $this->translate('Personal dissatisfaction');
    $terms[] = $this->translate('Personal reasons');
    $terms[] = $this->translate('New job found');
    $terms[] = $this->translate('Retirement');
    $terms[] = $this->translate('Fired');
    $terms[] = $this->translate('Additional E-mail');
    $terms[] = $this->translate('Telephone');
    $terms[] = $this->translate('Fax');
    $terms[] = $this->translate('Mobile');
    $terms[] = $this->translate('Skype');
    
    $terms[] = $this->translate('Yes');
    $terms[] = $this->translate('No');
    $terms[] = $this->translate('Download');
    $terms[] = $this->translate('Actions');

    // Job Categories in the database
    $terms[] = $this->translate('Accounting/Auditing');
    $terms[] = $this->translate('Administrative and Support Services');
    $terms[] = $this->translate('Purchasing');
    $terms[] = $this->translate('Aerospace/Aviation/Defense');
    $terms[] = $this->translate('Legal');
    $terms[] = $this->translate('Advertising/Marketing/Public Relations');
    $terms[] = $this->translate('Employment Placement Agencies');
    $terms[] = $this->translate('Agriculture, Environment, Forestry/Fishing');
    $terms[] = $this->translate('Arts/Music/Entertainment/Media/Fashion');
    $terms[] = $this->translate('Architectural Services');
    $terms[] = $this->translate('Insurance');
    $terms[] = $this->translate('Automotive/Motor Vehicle/Parts');
    $terms[] = $this->translate('Banking');
    $terms[] = $this->translate('Biotechnology and Pharmaceutical');
    $terms[] = $this->translate('Sales - Account Management');
    $terms[] = $this->translate('Sales - Telemarketing');
    $terms[] = $this->translate('Sales - Work at Home');
    $terms[] = $this->translate('Sales');
    $terms[] = $this->translate('Retail/Wholesale');
    $terms[] = $this->translate('Consulting Services');
    $terms[] = $this->translate('Customer Service and Call Center');
    $terms[] = $this->translate('Construction');
    $terms[] = $this->translate('Electronics');
    $terms[] = $this->translate('Journalist/Reporters');
    $terms[] = $this->translate('Military');
    $terms[] = $this->translate('Executive Management');
    $terms[] = $this->translate('Career Fairs');
    $terms[] = $this->translate('Finance/Economics');
    $terms[] = $this->translate('Healthcare - Other');
    $terms[] = $this->translate('Healthcare - LPNs & LVNs');
    $terms[] = $this->translate('Healthcare - Pharmacy');
    $terms[] = $this->translate('Healthcare - Therapy/Rehab Services');
    $terms[] = $this->translate('Business and Finance');
    $terms[] = $this->translate('Healthcare - Medical & Dental Practitioners');
    $terms[] = $this->translate('Healthcare - Optical');
    $terms[] = $this->translate('Healthcare - Radiology/Imaging');
    $terms[] = $this->translate('Healthcare - Laboratory/Pathology Services');
    $terms[] = $this->translate('Energy/Utilities');
    $terms[] = $this->translate('Information Technology');
    $terms[] = $this->translate('Engineering');
    $terms[] = $this->translate('Installation, Maintenance, and Repair');
    $terms[] = $this->translate('Internet - Graphics/E-Commerce');
    $terms[] = $this->translate('Education and Training');
    $terms[] = $this->translate('Computers, Hardware');
    $terms[] = $this->translate('IT - Helpdesk');
    $terms[] = $this->translate('Computers, Software');
    $terms[] = $this->translate('Airlines');
    $terms[] = $this->translate('Transportation and Warehousing');
    $terms[] = $this->translate('Real Estate/Mortgage');
    $terms[] = $this->translate('Nonprofit');
    $terms[] = $this->translate('Other');
    $terms[] = $this->translate('Construction, Mining and Trades');
    $terms[] = $this->translate('Manufacturing and Production');
    $terms[] = $this->translate('Consumer Products');
    $terms[] = $this->translate('Product Management');
    $terms[] = $this->translate('Public Administration and Policy');
    $terms[] = $this->translate('Research & Development');
    $terms[] = $this->translate('Human Resources/Recruiting');
    $terms[] = $this->translate('Restaurant and Food Service');
    $terms[] = $this->translate('Personal Care and Service Industry');
    $terms[] = $this->translate('Environmental Services');
    $terms[] = $this->translate('Financial Services/Leasing/Credit');
    $terms[] = $this->translate('Security and Law Enforcement');
    $terms[] = $this->translate('Sports and Recreation/Fitness');
    $terms[] = $this->translate('Telecommunications');
    $terms[] = $this->translate('Textiles');
    $terms[] = $this->translate('Tourism/Hotel/Hospitality/Events');
    $terms[] = $this->translate('Veterinary Services');
    $terms[] = $this->translate('Translation/Interpreting/Consulting');
        
    $terms[] = $this->translate('Albanian');
    $terms[] = $this->translate('Bulgarian');
    $terms[] = $this->translate('Croatian');
    $terms[] = $this->translate('Czech');
    $terms[] = $this->translate('Danish');
    $terms[] = $this->translate('Dutch');
    $terms[] = $this->translate('English');
    $terms[] = $this->translate('Estonian');
    $terms[] = $this->translate('Finnish');
    $terms[] = $this->translate('French');
    $terms[] = $this->translate('German');
    $terms[] = $this->translate('Greek');
    $terms[] = $this->translate('Hungarian');
    $terms[] = $this->translate('Irish');
    $terms[] = $this->translate('Italian');
    $terms[] = $this->translate('Latvian');
    $terms[] = $this->translate('Lithuanian');
    $terms[] = $this->translate('Maltese');
    $terms[] = $this->translate('Norwegian');
    $terms[] = $this->translate('Polish');
    $terms[] = $this->translate('Portuguese');
    $terms[] = $this->translate('Romanian');
    $terms[] = $this->translate('Russian');
    $terms[] = $this->translate('Slovak');
    $terms[] = $this->translate('Slovenian');
    $terms[] = $this->translate('Spanish');
    $terms[] = $this->translate('Swedish');
    
    $terms[] = $this->translate("Value is required and can't be empty");
    $terms[] = $this->translate("The input is less than %s characters long");
    $terms[] = $this->translate("Missing captcha fields");
    $terms[] = $this->translate("Captcha ID field is missing");
    $terms[] = $this->translate("Empty captcha value");
    
    $terms[] = $this->translate("Invalid type given. String expected");
    $terms[] = $this->translate("The input is not a valid email address. Use the basic format local-part@hostname");
    $terms[] = $this->translate("'%hostname%' is not a valid hostname for the email address");
    $terms[] = $this->translate("'%hostname%' does not appear to have any valid MX or A records for the email address");
    $terms[] = $this->translate("'%hostname%' is not in a routable network segment. The email address should not be resolved from public network");
    $terms[] = $this->translate("'%localPart%' can not be matched against dot-atom format");
    $terms[] = $this->translate("'%localPart%' can not be matched against quoted-string format");
    $terms[] = $this->translate("'%localPart%' is not a valid local part for the email address");
    $terms[] = $this->translate("The input exceeds the allowed length");

    
    $terms[] = $this->translate("The input appears to be a DNS hostname but the given punycode notation cannot be decoded");
    $terms[] = $this->translate("Invalid type given. String expected");
    $terms[] = $this->translate("The input appears to be a DNS hostname but contains a dash in an invalid position");
    $terms[] = $this->translate("The input does not match the expected structure for a DNS hostname");
    $terms[] = $this->translate("The input appears to be a DNS hostname but cannot match against hostname schema for TLD '%tld%'");
    $terms[] = $this->translate("The input does not appear to be a valid local network name");
    $terms[] = $this->translate("The input does not appear to be a valid URI hostname");
    $terms[] = $this->translate("The input appears to be an IP address, but IP addresses are not allowed");
    $terms[] = $this->translate("The input appears to be a local network name but local network names are not allowed");
    $terms[] = $this->translate("The input appears to be a DNS hostname but cannot extract TLD part");
    $terms[] = $this->translate("The input appears to be a DNS hostname but cannot match TLD against known list");
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    