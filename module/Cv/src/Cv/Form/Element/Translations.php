<?php
namespace Cv\Form\Element;

use Cv\Model\TranslationTable;
use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\I18n\Translator\Translator;

class Translations extends Select implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $translator;
    protected $translationTable;
    
    public function __construct(TranslationTable $translationTable, \Zend\Mvc\I18n\Translator $translator){
        parent::__construct();
        $this->translationTable = $translationTable;
        $this->translator = $translator;
    }
    
    public function init()
    {
        $data = array();
        
        $translations = $this->translationTable->fetchAll();
        foreach ($translations as $translation){
            $data[$translation->getId()] = $this->translator->translate($translation->getTitle());
        }
        $this->setValueOptions($data);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
