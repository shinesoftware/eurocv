<?php
namespace Cv\Model;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class ContactsTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * @return the $tableGateway
     */
    public function getTableGateway ()
    {
        return $this->tableGateway;
    }
    
    public function saveData (Contacts $contact)
    {
        $data = $contact->getArrayCopy();
        $id = (int) $contact->getId();
        
        if (empty($id)) {
        	unset($data['id']);
            $data['createdat'] = date('Y-m-d H:i:s');
            $data['updatedat'] = date('Y-m-d H:i:s');
            $this->tableGateway->insert($data); // add the record
            $id = $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->getContact($id, $data['personaldata_id'])) {
                $data['updatedat'] = date('Y-m-d H:i:s');
                unset( $data['createdat']);
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
            } else {
                throw new \Exception('Contact ID does not exist');
            }
        }
        $contact = $this->getContact($id);
        return $contact;
    }

    /**
     * Get all the records
     * 
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Get Contact account by ContactId
     * 
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getContact ($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get Contact by personaldata_id
     *
     * @param integer $personaldata_id
     * @throws \Exception
     * @return Row
     */
    public function getContactsByPdId ($personaldata_id)
    {
    
        $rowset = $this->tableGateway->select(array('personaldata_id' => $personaldata_id));
        $row = $rowset->current();
        if (! $row) {
            return false;
        }
        return $row;
    }
    
    /**
     * Get Last Contact account
     * 
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getLast ()
    {
        $rowset = $this->tableGateway->select(function (Select $select) {
		     $select->sort('id DESC')->limit(1);
		});
        
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find the record");
        }
        return $row;
    }

    /**
     * Delete Contact account by ContactId
     * 
     * @param string $id            
     */
    public function deleteContact ($id, $personaldata_id)
    {
        $this->tableGateway->delete(array(
                'id' => $id,
                'personaldata_id' => $personaldata_id
        ));
    }
}
