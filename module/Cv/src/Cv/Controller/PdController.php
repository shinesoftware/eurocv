<?php
namespace Cv\Controller;
use Cv\Model\Addresses;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Cv\Model\Pd;
use Zend\Stdlib\Hydrator\ClassMethods;
use Cv\Hydrator\Strategy\DateTimeStrategy;
/**
 * Personal Data Controller
 * This controller gets the personal data of the user
 */
class PdController extends AbstractActionController
{

    protected $userId;

    protected $personaldataId;

    /**
     * OnDispatch of the controller
     * (non-PHPdoc)
     *
     * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
     */
    public function onDispatch (\Zend\Mvc\MvcEvent $e)
    {
        // check the credencials
        if (! $this->zfcUserAuthentication()->hasIdentity()) {
            return $this->redirect()->toRoute('www/user');
        }
        
        $translation = $this->getServiceLocator()->get('TranslationTable');
        $translation_id = 2; // English
                             
        // Get the identity information
        $Identity = $this->zfcUserAuthentication()->getIdentity();
        $this->userId = $Identity->getId();
        
        // Get the TableGateway object to retrieve the data
        $user = $this->getServiceLocator()->get('PdTable');
        
        // Get the user
        $myUser = $user->getPdByUserId($this->userId);
        
        if (! empty($myUser)) {
            $this->personaldataId = $myUser->getId();
            
            // Check the user and get the default language
            $translation_id = $myUser->getTranslationId();
        }
        
        return parent::onDispatch($e);
    }

    /**
     * Get the default upload location path
     */
    public function getFileUploadLocation ()
    {
        // Fetch Configuration from Module Config
        $config = $this->getServiceLocator()->get('config');
        return $config['module_config']['photo_location'];
    }

    /**
     * See all the user data information
     * (non-PHPdoc)
     *
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     */
    public function indexAction ()
    {
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\PdForm');
        
        // Get the TableGateway object to retrieve the data
        $user = $this->getServiceLocator()->get('PdMapper');
        
        // Get the record by its id
        $myUser = $user->findUser('user_id', $this->userId);
        
        $addresses = ! empty($myUser) ? $myUser->getAddresses() : null;
        $contacts = ! empty($myUser) ? $myUser->getContacts() : null;
        
        // Bind the data in the form
        if (! empty($myUser)) {
            $form->bind($myUser);
        }
        
        $viewModel = new ViewModel(array ( 
                'form' => $form, 
                'user' => $myUser, 
                'addresslist' => $addresses, 
                'contactlist' => $contacts
        ));
        
        return $viewModel;
    }

    /**
     * Delete the user information
     */
    public function deleteAction ()
    {
        
        // Get the TableGateway object to retrieve the data
        $pd = $this->getServiceLocator()->get('PdTable');
        
        // Delete the user
        if ($pd->deletePd($this->personaldataId)) {
            $this->flashMessenger()->setNamespace('success')->addMessage('The user information have been deleted.');
        }
        
        return $this->redirect()->toRoute('zfcuser/logout');
    }

    /**
     * Delete the address information
     */
    public function delAddressAction ()
    {
        $addressId = $this->params()->fromRoute('id');
        
        // Get the TableGateway object to retrieve the data
        $address = $this->getServiceLocator()->get('AddressesTable');
        
        // Delete the address of the user
        if ($address->deleteAddress($addressId, $this->personaldataId)) {
            $this->flashMessenger()->setNamespace('success')->addMessage('The address information has been deleted.');
        }
        
        return $this->redirect()->toRoute('cv/pd');
    }

    /**
     * Delete the contact information
     */
    public function delContactAction ()
    {
        $contactId = $this->params()->fromRoute('id');
        
        // Get the TableGateway object to retrieve the data
        $contact = $this->getServiceLocator()->get('ContactsTable');
        
        // Delete the address of the user
        if ($contact->deleteContact($contactId, $this->personaldataId)) {
            $this->flashMessenger()->setNamespace('success')->addMessage('The contact information has been deleted.');
        }
        
        return $this->redirect()->toRoute('cv/pd');
    }

    /**
     * Delete the contact information
     */
    public function deletePhotoAction ()
    {
        // Get the TableGateway object to retrieve the data
        $user = $this->getServiceLocator()->get('PdTable');
        
        // Delete the address of the user
        if ($user->deletePhoto($this->personaldataId)) {
            $this->flashMessenger()->setNamespace('success')->addMessage('The contact information has been deleted.');
        }
        
        return $this->redirect()->toRoute('cv/pd');
    }

    /**
     * Edit the address information
     */
    public function editAddressAction ()
    {
        $addressId = $this->params()->fromRoute('id');
        
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\PdForm');
        
        // Get the TableGateway object to retrieve the data
        $user = $this->getServiceLocator()->get('PdMapper');
        
        // Get the TableGateway object to retrieve the data
        $address = $this->getServiceLocator()->get('AddressesTable');
        
        // Get the record by its id
        $myUser = $user->findUser('user_id', $this->userId);
        $addresses = ! empty($myUser) ? $myUser->getAddresses() : null;
        $contacts = ! empty($myUser) ? $myUser->getContacts() : null;
        
        // Get the user address
        $userAddress = $address->getAddress($addressId);
        
        $viewModel = new ViewModel(array ( 
                'form' => $form, 
                'user' => $myUser, 
                'addresslist' => $addresses, 
                'contactlist' => $contacts
        ));
        
        // overwrite the Address field to match and show them in the form
        // elements
        $myUser->setAddresses($userAddress);
        
        // Bind the data in the form
        if (! empty($myUser)) {
            $form->bind($myUser);
        }
        
        $viewModel->setTemplate('cv/pd/index');
        return $viewModel;
    }

    /**
     * Edit the contact information
     */
    public function editContactAction ()
    {
        $contactId = $this->params()->fromRoute('id');
        
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\PdForm');
        
        // Get the TableGateway object to retrieve the data
        $user = $this->getServiceLocator()->get('PdMapper');
        
        // Get the TableGateway object to retrieve the data
        $address = $this->getServiceLocator()->get('ContactsTable');
        
        // Get the record by its id
        $myUser = $user->findUser('user_id', $this->userId);
        $addresses = ! empty($myUser) ? $myUser->getAddresses() : null;
        $contacts = ! empty($myUser) ? $myUser->getContacts() : null;
        
        // Get the user address
        $userContact = $address->getContact($contactId);
        
        $viewModel = new ViewModel(array ( 
                'form' => $form, 
                'user' => $myUser, 
                'addresslist' => $addresses, 
                'contactlist' => $contacts
        ));
        
        // overwrite the Address field to match and show them in the form
        // elements
        $myUser->setContacts($userContact);
        
        // Bind the data in the form
        if (! empty($myUser)) {
            $form->bind($myUser);
        }
        
        $viewModel->setTemplate('cv/pd/index');
        return $viewModel;
    }

    /**
     * Prepare the data and then save them
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function processAction ()
    {
        
        // Get the TableGateway object to retrieve the data
        $user = $this->getServiceLocator()->get('PdMapper');
        
        if (! $this->request->isPost()) {
            return $this->redirect()->toRoute(NULL, array ( 
                    'controller' => 'pd', 
                    'action' => 'index'
            ));
        }
        
        $post = $this->request->getPost();
        
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\PdForm');
        $form->setData($post);
        
        // Check if the personal data ID has been assigned and if he is in registration mode 
        // we disable the "exclude" option http://framework.zend.com/manual/2.0/en/modules/zend.validator.db.html 
        if (empty($this->personaldataId)) { // REGISTRATION MODE
            $validators = $form->getInputFilter()->get('pd')->get('pac')->getValidatorChain()->getValidators();
            foreach ($validators as $validator){
                if($validator['instance'] instanceof \Zend\Validator\Db\NoRecordExists){
                    $validator['instance']->setExclude(null);
                }
            }
        }else{ // EDIT MODE
            // If the user change his PAC (username) code check in the whole db if the new PAC code is available 
            $myUser = $user->findUser('user_id', $this->userId);
            if($post['pd']['pac'] != $myUser->getPac()){
                $validators = $form->getInputFilter()->get('pd')->get('pac')->getValidatorChain()->getValidators();
                foreach ($validators as $validator){
                    if($validator['instance'] instanceof \Zend\Validator\Db\NoRecordExists){
                        $validator['instance']->setExclude(null);
                    }
                }
            }
        } 
        
        if (!$form->isValid()) {
            
            // Get the record by its id
            $myUser = $user->findUser('user_id', $this->userId);
            $addresses = ! empty($myUser) ? $myUser->getAddresses() : null;
            $contacts = ! empty($myUser) ? $myUser->getContacts() : null;
            
            $viewModel = new ViewModel(array ( 
                    'error' => true, 
                    'form' => $form, 
                    'user' => $myUser, 
                    'addresslist' => $addresses, 
                    'contactlist' => $contacts
            ));
            $viewModel->setTemplate('cv/pd/index');
            return $viewModel;
        }
        
        // Get the posted vars
        $user = $form->getData();
        $is_image = new \Zend\Validator\File\IsImage();
        $adapter = new \Zend\File\Transfer\Adapter\Http();
        $size = new \Zend\Validator\File\Size(array('max'=> '500kB'));
        $files = $adapter->getFileInfo();
        $pac = strtolower($post->pd['pac']); 
        
        if (! empty($files)) {
            foreach ($files as $key => $file) {
            	
                if ($adapter->isUploaded($key)) {
                	
                	// check if the file is an image and the file size is correct
                	$adapter->setValidators(array($size, $is_image), $key);
                	
                    if ($adapter->isValid($file['name'])) {
                        if ($adapter->receive()) {
                            
                            $tmp = $file['tmp_name']; // get the temp filename path

                            // get the content of the file
                            $data = file_get_contents($adapter->getFileName($key));

                            // extract the extension of the file
                            $ext = pathinfo($adapter->getFileName($key), PATHINFO_EXTENSION);
                            
                            // create the original filename path using the PAC code
                            $path = $this->getFileUploadLocation() . "/" . $pac . "." . strtolower($ext);
                            
                            // write the content of the file in a new file
                            file_put_contents($path, $data);
                        }
                    }else{
                    	$this->flashMessenger()->setNamespace('danger')->addMessage(implode("<br/>", $adapter->getMessages()));
                    }
                }
            }
        }
        
        // Set the userId
        $user->setUserId($this->userId);
        
        $address = $user->getAddresses();
        $contact = $user->getContacts();
        
        $user->setAddresses(null);
        $user->setContacts(null);
        
        // Save the data in the database
        $user = $this->getServiceLocator()->get('PdTable')->saveData($user);
        
        if ($user->getId() && $address->getStreet()) {
            $address->setPersonaldataId($user->getId());
            
            // save the address in the database
            $addresses = $this->getServiceLocator()->get('AddressesTable')->saveData($address);
        }
        
        if ($user->getId() && $contact->getContact()) {
            $contact->setPersonaldataId($user->getId());
            
            // save the address in the database
            $contacts = $this->getServiceLocator()->get('ContactsTable')->saveData($contact);
        }
        
        $this->flashMessenger()->setNamespace('success')->addMessage('The personal data information have been saved.');
        
        return $this->redirect()->toRoute(NULL, array ( 
                'controller' => 'pd', 
                'action' => 'index'
        ));
    }
}