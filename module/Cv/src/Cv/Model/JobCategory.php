<?php
namespace Cv\Model;

class JobCategory
{
    protected $id;
    protected $category;
    
    /**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $category
     */
    public function getCategory ()
    {
        return $this->category;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $category
     */
    public function setCategory ($category)
    {
        $this->category = $category;
    }

	/**
     * 
     * @param array $data
     */
    function exchangeArray ($data)
    {
        foreach ($data as $field => $value){
            $this->$field = (isset($value)) ? $value : null;
        }
    }
    
    /**
     * Get the keys of the array
     * @return multitype:
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
}