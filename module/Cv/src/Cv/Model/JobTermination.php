<?php
namespace Cv\Model;

class JobTermination
{
    protected $id;
    protected $name;
    protected $isvoluntary;
    
	/**
     * 
     * @param array $data
     */
    function exchangeArray ($data)
    {
        foreach ($data as $field => $value){
            $this->$field = (isset($value)) ? $value : null;
        }
    }
    
    /**
     * Get the keys of the array
     * @return multitype:
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
	/**
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

	/**
     * @return the $name
     */
    public function getName ()
    {
        return $this->name;
    }

	/**
     * @return the $isvoluntary
     */
    public function getIsvoluntary ()
    {
        return $this->isvoluntary;
    }

	/**
     * @param field_type $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

	/**
     * @param field_type $name
     */
    public function setName ($name)
    {
        $this->name = $name;
    }

	/**
     * @param field_type $isvoluntary
     */
    public function setIsvoluntary ($isvoluntary)
    {
        $this->isvoluntary = $isvoluntary;
    }

    
}