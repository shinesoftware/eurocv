<?php
namespace Admin\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Column\Style;
use ZfcDatagrid\Column\Formatter;
use ZfcDatagrid\Filter;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

/**
 * Users Controller
 * This controller handle the users
 */
class UsersController extends AbstractActionController
{

    /**
     * OnDispatch of the controller
     * (non-PHPdoc)
     *
     * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
     */
    public function onDispatch (\Zend\Mvc\MvcEvent $e)
    {
        return parent::onDispatch($e);
    }

    private function createGrid ()
    {
    	
    	// Get the TableGateway object to retrieve the data
    	$dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
    	
    	// Check the Work Experiences
    	$sql = new Sql($dbAdapter);
    	$subQry = $sql->select()->columns(array(new \Zend\Db\Sql\Expression('COUNT(*)')));
    	$subQry->from('work_experiences');
    	$subQry->where(array('personaldata_id' => 'pd.id'));
    	$subQry->limit(1);
//     	print_r(get_class($subQry));
//     	echo $subQry->getSqlString();
//     	die;
        $select = new Select();
        $select->columns(array('weitems' => new \Zend\Db\Sql\Expression('?', array($subQry))));
        $select->from(array ('pd' => 'personal_data'))
               ->join(array('c' => 'country'), 'pd.country_id = c.id', array('country'), 'left');
        
//         print_r(get_class($select));
//         echo $select->getSqlString();
//         die;
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($select, $dbAdapter);
        
        // adding the checkboxes
//         $action = new Column\Action\Checkbox();
//         $col = new Column\Action('checkboxes');
//         $col->addAction($action);
//         $grid->addColumn($col);
        
        $colId = new Column\Select('id', 'pd');
        $colId->setLabel('Id');
        $colId->setIdentity();
        $grid->addColumn($colId);
        
        $colFormatter = new \Base\Model\DatagridImage();
        $colFormatter->setPath(PUBLIC_PATH . "/photos/");
        $colFormatter->setAttribute("width", "50");
        $colFormatter->setAttribute("class", "img-responsive img-thumbnail");
        $colFormatter->setPrefix("/photos/");
        $col = new Column\Select('pac', 'pd');
        $col->setFormatter($colFormatter);
        $col->setType(new Type\Image());
        $col->setLabel('Photo');
        $grid->addColumn($col);
        
//         $col = new Column\Select('weitems');
//         $col->setLabel('First name');
//         $grid->addColumn($col);
        
        
        $col = new Column\Select('firstname', 'pd');
        $col->setLabel('First name');
        $grid->addColumn($col);
        
        $col = new Column\Select('lastname', 'pd');
        $col->setLabel('Last name');
        $grid->addColumn($col);
        
        $col = new Column\Select('country', 'c');
        $col->setLabel('Country');
        $grid->addColumn($col);
        
        $col = new Column\Select('email', 'pd');
        $col->setFormatter(new \ZfcDatagrid\Column\Formatter\Email());
        $col->addStyle(new Style\Bold());
        $col->setLabel('E-Mail');
        $grid->addColumn($col);
        
        $col = new Column\Select('pac');
        $col->addStyle(new Style\Bold());
        $col->setLabel('PAC');
        $grid->addColumn($col);
        
        $colType = new Type\DateTime('Y-m-d H:i:s', \IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT);
        $colType->setSourceTimezone('Europe/Rome');
        $colType->setOutputTimezone('UTC');
        $colType->setLocale('it_IT');
        
        $col = new Column\Select('createdat', 'pd');
        $col->setType($colType);
        $col->setLabel('Created at');
        $col->setUserFilterDisabled(true);
        $col->setSortDefault(1, 'DESC');
        $grid->addColumn($col);
        
        $col = new Column\Select('updatedat', 'pd');
        $col->setLabel('Updated at');
        $col->setType($colType);
        $col->setUserFilterDisabled(true);
        $grid->addColumn($col);
        
        $col = new Column\Select('public', 'pd');
        $col->setType(new \ZfcDatagrid\Column\Type\String());
        $col->setLabel('Is Public');
        $col->setTranslationEnabled(true);
        $col->setFilterSelectOptions(array ( 
                '' => '-', 
                '0' => 'No', 
                '1' => 'Yes'
        ));
        $col->setReplaceValues(array ( 
                '' => '-', 
                '0' => 'No', 
                '1' => 'Yes'
        ));
        $grid->addColumn($col);
        
        // Add actions to the grid
        $editaction = new Column\Action\Button();
        $editaction->setAttribute('href', "/admin/users/edit/" . $editaction->getRowIdPlaceholder());
        $editaction->setAttribute('class', 'btn btn-xs btn-default');
        $editaction->setLabel('edit');
        
        // Add actions to the grid
        $showaction = new Column\Action\Button();
        $showaction->setAttribute('href', 'http://' . $showaction->getColumnValuePlaceholder(new Column\Select('pac')) . ".eurocv.eu");
        $showaction->setAttribute('target', '_blank');
        $showaction->setAttribute('class', 'btn btn-xs btn-success');
        $showaction->setLabel('show');
        
        $delaction = new Column\Action\Button();
        $delaction->setAttribute('href', 'users/delete/' . $delaction->getRowIdPlaceholder());
        $delaction->setAttribute('onclick', "return confirm('Are you sure?')");
        $delaction->setAttribute('class', 'btn btn-xs btn-danger');
        $delaction->setLabel('delete');
        
        $col = new Column\Action();
        $col->setWidth(8);
        $col->addAction($editaction);
        $col->addAction($showaction);
        $col->addAction($delaction);
        $grid->addColumn($col);
        
        $grid->setToolbarTemplate('');
        
        return $grid;
    }
    
    
    /**
     * Edit the main user information
     */
    public function editAction ()
    {
    	$id = $this->params()->fromRoute('id');
    	
    	$form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\PdForm');
    
    	// Get the TableGateway object to retrieve the data
    	$user = $this->getServiceLocator()->get('PdMapper');
    
    	// Get the record by its id
    	$myUser = $user->findUser('id', $id);
        	
    	$addresses = ! empty($myUser) ? $myUser->getAddresses() : null;
    	$contacts = ! empty($myUser) ? $myUser->getContacts() : null;
    
    	// Bind the data in the form
    	if (! empty($myUser)) {
    		$form->bind($myUser);
    	}
    
    	$viewModel = new ViewModel(array (
    			'form' => $form,
    			'user' => $myUser,
    			'addresslist' => $addresses,
    			'contactlist' => $contacts
    	));
    
    	return $viewModel;
    }
    
    
    /**
     * Prepare the data and then save them
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function processAction ()
    {
    
    	// Get the TableGateway object to retrieve the data
    	$user = $this->getServiceLocator()->get('PdMapper');
    	
    	
    	if (! $this->request->isPost()) {
    		return $this->redirect()->toRoute(NULL, array (
    				'controller' => 'users',
    				'action' => 'index'
    		));
    	}
    
    	$post = $this->request->getPost();
    	$userId = $post['pd']['id'];
    	
    	$form = $this->getServiceLocator()->get('FormElementManager')->get('Cv\Form\PdForm');
    	$form->setData($post);
    
    	// Check if the personal data ID has been assigned and if he is in registration mode
    	// we disable the "exclude" option http://framework.zend.com/manual/2.0/en/modules/zend.validator.db.html
    	// If the user change his PAC (username) code check in the whole db if the new PAC code is available
    	$myUser = $user->findUser('id', $userId);
   		if($post['pd']['pac'] != $myUser->getPac()){
   			$validators = $form->getInputFilter()->get('pd')->get('pac')->getValidatorChain()->getValidators();
   			foreach ($validators as $validator){
   				if($validator['instance'] instanceof \Zend\Validator\Db\NoRecordExists){
   					$validator['instance']->setExclude(null);
   				}
   			}
   		}
   		
   		// Delete the unused fields
   		$addresses = $form->get('pd')->get('addresses');
   		$addresses->remove('visible');
   		$addresses->remove('showmap');
   		$addresses->remove('country_id');
   		
   		$contacts = $form->get('pd')->get('contacts');
   		$contacts->remove('type_id');
   		$contacts->remove('visible');
   		
    	if (!$form->isValid()) {
    
    		// Get the record by its id
    		$myUser = $user->findUser('id', $userId);
    		$viewModel = new ViewModel(array (
    				'error' => true,
    				'form' => $form,
    				'user' => $myUser,
    		));
    		$viewModel->setTemplate('admin/users/edit');
    		return $viewModel;
    	}
    
    	// Get the posted vars
    	$user = $form->getData();
    
    	// Set the userId
    	$user->setUserId($userId);
    
    	$user->setAddresses(null);
    	$user->setContacts(null);
    
    	// Save the data in the database
    	$personaldataTable = $this->getServiceLocator()->get('PdTable')->saveData($user);
    
    	$this->flashMessenger()->setNamespace('success')->addMessage('The personal data information have been saved.');
    
    	return $this->redirect()->toRoute(NULL, array (
    			'controller' => 'users',
    			'action' => 'edit',
    			'id' => $userId
    	));
    }

    /**
     * Create the grid
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction ()
    {
        $grid = $this->createGrid();
        $grid->render();
        
        $response = $grid->getResponse();
        
        if ($grid->isHtmlInitReponse()) {
            $view = new ViewModel();
            $view->addChild($response, 'grid');
            $view->setVariable('title', "User list");
            return $view;
        } else {
            return $response;
        }
    }

    /**
     * Create a simple html grid
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function htmlAction ()
    {
        $grid = $this->createGrid();
        $grid->setRendererName('PrintHtml');
        $grid->render();
        
        return $grid->getResponse();
    }

    /**
     * Delete the user records and files
     *
     * @return \Zend\Http\Response
     */
    public function deleteAction ()
    {
        $id = $this->params()->fromRoute('id');
        
        if (is_numeric($id)) {
            $user = $this->getServiceLocator()->get('PdTable');
            $uploads = $this->getServiceLocator()->get('UploadsTable');
            
            // Delete the uploaded files
            $uploads->deleteUploads($id);
            
            // Delete the user informaiton
            $user->deletePd($id);
            
            // Go back showing a message
            $this->flashMessenger()->setNamespace('success')->addMessage('The curriculum vitae has been deleted!');
            return $this->redirect()->toRoute('zfcadmin/users');
        }
        
        $this->flashMessenger()->setNamespace('danger')->addMessage('The curriculum vitae has been not deleted!');
        return $this->redirect()->toRoute('zfcadmin/users');
    }
}