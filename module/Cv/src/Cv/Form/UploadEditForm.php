<?php
namespace Cv\Form;

use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;
use \Cv\Hydrator\Strategy\DateTimeStrategy;

class UploadEditForm extends Form
{
    public function init ()
    {
        $hydrator = new ClassMethods(true);
        
        $this->setAttribute('method', 'post');
        $this->setHydrator($hydrator);
        
        $this->setAttribute('enctype','multipart/form-data');
       
        $this->add(array ( 
                'name' => 'title', 
                'attributes' => array ( 
                        'type' => 'text',
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Title')
                )
        ));

        $this->add(array ( 
                'type' => 'Cv\Form\Element\WorkExperiences',
                'name' => 'workexperience_id', 
                'attributes' => array ( 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Work Experience')
                )
        ));

        $this->add(array ( 
                'type' => 'Cv\Form\Element\Educations', 
                'name' => 'education_id', 
                'attributes' => array ( 
                        'class' => 'form-control'
                ), 
                'options' => array ( 
                        'label' => _('Education and Training')
                )
        ));
        
        $this->add(array (
                'type' => 'Cv\Form\Element\Languages',
                'name' => 'language_id',
                'attributes' => array (
                        'class' => 'form-control'
                ),
                'options' => array (
                        'label' => _('Languages')
                )
        ));
        
        
        $this->add(array (
                'name' => 'submit',
                'attributes' => array (
                        'type' => 'submit',
                        'class' => 'btn btn-success',
                        'value' => _('Save')
                )
        ));
        
        $this->add(array (
                'name' => 'id',
                'attributes' => array (
                        'type' => 'hidden'
                )
        ));
    }
}
