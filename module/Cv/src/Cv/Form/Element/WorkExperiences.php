<?php
namespace Cv\Form\Element;

use Cv\Model\WeTable;
use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\I18n\Translator\Translator;

class WorkExperiences extends Select implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $translator;
    protected $weTable;
    protected $personaldata_id;
    
    public function __construct(WeTable $weTable, $idPersonaldata, \Zend\Mvc\I18n\Translator $translator){
        parent::__construct();
        $this->weTable = $weTable;
        $this->personaldata_id = $idPersonaldata;
        $this->translator = $translator;
    }
    
    public function init()
    {
        $data = array();
        $this->setEmptyOption($this->translator->translate('Please select ...'));
        $wexp = $this->weTable->fetchAllbyId($this->personaldata_id);
        foreach ($wexp as $we){
            $data[$we->getId()] = $we->getEmployer();
        }
        $this->setValueOptions($data);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
