<?php
namespace Cv\Model;

class Et
{

    public $id;

    public $startdate;

    public $enddate;

    public $organisation;

    public $skills;

    public $title;

    public $level;

    public $personaldata_id;

    public $translation_id;

    public $visible;

    public $sort;

    public $createdat;

    public $updatedat;

    /**
     *
     * @return multitype:
     */
    public function getArrayCopy ()
    {
        return get_object_vars($this);
    }

    /**
     * This method get the array posted and assign the values to the table
     * object
     *
     * @param array $data            
     */
    public function exchangeArray ($data)
    {
        foreach ($data as $field => $value) {
            $this->$field = (isset($value)) ? $value : null;
        }
        
        return true;
    }

    /**
     *
     * @return the $id
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     *
     * @return the $startdate
     */
    public function getStartdate ()
    {
        return $this->startdate;
    }

    /**
     *
     * @return the $enddate
     */
    public function getEnddate ()
    {
        return $this->enddate;
    }

    /**
     *
     * @return the $organisation
     */
    public function getOrganisation ()
    {
        return $this->organisation;
    }

    /**
     *
     * @return the $skills
     */
    public function getSkills ()
    {
        return $this->skills;
    }

    /**
     *
     * @return the $title
     */
    public function getTitle ()
    {
        return $this->title;
    }

    /**
     *
     * @return the $level
     */
    public function getLevel ()
    {
        return $this->level;
    }

    /**
     *
     * @return the $personaldata_id
     */
    public function getPersonaldataId ()
    {
        return $this->personaldata_id;
    }

    /**
     *
     * @return the $translation_id
     */
    public function getTranslationId ()
    {
        return $this->translation_id;
    }

    /**
     *
     * @return the $visible
     */
    public function getVisible ()
    {
        return $this->visible;
    }

    /**
     *
     * @return the $sort
     */
    public function getSort ()
    {
        return $this->sort;
    }

    /**
     *
     * @return the $createdat
     */
    public function getCreatedat ()
    {
        return $this->createdat;
    }

    /**
     *
     * @return the $updatedat
     */
    public function getUpdatedat ()
    {
        return $this->updatedat;
    }

    /**
     *
     * @param field_type $id            
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @param field_type $startdate            
     */
    public function setStartdate ($startdate)
    {
        $this->startdate = $startdate;
    }

    /**
     *
     * @param field_type $enddate            
     */
    public function setEnddate ($enddate)
    {
        $this->enddate = $enddate;
    }

    /**
     *
     * @param field_type $organisation            
     */
    public function setOrganisation ($organisation)
    {
        $this->organisation = $organisation;
    }

    /**
     *
     * @param field_type $skills            
     */
    public function setSkills ($skills)
    {
        $this->skills = $skills;
    }

    /**
     *
     * @param field_type $title            
     */
    public function setTitle ($title)
    {
        $this->title = $title;
    }

    /**
     *
     * @param field_type $level            
     */
    public function setLevel ($level)
    {
        $this->level = $level;
    }

    /**
     *
     * @param field_type $personaldata_id            
     */
    public function setPersonaldataId ($personaldata_id)
    {
        $this->personaldata_id = $personaldata_id;
    }

    /**
     *
     * @param field_type $translation_id            
     */
    public function setTranslationId ($translation_id)
    {
        $this->translation_id = $translation_id;
    }

    /**
     *
     * @param field_type $visible            
     */
    public function setVisible ($visible)
    {
        $this->visible = $visible;
    }

    /**
     *
     * @param field_type $sort            
     */
    public function setSort ($sort)
    {
        $this->sort = $sort;
    }

    /**
     *
     * @param field_type $createdat            
     */
    public function setCreatedat ($createdat)
    {
        $this->createdat = $createdat;
    }

    /**
     *
     * @param field_type $updatedat            
     */
    public function setUpdatedat ($updatedat)
    {
        $this->updatedat = $updatedat;
    }
}