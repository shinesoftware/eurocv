<?php
namespace Cv\Model;
use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class JobTerminationTable
{

    protected $tableGateway;

    public function __construct (TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveJobTermination (JobTermination $jobtermination)
    {
        $data = array ( 
                'category' => $jobtermination->category
        );
        
        $id = (int) $jobtermination->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getJobTermination($id)) {
                $this->tableGateway->update($data, array ( 
                        'id' => $id
                ));
            } else {
                throw new \Exception('JobTermination ID does not exist');
            }
        }
        
        return $jobtermination;
    }

    /**
     * Get all users
     *
     * @return ResultSet
     */
    public function fetchAll ()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Get JobTermination account by JobTerminationId
     *
     * @param string $id            
     * @throws \Exception
     * @return Row
     */
    public function getJobTermination ($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array ( 
                'id' => $id
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Delete JobTermination by JobTerminationId
     *
     * @param string $id            
     */
    public function deleteJobTermination ($id)
    {
        $this->tableGateway->delete(array ( 
                'id' => $id
        ));
    }
}
