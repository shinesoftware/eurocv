<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Service\Europass;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class IndexController extends AbstractActionController
{
    protected $pac;
    protected $user;
    protected $locale;
    protected $translation_id;
    protected $languagecode;
    
    /**
     * OnDispatch of the controller
     * (non-PHPdoc)
     * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
     */
    public function onDispatch(\Zend\Mvc\MvcEvent $e){
        
        $locale = "en_US";
        
        $this->pac = $this->params()->fromRoute('pac');
        $this->languagecode = $this->params()->fromRoute('language');

        // Get the TableGateway object to retrieve the data
        $user = $this->getServiceLocator()->get('PdMapper');
        
        $translation = $this->getServiceLocator()->get('TranslationTable');
        
        // Get the user
        $this->user = $user->findUser('pac', $this->pac);

        // Check the user and get the default language
        if(!empty($this->user)) {
            
            // get the default language set by the user
            $this->translation_id = $this->user->getTranslationId();
            $this->locale = $translation->getTranslation($this->translation_id)->getLocale();
            
             // If the user call pac.en.eurocv.eu change the language translation 
             if(!empty($this->languagecode)){
                 $commondata = $e->getApplication()->getServiceManager()->get('CommonData');

                 // get the language versions of the items
                 $languageversions = $commondata->isMultilanguage($this->user->getId());
                 
                 // Get the translation information
                 $translationdata = $translation->getTranslationbyCode($this->languagecode);

                 // check if the request custom url gets a correct cv version 
                 if(!empty($translationdata) && !empty($languageversions) && is_array($languageversions)){
                     if(in_array($translationdata->getId(), array_keys($languageversions))){
                         $this->translation_id = $translationdata->getId();
                         $this->locale = $translation->getTranslationbyCode($this->languagecode)->getLocale();
                     }
                 }
             }else{
                 $this->languagecode = $translation->getTranslation($this->translation_id)->getCode();
             }
             
             // Set the base locale for the session
             $translator = $e->getApplication()->getServiceManager()->get('translator');
             $translator->setLocale($this->locale)->setFallbackLocale('en_US');
        }

        return parent::onDispatch( $e );
    }
    
    /**
     * This action handles the homepage
     * (non-PHPdoc)
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     */
    public function indexAction()
    {
        $model = new ViewModel();
        return $model;
    }
    
    public function exportAction()
    {
        $europass = new Europass($this->locale, $this->getServiceLocator()->get('LangsTable'));
        
        // If the owner has been logged in ...
        if($this->zfcUserAuthentication()->hasIdentity()){
        
            // If the user has been logged and he is the owner of the cv
            if($this->user->getUserId() == $this->zfcUserAuthentication()->getIdentity()->getId()){
                if(!$this->user->getPublic()){
                    $this->flashMessenger()->setNamespace('danger')->addMessage('This curriculum vitae has been set as private! Only you can see it.');
                }
            }else{
                if(!$this->user->getPublic()){
                    return $this->redirect()->toRoute('pac/private', array (
                            'pac' => $this->pac,
                    ));
                }
            }
        }else{
        
            if(!empty($this->user)){
            
                // Check if the curriculum is private
                if(!empty($this->user) && !$this->user->getPublic()){
            
                    return $this->redirect()->toRoute('pac/private', array (
                            'pac' => $this->pac,
                    ));
                }
            }else{
                $this->flashMessenger()->setNamespace('danger')->addMessage('This curriculum vitae has been not found!');
                return $this->redirect()->toRoute('home');
            }
            
        }
        
        
        $export = $this->params()->fromRoute('export');
        
        // Get the personal data information
        $pd = $this->getServiceLocator()->get('PdTable');
        $pd->setDownload($this->user->getId()); // Set the visits
        
        // Get the work experiences
        $we = $this->getServiceLocator()->get('WeTable');
        $workExperiences = $we->fetchAllbyId($this->user->getId(), $this->translation_id);
        
        // Get the educations and trainings
        $et = $this->getServiceLocator()->get('EtTable');
        $educations = $et->fetchAllbyId($this->user->getId(), $this->translation_id);
        
        // Get the other languages
        $ol = $this->getServiceLocator()->get('OlTable');
        $languages = $ol->fetchAllbyId($this->user->getId(), $this->translation_id);
        
        // Get the compentences and skills
        $cs = $this->getServiceLocator()->get('CsTable');
        $competences = $cs->fetchAllbyId($this->user->getId(), $this->translation_id)->current();
        
        // Get the coverletter
        $cl = $this->getServiceLocator()->get('ClTable');
        $coverletter = $cl->fetchAllbyId($this->user->getId(), $this->translation_id)->current();
        
        // Get the jobcategory
        $jobcategoryId = $this->user->getJobcategoryId();
        $jobCategory = !empty($jobcategoryId) ? $this->getServiceLocator()->get('JobCategoryTable')->getJobCategory($jobcategoryId) : null;
        
        $europass->setUser($this->user);
        $europass->setJobcategory($jobCategory);
        $europass->setWorkexperience($workExperiences);
        $europass->setEducation($educations);
        $europass->setLanguages($languages);
        $europass->setCompetences($competences);
        $europass->setCoverletter($coverletter);
        $europass->setFormat($export);
        $result = $europass->build();
        
        die($result);
    }
    
    /**
     * This action handles the download of the files attached
     * (non-PHPdoc)
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     */
    public function downloadAction()
    {
        $filename = $this->params()->fromRoute('file');
        
        if(!empty($filename)){
            $uploadTable = $this->getServiceLocator()->get('UploadsTable');
            $file = $uploadTable->getUploadByName($filename);
            
            // Fetch Configuration from Module Config
            $uploadPath = $this->getFileUploadLocation();
            
            $fileName = $uploadPath . "/" . $file->getCodedfilename();
            if(file_exists($fileName)){
                
                $response = new \Zend\Http\Response\Stream();
                $response->setStream(fopen($fileName, 'r'));
                $response->setStatusCode(200);
                
                $headers = new \Zend\Http\Headers();
                $headers->addHeaderLine('Content-Type', 'application/octet-stream')
                        ->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($fileName) . '"')
                        ->addHeaderLine('Content-Length', filesize($fileName));
                
                $response->setHeaders($headers);
                return $response;
            }
        }
        
        $model = new ViewModel(array('file' => $file));
        $model->setTemplate('application/index/filenotfound');
        return $model;
    }

    /**
     * This action handles the selection of the language
     * 
     * @return \Zend\Http\Response
     */
    public function changelngAction ()
    {
        $session = new Container('base');
        $translation_id = $this->params()->fromRoute('id');
        $translation = $this->getServiceLocator()->get('TranslationTable');
        
        if(!empty($translation_id) && is_numeric($translation_id)){
            $locale = $translation->getTranslation($translation_id)->getLocale();
            $session->offsetSet('locale', $locale);
        }
       
        if($this->getRequest()->getHeader('Referer')){
        	$url = $this->getRequest()->getHeader('Referer')->getUri();

        	return $this->redirect()->toUrl($url);
        }else{
        	return $this->redirect()->toRoute('home');
        }
    }
    
    /**
     * This action handles the curriculum vitae visualization
     * @return \Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function showAction()
    {
        
        // Check if the user is present
        if(empty($this->user)){
            $this->getResponse()->setStatusCode(410); // The requested resource is no longer available at the server and no forwarding address is known. 
            $this->flashMessenger()->setNamespace('danger')->addMessage('The curriculum vitae has not been found.');
            $model = new ViewModel(array (
                    'pac' => $this->pac,
            ));
            $model->setTemplate('application/index/notfound');
            return $model;
        }

        // If the owner has been logged in ... 
        if($this->zfcUserAuthentication()->hasIdentity()){
            
            // If the user has been logged and he is the owner of the cv 
            if($this->user->getUserId() == $this->zfcUserAuthentication()->getIdentity()->getId()){
                if(!$this->user->getPublic()){
                    $this->flashMessenger()->setNamespace('danger')->addMessage('This curriculum vitae has been set as private! Only you can see it.');
                }
            }else{
                if(!$this->user->getPublic()){
                    return $this->redirect()->toRoute('pac/private', array (
                            'pac' => $this->pac,
                    ));
                }
            }
        }else{
            // Check if the curriculum is private
            if(!$this->user->getPublic()){
                return $this->redirect()->toRoute('pac/private', array (
                        'pac' => $this->pac,
                ));
            }
        }
        
        // Get the personal data information
        $pd = $this->getServiceLocator()->get('PdTable');
        $pd->setVisits($this->user->getId()); // Set the visits
        
        // Get the work experiences
        $we = $this->getServiceLocator()->get('WeTable');
        $workExperiences = $we->fetchAllbyId($this->user->getId(), $this->translation_id);
        
        // Get the educations and trainings
        $et = $this->getServiceLocator()->get('EtTable');
        $educations = $et->fetchAllbyId($this->user->getId(), $this->translation_id);
        
        // Get the other languages
        $ol = $this->getServiceLocator()->get('OlTable');
        $languages = $ol->fetchAllbyId($this->user->getId(), $this->translation_id);

        // Get the compentences and skills
        $cs = $this->getServiceLocator()->get('CsTable');
        $competences = $cs->fetchAllbyId($this->user->getId(), $this->translation_id)->current();
        
        // Get the cover letter
        $cl = $this->getServiceLocator()->get('ClTable');
        $coverletter = $cl->fetchAllbyId($this->user->getId(), $this->translation_id)->current();
        
        $export_format = $this->params()->fromRoute('export');
        if(!empty($export_format)){
            $this->flashMessenger()->setNamespace('info')->addMessage('The export feature has been disabled in the meanwhile read the CV in html format');
        }
        
        // Move all the information to the view 
        $model = new ViewModel(array (
                'locale' => $this->languagecode,
                'user' => $this->user,
                'we' => $workExperiences,
                'et' => $educations,
                'ol' => $languages,
                'cs' => $competences,
                'cl' => $coverletter,
        ));
        return $model;
    }

    public function getFileUploadLocation ()
    {
        // Fetch Configuration from Module Config
        $config = $this->getServiceLocator()->get('config');
        return $config['module_config']['document_location'];
    }
    
}
