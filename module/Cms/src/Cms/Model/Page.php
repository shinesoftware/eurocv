<?php
namespace Cms\Model;

/*
	CREATE TABLE page (
	         id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
	         title TEXT NOT NULL,
	         uri VARCHAR(255) NULL,
	         createdat DATETIME NOT NULL,
	         updatedat DATETIME NOT NULL,
	         content TEXT NOT NULL,
	         PRIMARY KEY (id)
	);
 */

class Page
{

    public $id;

    public $title;

    public $uri;
    
    public $content;
    
    public $createdat;
    
    public $updatedat;

    function exchangeArray ($data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->title = (isset($data['title'])) ? $data['title'] : null;
        $this->uri = (isset($data['uri'])) ? $data['uri'] : null;
        $this->content = (isset($data['content'])) ? $data['content'] : null;
        $this->createdat = (isset($data['createdat'])) ? $data['createdat'] : null;
        $this->updatedat = (isset($data['updatedat'])) ? $data['updatedat'] : null;
        
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}